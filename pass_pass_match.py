import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

cuts=["olv","drll","eta","jetpt","lepjetsep","met","mll","cjv","leppt","njets"]
def get_events(tree):
    merged_tree = uproot.open(tree)[f"reco_truth_merge"]
    cols=["recoMatch","truthMatch","r_weight","t_weight"]
    panda_frame=merged_tree.pandas.df(cols)
    total_events=np.sum(panda_frame["r_weight"])
    reco_events=np.sum(panda_frame[panda_frame["recoMatch"]==1]["r_weight"])
    true_events=np.sum(panda_frame[panda_frame["truthMatch"]==1]["t_weight"])
    matches =np.sum(panda_frame[np.logical_and(panda_frame["truthMatch"]==1,panda_frame["recoMatch"]==1)]["r_weight"])
    return reco_events,true_events,matches

df=pd.DataFrame(columns=["cut","Reco Events","Truth Events","Matched Reco Truth"])
i=0
name={"olv":"olv","cjv":"cjv","njets":"nJet cut","eta":"Eta Cut","jetpt":"jet Pt cut","lepjetsep":"DRlj Cut","met":"met cut","mll":"mll cut","leppt":"lep pt cut","drll":"DRll cut"}
for cut in cuts:
    reco,true,match=get_events(f"{cut}_merged__total.root") 
    print(reco,true,match)
    df.loc[i]=[name[cut],reco,true,match]
    i=i+1
df=df.sort_values(by=["Reco Events"],ascending=False)
print(get_events("v21_merged_total.root"))
