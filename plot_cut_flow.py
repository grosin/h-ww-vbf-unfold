import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def plot_cutflow(cutflow,truth_cuts=True,reco_cuts=False):
    cutflow.fillna("None",inplace=True)
    x=[i for i in range(len(cutflow))]
    plt.bar(x,cutflow["fakes"])
    plt.ylabel("Fakes %")
    if(truth_cuts and reco_cuts):
        plt.title("Fakes with applied reco and truth cuts")
    elif(reco_cuts):
        plt.title("Fakes with applied reco cuts only")
    elif(truth_cuts):
        plt.title("Fakes with applied truth cuts only")
    if(truth_cuts):
        plt.xticks(x,cutflow["truth cuts"], rotation='vertical',fontsize="x-small")
    else:
         plt.xticks(x,cutflow["reco cuts"], rotation='vertical',fontsize="x-small")
    plt.show()

data=pd.read_csv("fakes_record.txt")
truth_cutflow=data[data["reco cuts"].isna()]
reco_cutflow=data[data["truth cuts"].isna()]
sim_cutflow=data[~np.logical_xor(data["truth cuts"].isna(),data["reco cuts"].isna())]
plot_cutflow(reco_cutflow,False,True)
plot_cutflow(truth_cutflow,True,False)
plot_cutflow(sim_cutflow,True,True)
'''
x=[i for i in range(len(fakes))]
plt.plot(x,fakes,"-.")
plt.xticks(x, data["cut"], rotation='vertical')
plt.ylabel("Fakes fractions")
plt.title("Fakes Vs. Cutflow")
plt.show()
'''
