#ifndef MERGE_H
#define MERGE_H
#include <algorithm>
#include <vector>
#include "TRandom.h"
#include "TH1F.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include <cstring>
#include <TMath.h>
#include <memory>
#include <fstream>
#include <TTree.h>
#include "TH2F.h"
#include <TRandom.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unordered_map>
#include "fakes_cutflow.h"

std::unordered_map<int,int> FillRunNumber(TTree * recoTree,const char * event_name="eventNumber",bool truth=false);
std::unordered_map<int,int> FillRunNumberWithCuts(TTree * Tree,const char * event_name, std::vector<cut> & cutflow);
bool IsInMap(std::unordered_map<int,int> &mymap, int input);
bool IsInMap(std::unordered_map<std::string,std::string> &mymap, std::string input);

TTree * merge_trees(TTree *truthTree, TTree *recoTree);
#endif /* MERGE_H */
