#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ TTree;
#pragma link C++ TIOFeatures;
#pragma link C++ TTreeReader;
#pragma link C++ global gROOT;
#pragma link C++ global TROOT;

#endif
