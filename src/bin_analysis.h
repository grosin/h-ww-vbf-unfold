#ifndef BIN_ANALYSIS
#define BIN_ANALYSIS
#include <iostream>
#include <string>
#include <vector>
#include <TCanvas.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>
#include <TH2F.h>
#include <TTree.h>
#include <TTree.h>
#include <TROOT.h>
#include "RooUnfoldResponse.h"

TH1F*  purity(RooUnfoldResponse &response,TH1F * reco);

TH1F* effeciency_plots(RooUnfoldResponse &response,TH1F * truth);

void average_weighted_effeciency(TH1F * reco, TH1F *truth, double * binning, int nbins);
TH1F * misses_plot(RooUnfoldResponse &response, TH1F* truth,std::string name);
TH1F * fakes_plot(RooUnfoldResponse &response, TH1F* reco);
void false_spectrum_comparison(RooUnfoldResponse &response, std::string name);
void false_spectrum_comparison(TTree *reco_tree, TTree *truth_tree, std::string name);
void optimize_binning();
void draw_false_spectrum(TH1F *total_reco, TH1F* misses, TH1F *fakes);
#endif //BIN_ANALYSIS
