#include <TTree.h>
#include <vector>
#include <string>
#include <TH1F.h>
#include <TFile.h>
#include <iostream>
#include <TPad.h>
#include <TGraph.h>
#include <TGraphAsymmErrors.h>
#include <fstream>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <map>
#include <TNtuple.h>
#include <string>
#include "tools.h"
#include "real_data_analysis.h"
#include <THStack.h>
#include <TStyle.h>
#include <RooUnfoldResponse.h>
#include "merge.h"
#include "unfold.h"
#include <bits/stdc++.h> 
#include <tuple>
#include "bin_analysis.h"
#include <TPaveLabel.h>
#include <algorithm>
#include "config.h"
#include <TProfile.h>
#include <TMatrixDSym.h>
#include "systematics.h"
#include "TLatex.h"
#include "validation_plots.h"
#include "fakes_cutflow.h"
using namespace std;
double ww_norm;
double zjets_norm;
double top_norm;
std::map<std::string,distribution_container> distribution_maps;

struct normalizations {
  double ww;
  double zjets;
  double top;
} ;

string to_lower(string st){
  transform(st.begin(), st.end(), st.begin(), ::tolower);
  return st;
}

tuple<TH1F *,THStack*>create_data(string &feature){
  fill_map();
  string dict_feature=to_lower(feature);
  string file_path="ntupls/reco/total_merged_Z_BDT_newBDT.root";
  map<string,TTree*> background_trees;
  map<string,TH1F*> background_hists;
  TTree *vbf_tree=LoadTree(file_path,"vbf");
  TTree *data_tree=LoadTree(file_path,"data");

  map<string,double> scale_map;
  background_trees["diboson"]=LoadTree(file_path,"diboson");
  scale_map["diboson"]=ww_norm;
  
  background_trees["Zjets"]=LoadTree(file_path,"Zjets");
  scale_map["Zjets"]=zjets_norm;

  background_trees["Vgamma"]=LoadTree(file_path,"Vgamma");
  scale_map["Vgamma"]=1;

  background_trees["ggf"]=LoadTree(file_path,"ggf");
  scale_map["ggf"]=1;

  background_trees["top"]=LoadTree(file_path,"top");
  scale_map["top"]=top_norm;

  background_trees["vh"]=LoadTree(file_path,"vh");
  scale_map["vh"]=1;

  TH1F *vbf_hist=LoopTree(vbf_tree,feature.c_str(),distribution_maps[dict_feature].bins,distribution_maps[dict_feature].binning,(feature+" vbf").c_str());
  vbf_hist->SetFillColor(kRed);
  TH1F *data_hist=LoopTree(data_tree,feature.c_str(),distribution_maps[dict_feature].bins,distribution_maps[dict_feature].binning,"data");
  data_hist->SetFillColor(kWhite);
  data_hist->SetMarkerStyle(21);
  
  DrawHist<TH1F>(data_hist);
  int i=0;
  THStack * mcStack = new THStack("mcStack"," stacked");
  vbf_hist->SetStats(0);
  int colors[]={kBlue,kSpring,kBlack,kYellow,kMagenta,kGreen,kGray,kOrange,kViolet};
  mcStack->Add(vbf_hist);
  for(auto const & variable : background_trees){
    background_hists[variable.first]=LoopTree(variable.second,feature.c_str(),distribution_maps[dict_feature].bins,distribution_maps[dict_feature].binning,variable.first,scale_map[variable.first],false);
    std::cout<<scale_map[variable.first]<<"\n";
    background_hists[variable.first]->SetFillColor(colors[i++]);
    background_hists[variable.first]->SetFillStyle(1001);
    mcStack->Add(background_hists[variable.first]);
  }
  std::cout<<"finished looping\n";
  TCanvas * c1 = new TCanvas("c1","stacked hists",700,900);
  c1->cd();
    std::cout<<"created canvas\n";

  mcStack->SetTitle(feature.c_str());
  mcStack->Draw("hist");
  std::cout<<"drew hist canvas\n";

  gStyle->SetOptStat(0);
  //gPad->SetLogy(1);
  data_hist->SetStats(0);
  std::cout<<"set stats \n";
    
  data_hist->Draw("E1 SAME");
  std::cout<<"bulit legend\n";
  TH1F *truth_data=LoadTruthData(feature,distribution_maps[to_lower(feature)].bins,distribution_maps[to_lower(feature)].binning);
  truth_data->Scale(3);
  truth_data->SetMarkerStyle(21);
  truth_data->SetName("truth data");
  truth_data->SetLineColor(kRed);
  truth_data->Draw("E1 SAME");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");

  c1->Print((feature+"_stacked.pdf").c_str());
  std::cout<<"create stack histogram \n";

  for(auto const & variable : background_hists){
     data_hist->Add(variable.second,-1);
   }
  std::cout<<"finished subtracting background\n";
  double max=data_hist->GetMaximum();
  vbf_hist->GetYaxis()->SetRangeUser(1,max);
  TCanvas * c2 = new TCanvas("c2","vbf_only",700,900);
  c2->cd();
  vbf_hist->SetTitle(feature.c_str());
  //gPad->RangeAxis(0.0,0.0,3000.0, 100.0);
  gPad->SetLogy(1);
  vbf_hist->Draw("hist");
  
  data_hist->Draw("E1 SAME");


  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  c2->Print((feature+"vbf_data_approximation.pdf").c_str());
  delete vbf_tree;
  delete data_tree;
  for(auto const & variable : background_trees){
    delete variable.second;
  }
  
  return  std::make_tuple(data_hist,mcStack);

}

void merge_trees(std::string cut){
 string recoPath="ntupls/reco/reco_ntuple_vbf_v21.root";
 string truthPath="ntupls/truth/truth_ntuple_vbf_v21.root";

 TFile *recoFile=TFile::Open(recoPath.c_str());
 TFile *truthFile=TFile::Open(truthPath.c_str());

 TTree * recoTree=(TTree*)recoFile->Get("Vbf_Total");
 TTree * truthTree=(TTree*)truthFile->Get("Vbf_Total");
  
  
 std::cout<<"Merging\n";

 std::string merged_tree_name="ntupls/merged/v21_merged_total.root";
 TFile * mergedFile= TFile::Open(merged_tree_name.c_str(),"RECREATE");
 
 TTree *merged=merge_trees(truthTree,recoTree);
 //merged->Write();
 mergedFile->Write();
 mergedFile->Close();
  


}

void make_data_loop_feature(){
  //std::string  features[]={"Mjj","Mll","DPhijj","DPhill","DEtajj","DPhijj","DRll","DRjj"};
  std::string  features[]={"Mjj"};
  std::tuple<TH1F*,THStack*> stack_tuple;
  
  std::map<std::string,normalizations> norm_map;
  norm_map["Mjj"]={0.7,0.64,0.58};
  norm_map["Mll"]={0.54,0.57,0.625};
  norm_map["DPhijj"]={0.55,0.49,0.66};
  norm_map["DPhill"]={0.63,0.30,0.63};
  norm_map["DEtajj"]={0.74,0.75,0.555};
  norm_map["DPhijj"]={0.52,0.66,0.59};
  norm_map["DRll"]={0.7,0.65,0.57};
  norm_map["DRjj"]={0.57,0.60,0.60};
  for(auto &feature : features){
    TH1F * reduced_data;
    TH1F *unfolded_hist;
    THStack * mcStack;
    TFile *saveFile=TFile::Open((feature+"_save.root").c_str(),"RECREATE");
    ww_norm=norm_map[feature].ww;
    zjets_norm=norm_map[feature].zjets;
    top_norm=norm_map[feature].top;
    stack_tuple=create_data(feature);
    
    std::cout<<"finished creating data \n";
    reduced_data=get<0>(stack_tuple);
    mcStack=get<1>(stack_tuple);
    saveFile->cd();
    mcStack->Write();
    reduced_data->Write();
    RooUnfoldResponse response_matrix=UnfoldDist(3,to_lower(feature).c_str(),"",1);
    print_matrix(response_matrix.Mresponse(),feature.c_str());
    saveFile->cd();
    response_matrix.Hresponse()->Write();
    unfolded_hist =unfold_data(reduced_data,response_matrix,3);
    saveFile->cd();
    unfolded_hist->Write();
    
    TH1F *truth_data=LoadTruthData(feature,distribution_maps[to_lower(feature)].bins,distribution_maps[to_lower(feature)].binning);
    truth_data->Scale(40);
    TCanvas *cav=new TCanvas();
    cav->cd();
    unfolded_hist->SetLineColor(kRed);
    truth_data->SetLineColor(kBlue);
    truth_data->SetMarkerStyle(21);
    truth_data->SetFillColor(kWhite);
    reduced_data->SetLineColor(kSpring);
    unfolded_hist->Draw();
    truth_data->Draw("SAME");
    reduced_data->Draw("SAME");
    gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
    cav->Print((feature+"_Unfolded_data_mc_comparison.pdf").c_str());
    saveFile->Close();
    
      //delete reduced_data;
      //delete unfolded_hist;
      //delete mcStack;
      //delete saveFile;
   
  }

}


void draw_purity_effeciency(string &dist,RooUnfoldResponse UnfoldTuple){
  auto response_matrix=UnfoldTuple;
  TH1F* truth_hist=(TH1F*)UnfoldTuple.Htruth()->Clone("my_truth_hist");
  TH1F*reco_hist=(TH1F*)UnfoldTuple.Hmeasured()->Clone("my_reco_hist");

  TH1F* purity_hist=purity(response_matrix,reco_hist);
  TH1F *effeciency_hist=effeciency_plots(response_matrix,truth_hist);
  
  TCanvas * c2 = new TCanvas("c2","",700,900);

  std::map<std::string,std::string> unfolding_features={
                                                        {"pt_H","Higgs Pt"},
                                                        {"MTll","MTll"},
                                                        {"ptTot","Total Pt"},
                                                        {"Ptll","Ptll"},
                                                        {"lep0_pt","Leading Lep Pt"},
                                                        {"lep1_pt","Subleading Lep Pt"},
                                                        {"DYll","DYll"},
                                                        {"DPhill","DPhill"},
                                                        {"costhetastar","Cos(#theta*)"},
                                                        {"Mjj","Mjj"},
                                                        {"jet0_pt","Leading Jet Pt"},
                                                        {"jet1_pt","Subleading Jet Pt"},
                                                        {"DYjj","DYjj"},
                                                        {"Mll","Mll"},
                                                        {"DPhijj","DPhijj"}
  };
  std::map<std::string,std::string> VarUnitMap{
                                                        {"pt_H","[MeV]"},
                                                        {"lep0_pt","[MeV]"},
                                                        {"lep1_pt","[MeV]"},
                                                        {"DYll"," "},
                                                        {"DPhill","[Rad]"},
                                                        {"costhetastar"," "},
                                                        {"Mjj","[MeV]"},
                                                        {"jet0_pt","[MeV]"},
                                                        {"jet1_pt","[MeV]"},
                                                        {"DYjj"," "},
                                                        {"Mll","[MeV]"},
                                                        {"DPhijj","[Rad]"}                                               

  };
  
  //density_histogram(truth_hist);
  //density_histogram(reco_hist);
  truth_hist->SetMarkerStyle(1);
  reco_hist->SetMarkerStyle(1);
  truth_hist->SetLineColor(kRed);
  reco_hist->SetLineColor(kBlue);
  //c2->Divide(1,2,0,0);
  c2->cd();
  gStyle->SetOptTitle(0);
 
  //truth_hist->SetTitle("truth");
  //gPad->SetTitle(dist.c_str());
  //truth_hist->SetMinimum(0);
  //reco_hist->SetTitle("reco");
  //c2->cd(1);
  //truth_hist->GetYaxis()->SetTitle("Events / Bin Width");
  //truth_hist->GetYaxis()->SetTitleOffset(1.5);
  //truth_hist->Draw("E1");
  //reco_hist->Draw("E1 SAME");
  //gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  

  TLatex *lat=new TLatex();

  purity_hist->SetMarkerStyle(1);
  effeciency_hist->SetMarkerStyle(1);
  purity_hist->SetMinimum(0.5);    
  purity_hist->SetLineColor(kBlue);
  effeciency_hist->SetLineColor(kRed);
  purity_hist->SetTitle("Purity");
  effeciency_hist->SetTitle("Effeciency");
  effeciency_hist->GetXaxis()->SetTitle(Form("%s %s",unfolding_features[dist].c_str(),VarUnitMap[dist].c_str()));
  effeciency_hist->GetYaxis()->SetTitle("Effeciency and Purity Ratio");
  effeciency_hist->GetYaxis()->SetTitleOffset(1.5);
  //c2->cd(2);

  effeciency_hist->SetMinimum(0.5);
  effeciency_hist->Draw("E1");
  
  //gPad->SetLogy(1);
  purity_hist->Draw("E1 SAME");
  gStyle->SetOptStat(0);

  gPad->BuildLegend(0.75,0.85,0.95,0.95,"");
  TPaveLabel *title = new TPaveLabel(.35,.90,.55,.95,unfolding_features[dist].c_str(),unfolding_features[dist].c_str());
  double x_coordinate=(purity_hist->GetBinLowEdge(purity_hist->GetNbinsX()))/1.2;

  lat->DrawLatex(-1,0.65,"#it{#bf{ATLAS}} Internal");
  lat->DrawLatex(-1,0.65*0.96,Form("#sqrt{s} = 13 TeV, %.1f fb^{-1}",139.1));

  c2->Print((dist+"_eff_purity.pdf").c_str());

}

void draw_misses_fakes(string const &dist,RooUnfoldResponse & UnfoldTuple){
  auto response_matrix=UnfoldTuple;
  TH1F* truth_hist=(TH1F*)UnfoldTuple.Htruth();
  TH1F*reco_hist=(TH1F*)UnfoldTuple.Hmeasured();

  
  //TCanvas * c2 = new TCanvas("c2","",700,900);

  std::map<std::string,std::string> unfolding_features={
                                                        {"higgs_pt","Higgs Pt"},
                                                        { "MTll","MTll"},
                                                        {"ptTot","Total Pt"},
                                                        {"Ptll","Ptll"},
                                                        {"lep_pt_0","Leading Lep Pt"},
                                                        {"lep_pt_1","Subleading Lep Pt"},
                                                        {"DYll","DYll"},
                                                        {"DPhill","DPhill"},
                                                        {"cosTheta","Cos(#theta*)"},
                                                        {"Mjj","Mjj"},
                                                        {"jet_pt_0","Leading Jet Pt"},
                                                        {"jet_pt_1","Subleading Jet Pt"},
                                                        {"DYjj","DYjj"},
                                                        {"Mll","Mll"}
  };

  //TPaveLabel *title = new TPaveLabel(.35,.90,.55,.95,unfolding_features[dist].c_str(),unfolding_features[dist].c_str());
  //title->Draw(); 
  TH1F* misses_hist=misses_plot(response_matrix,truth_hist,dist);
  TH1F *fakes_hist=fakes_plot(response_matrix,reco_hist);
  misses_hist->SetMarkerStyle(1);
  fakes_hist->SetMarkerStyle(1);
  misses_hist->SetMinimum(0.0001);    
  fakes_hist->SetLineColor(kBlue);
  fakes_hist->SetLineColor(kRed);
  misses_hist->SetName((dist+"_Misses").c_str());
  fakes_hist->SetName((dist+"Fakes").c_str());
  
  //DrawHist<TH1F>(misses_hist);
  //DrawHist<TH1F>(fakes_hist);
  std::cout<<dist<<",";
  for(int i=1;i<=fakes_hist->GetNbinsX();i++)
    std::cout<<fakes_hist->GetBinContent(i)<<"("<<fakes_hist->GetBinContent(i)/UnfoldTuple.Vmeasured()(i-1) <<")"<<",";
  std::cout<<"\n";
}

void print_save_response_matrix(TH1 * response){
  std::ofstream matrix_file ("response_matrices.txt",ios::app);  
  TFile * response_root=TFile::Open("response_matries.root","RECREATE");
  response->Write();
  matrix_file<<"variable:"<<response->GetName()<<"\n";
  
  for(int i=0;i< response->GetNbinsX();i++){
    for(int j=0; j<response->GetNbinsY();j++){
      matrix_file<<response->GetBinContent(i,j)<<",";
    }
    matrix_file<<"\n";
  }
  response_root->Close();
  matrix_file.close();
}

int c_factor(TH1F * truth, TH1F *reco){
  std::cout<<"reco Integral: "<<reco->Integral()<<"\n";
  std::cout<<"truthIntegral: "<<truth->Integral()<<"\n";
  return reco->Integral()/truth->Integral();
}
void cutflow_analysis(){
  std::string cuts [] = {"njets","eta","leppt","mll","met","lepjetsep","jetpt","drll","cjv","olv"};
  bool draw=false;
  for(auto cut : cuts){
    std::string merged_tree_name="ntupls/merged/"+cut+"_merged__total.root";
    RooUnfoldResponse UnfoldTuple= UnfoldDist(3,"Mjj",merged_tree_name,draw);
  }
  

}

void loop_observables(int x ){
  bool draw=false;
  std::string merged_tree_name="ntupls/merged/higgs_olv_merged_total_420.root"; 
  std::string fourl_binning="fourl_binning_config.ini";
  std::string ggf_binning="anamika_binning.ini";
  std::string ggf_features []={"Mll","DPhill","higgs_pt","lep_pt_0","Ptll","cosTheta"};

  std::string fourl_features[]={"higgs_pt","jet_pt_0","jet_pt_1","cosTheta","Mjj","DPhijj","DEtajj"};
  std::string config_file;
 
  if(x==2){
    config_file="/home/guy/Cprojects/cern/HiggsAnalysis/caf_analysis/h-ww-vbf-unfold/"+ggf_binning;
    read_binning(config_file);
    for(auto feature:ggf_features){
      std::cout<<"feature: "<<feature<<"\n";
      RooUnfoldResponse UnfoldTuple= UnfoldDist(3,feature.c_str(),merged_tree_name.c_str(),draw);
      auto response=UnfoldTuple.Hresponse();
      print_matrix(response,feature.c_str());
    }
  }
  if(x==3){
    config_file="/home/guy/Cprojects/cern/HiggsAnalysis/caf_analysis/h-ww-vbf-unfold/"+fourl_binning;
    read_binning(config_file);
    for(auto feature:fourl_features){
      std::cout<<"feature: "<<feature<<"\n";
      RooUnfoldResponse UnfoldTuple= UnfoldDist(3,feature.c_str(),merged_tree_name.c_str(),draw);
      auto response=UnfoldTuple.Hresponse();
      print_matrix(response,feature.c_str());
    }
  }
}

void svd_study(std::string feature){
  std::string merged_tree_name="ntupls/merged/unfolding_olv_merged_total.root"; 
  int nbins=distribution_maps[feature].bins;
  bool draw=true;
  for(int i=0;i<nbins;i++){
    std::cout<<"feature: "<<feature<<"\n";
    RooUnfoldResponse UnfoldTuple= UnfoldSVD(i,feature.c_str(),merged_tree_name.c_str(),draw);
    if(i==3){
      draw_purity_effeciency(feature,UnfoldTuple);
      //false_spectrum_comparison(std::get<0>(UnfoldTuple),feature);  
    }
  }
  //auto response=std::get<0>(UnfoldTuple).Hresponse();
  //print_matrix(response,feature.c_str());
  //draw_purity_effeciency(feature,UnfoldTuple); 
}

void create_data(TTree * truth_tree,TTree *reco_tree,vector<string> unfolding_features){
    std::cout<<"creating response\n";
    std::cout<<reco_tree<<"\n";
    std::ofstream projectFile ;
    projectFile.open("matrix_projection_stat.txt",std::ofstream::out);
    for( auto &feature:unfolding_features){
      RooUnfoldResponse response=UnfoldInPlace(feature,truth_tree,reco_tree);
      std::cout<<"unfolding response\n";
       draw_purity_effeciency(feature,response);
      
      auto matrix=response.Mresponse();
      auto hist=response.Hresponse();
      auto truth=response.Htruth();
      auto reco=response.Hmeasured();
      auto fakes=response.Vfakes();
      int nbins=reco->GetNbinsX();
      int total_events=0;
      double total_true_events=0;
      for(int i=0;i<nbins+1;i++){
        total_true_events+=truth->GetBinContent(i);
      }
      for(int i=0;i<nbins;i++){
        projectFile<<feature<<",Bin"<<i<<",";
        double projection;//=matrix[i][i]*truth->GetBinContent(i+1);
        for(int j=0;j<nbins;j++){
          projection+=matrix[i][j]*truth->GetBinContent(j+1);
          projectFile<<matrix[i][j]*truth->GetBinContent(j+1)<<",";
          total_events+=matrix[i][j]*truth->GetBinContent(j+1);
        }
        projectFile<<fakes[i];
        total_events+=fakes[i];
        projection=projection*reco->GetBinContent(i+1);
        projectFile<<"\n";
      }
      std::cout<<"Total Events!!!!"<<total_events<<"\n";  
      
      projectFile<<"\n";

      //print_matrix(matrix,feature.c_str());
      print_matrix(hist,feature.c_str());
      
    }
    projectFile.close();

}
TH1D* convert(TGraphAsymmErrors * g,std::string feature){
  int N=g->GetN();
  string new_feature=feature;
  if(feature.compare("DPhijj")==0)
    new_feature="SignedDPhijj";
  auto binning=distribution_maps[new_feature].binning;
  for(int b=0;b<distribution_maps[new_feature].bins+1;b++){
    if(binning[b]>1000)
      binning[b]=binning[b]/1000;
  }
  TH1D* hist=new TH1D(new_feature.c_str(),feature.c_str(),distribution_maps[new_feature].bins,binning);
  double x,y;
  for(int i=0;i<N;i++){
    g->GetPoint(i,x,y);
    double error=(g->GetErrorYhigh(i)+g->GetErrorYlow(i)) /2 ;
    std::cout<<x<<","<<y<<"\n";
    hist->SetBinContent(i+1,y);
    hist->SetBinError(i+1,error);
  }
  return hist;
  
}

void graph2th1(){
  std::vector<std::string>  features={"Mjj","Mll","pt_H","DPhill","costhetastar","DYll","DYjj","DPhijj","lep0_pt","lep1_pt","jet0_pt","jet1_pt"};
  TFile * graphFile=TFile::Open("VBFNLO-NLO-theo-MjjDPhill_symErr.root");
  vector<TH1D*> saved_hists;
  TFile* vbfnlo=TFile::Open("VBFNLO_new_Phase_Space_symErr.root","RECREATE");
  for(auto feature:features){
    TGraphAsymmErrors *mjj=(TGraphAsymmErrors*)graphFile->Get(feature.c_str());
    TH1D *mjj_hist=convert(mjj,feature);
    DrawHist<TH1D>(mjj_hist);
    vbfnlo->cd();
    mjj_hist->Write();
  }

}

int main(){
  std::string config_file="/home/guy/Cprojects/cern/HiggsAnalysis/caf_analysis/h-ww-vbf-unfold/binning_config.ini";
  std::cout<<"set configs\n";
  std::string merged_tree_name="ntupls/merged/v21_merged_total.roo0t"; 
  read_binning(config_file);
  std::cout<<"read configs\n";

  // std::vector<std::string>  unfolding_features={"Mjj","Mll","pt_H","DPhill","costhetastar","DYll","DYjj","SignedDPhijj","lep0_pt","lep1_pt","jet0_pt","jet1_pt"};

   std::vector<std::string>  unfolding_features={"Mjj"};

  
   std::string truth_file_path="ntupls/truth/mtt_PowPy8-noMET.root";
   std::string reco_file_path="ntupls/reco/2jet.root";
  TFile * reco_file=TFile::Open(reco_file_path.c_str());
   std::cout<<"opened reco\n";
  TFile * truth_file=TFile::Open(truth_file_path.c_str());
   std::cout<<"opened truth\n";

  TTree* reco_tree=(TTree*)reco_file->Get("vbf_nominal");
     std::cout<<"retrieved reco\n";

  TTree* truth_tree=(TTree*)truth_file->Get("Vbf_Total");

  std::cout<<"printed both trees\n";
   for(auto& feature : unfolding_features){
     //twoDHistogramsFiducial(feature,truth_tree,reco_tree,"bdt_TopWWAll");
    //bdt_cut_skew( truth_tree,reco_tree,feature,"bdt_TopWWAll");
   }
  
  for(auto& feature : unfolding_features){
    //twoDHistogramsFiducial(feature,truth_tree,reco_tree,"bdt_vbf");
    //   bdt_cut_skew( truth_tree,reco_tree,feature,"bdt_vbf");
  }
  //graph2th1();
  //bdt_cut_skew( truth_tree,reco_tree,"DPhill","bdt_vbf");
  //create_data(truth_tree,reco_tree,unfolding_features);

  //false_spectrum_comparison(reco_tree,truth_tree,"jet0_pt");
  //false_spectrum_comparison(reco_tree,truth_tree,"jet1_pt");
  //false_spectrum_comparison(reco_tree,truth_tree,"lep0_pt");
  //false_spectrum_comparison(reco_tree,truth_tree,"lep1_pt");

  
   for( auto &feature:unfolding_features){
      RooUnfoldResponse response=UnfoldInPlace(feature,truth_tree,reco_tree);
      false_spectrum_comparison(response,feature);
   }
  

  cut dyjj_cut={"DYjj",2.1,">"};
  cut jet0pt_cut={"jet0_pt",30000,">"};
  cut jet1pt_cut={"jet1_pt",30000,">"};
  cut mll_cut={"Mll",10000,">"};
  cut lep0pt_cut={"lep0_pt",22000,">"};
  cut lep1pt_cut={"lep1_pt",15000,">"};
  cut lep0eta_cut={"lep0_eta",2.5,"<"};
  cut lep1eta_cut={"lep1_eta",2.5,"<"};
  cut drlj_cut={"DRlj",0.4,">"};
  cut cjv_cut={"CJV",20000,"<"};
  cut drll_cut={"DRll",0.1,">"};
  cut olv_cut={"OLV",1,"="};
  cut mtt_cut={"mtt",66000,"<"};

  std::vector<cut> dyjj_cutflow={dyjj_cut};
  std::vector<cut> jetpt_cutflow={jet0pt_cut,jet1pt_cut};
  std::vector<cut> leppt_cutflow={lep0pt_cut,lep1pt_cut};
  std::vector<cut> lepeta_cutflow={lep0eta_cut,lep1eta_cut};
  std::vector<cut> mll_cutflow={mll_cut};
  std::vector<cut> drlj_cutflow={drlj_cut};
  std::vector<cut> drll_cutflow={drll_cut};
  std::vector<cut> olv_cutflow={olv_cut};
  std::vector<cut> mtt_cutflow={mtt_cut};
  std::vector<cut> cjv_cutflow={cjv_cut};

  
  std::vector<cut> empty={};
  /*
  loop_fakes_ntuples(truth_tree,reco_tree,"jet0_pt",empty,empty);
  
  loop_fakes_ntuples(truth_tree,reco_tree,"jet1_pt",empty,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"lep1_pt",empty,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"lep0_pt",empty,empty);
  */
  //loop_fakes_ntuples(truth_tree,reco_tree,"CJV",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"OLV",empty,empty);
  
  //loop_fakes_ntuples(truth_tree,reco_tree,"jet1_isJvtHS",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"jet0_isJvtHS",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"jet1_isPU",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"jet0_isPU",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"lep0_is_m",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"MT",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"MET",empty,empty);
  //loop_fakes_ntuples(truth_tree,reco_tree,"nBJets",empty,empty);
  
  /*
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,dyjj_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,jetpt_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,leppt_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,lepeta_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,mll_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,drlj_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,drll_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,olv_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,mtt_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",empty,cjv_cutflow);

  
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",dyjj_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",jetpt_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",leppt_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",lepeta_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",mll_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",drlj_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",drll_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",olv_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",mtt_cutflow,empty);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",cjv_cutflow,empty);
  
  
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",dyjj_cutflow,dyjj_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",jetpt_cutflow,jetpt_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",leppt_cutflow,leppt_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",lepeta_cutflow,lepeta_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",mll_cutflow,mll_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",drlj_cutflow,drlj_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",drll_cutflow,drll_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",olv_cutflow,olv_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",mtt_cutflow,mtt_cutflow);
  loop_fakes_ntuples(truth_tree,reco_tree,"Mjj",cjv_cutflow,cjv_cutflow);
  */
  
  return 1;
}
