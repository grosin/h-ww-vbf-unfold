#include <TTree.h>
#include <unordered_map>
#include "merge.h"
#include <vector>
#include <string>
#include "RooUnfoldResponse.h"
class systematics{
 private:
  std::unordered_map<std::string,TTree *> up_systs;
  std::unordered_map<std::string,TTree *> down_systs;
  TTree * truth_nominal;
  TTree * reco_nominal;
  
 public:
  //constructors
  systematics(TTree*,TTree*);


  //destructor
  ~systematics();

  //add
  void add_systematic(std::string name,TTree * systematic);
    
  //unfold
  RooUnfoldResponse get_nominal_response(std::string variable);
  RooUnfoldResponse get_up_response(std::string systematic,std::string variable);
  RooUnfoldResponse get_down_response(std::string systematic,std::string variable);
  TH1F * get_up_reco_hist(std::string systematic,std::string variable,std::string sample="");
  TH1F * get_down_reco_hist(std::string systematic,std::string variable,std::string sample="");


  //printing
  std::vector<std::string> get_systematics();

};

std::vector<double> analyze_systematics(std::string variable,int iterations=3);
void save_systematics_response(std::string variable,std::string sample);
TH1F* sum_samples(std::vector<std::string> &samples, std::string variable,std::string syst);
void create_reco_systematic(std::string variable);
