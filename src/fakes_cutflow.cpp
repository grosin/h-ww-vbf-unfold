#include <iostream>
#include <TH1F.h>
#include <string>
#include <vector>
#include <TCanvas.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>
#include <TH2F.h>
#include <TTree.h>
#include <map>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TFolder.h>
#include <TRandom3.h>
#include <TProfile.h>
#include "unfold.h"
#include <fstream>
#include "RooUnfoldResponse.h"
#include <TMath.h>
#include "tools.h"
#include "merge.h"
#include "bin_analysis.h"
#include "fakes_cutflow.h"

void run_cutflow(TTree * truth_tree,TTree * reco_tree,std::string name){
  cut mjj_cut={"Mjj",1000000,">"};
  cut dyjj_cut={"DYjj",4,">"};
  std::vector<cut> cutflow={mjj_cut,dyjj_cut};
  std::vector<cut> empty={};
  loop_fakes_ntuples(truth_tree,reco_tree,name,empty,cutflow);


}

bool check_cut(cut & cut_value, float * cut_memory){
  if(cut_value.comp.compare(">")==0){
    if(*cut_memory < cut_value.num) return true;
  }
  else if(cut_value.comp.compare("<")==0){
    if(*cut_memory > cut_value.num) return true;
  }
  else{
    if(*cut_memory != cut_value.num) return true;
  }
  return false;
}

bool check_cut(cut & cut_value, double * cut_memory){
  if(cut_value.comp.compare(">")==0){
    if(*cut_memory < cut_value.num) return true;
  }
  else if(cut_value.comp.compare("<")==0){
    if(*cut_memory > cut_value.num) return true;
  }
  else{
    if(*cut_memory != cut_value.num) return true;
  }
  return false;
}

bool check_cut(cut & cut_value, int * cut_memory){
  if(cut_value.comp.compare(">")==0){
    if(*cut_memory < cut_value.num) return true;
  }
  else if(cut_value.comp.compare("<")==0){
    if(*cut_memory > cut_value.num) return true;
  }
  else{
    if(*cut_memory != cut_value.num) return true;
  }
  return false;
}
void loop_fakes_ntuples(TTree *truth_tree,TTree *reco_tree, std::string name,std::vector<cut> &reco_cutflow,std::vector<cut> &truth_cutflow){

  std::cout<<"setting histograms\n";
  TH1F *matched_events=new TH1F("matchedreco","matched reco",distribution_maps[name].bins,distribution_maps[name].binning);
  TH1F *total_events=new TH1F((name+"_reco").c_str(),(name+"_total").c_str(),distribution_maps[name].bins,distribution_maps[name].binning);;
  TH1F *fakes=new TH1F("fakes","fakes",distribution_maps[name].bins,distribution_maps[name].binning);
  
  float data;
  double weight;
  float inSR;
  int eventNumber;
  float AverageMu;

  vector<float*> cut_memory(reco_cutflow.size());
  vector<float> cut_variable(reco_cutflow.size());
  for(int i=0;i<reco_cutflow.size();i++){
    if(reco_cutflow[i].variable.compare(name)!=0){
      std::cout<<"setting cut variable to "<<reco_cutflow[i].variable.c_str()<<"\n";
      cut_memory[i]=&cut_variable[i];
      reco_tree->SetBranchAddress(reco_cutflow[i].variable.c_str(),&cut_variable[i]);
    }
    else{
      std::cout<<"setting cut variable to"<<name<<"\n";
      cut_memory[i]=&data;
    }
  }
  
  reco_tree->SetBranchAddress("weight",&weight);
  reco_tree->SetBranchAddress("eventNumber",&eventNumber);

  reco_tree->SetBranchAddress(name.c_str(),&data);
  reco_tree->SetBranchAddress("inSR",&inSR);
  bool TruthMatch=false;
  
  std::unordered_map<int,int> truth_map;
  if(truth_cutflow.size()==0)
    truth_map=FillRunNumber(truth_tree,"eventNumber",true);
  else
    truth_map=FillRunNumberWithCuts(truth_tree,"eventNumber",truth_cutflow);
  int nReco=reco_tree->GetEntries();
  bool cut=false;
  float total_fakes=0;
  float total=0;
  for (int i=0;i<nReco;i++){
    cut=false;
    reco_tree->GetEvent(i);
    for(int i=0;i<reco_cutflow.size();i++){
      cut=check_cut( reco_cutflow[i], cut_memory[i]);
      if(cut) break;
    }
    if(cut) continue;
    TruthMatch=IsInMap(truth_map,eventNumber);
    if(TruthMatch) matched_events->Fill(data);
    else {fakes->Fill(data);total_fakes+=1;}
    total_events->Fill(data);
    total+=1;
  }
  reco_tree->ResetBranchAddresses();
  std::cout<<"finished looping reco events\n";
  float fakes_ratio=total_fakes/total;
  draw_false_spectrum(total_events,matched_events, fakes);
  /*
  ofstream fakes_record;
  fakes_record.open("fakes_record.txt",std::ios::app);
  for(auto cut_data:reco_cutflow){
    fakes_record<<cut_data.variable<<":";
  }
  fakes_record<<",";
  for(auto cut_data:truth_cutflow){
    fakes_record<<cut_data.variable<<":";
  }
  fakes_record<<",";
  fakes_record<<fakes_ratio<<"\n";
  fakes_record.close();
  */
}

