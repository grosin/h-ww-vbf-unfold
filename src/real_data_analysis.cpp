#include <TTree.h>
#include <vector>
#include <string>
#include <TH1F.h>
#include <TFile.h>
#include <iostream>
#include <TPad.h>
#include <TGraph.h>
#include <fstream>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TFolder.h>
#include <map>
#include <TNtuple.h>
#include <string>
#include "tools.h"
#include <TColor.h>
#include <TMath.h>
#include "unfold.h"
using namespace std;



TH1F * LoopTree(TTree * data_tree,string feature,int nbins,double *binning,string name, double scale,bool SelectRegion){
  std::cout<<"looping data tree 2\n";
  float olv;
  float Bjets;
  float CJV;
  float mZ;
  float mtt;
  float mjj;
  float njets;
  float DYjj;
  float subJetpt;

  //regions
  float inSR;
  float inTopCR;
  float inWWCR;
  float inZjetsCR;
  
  //jet variables
  float jet0_phi;
  float jet1_phi;
  float jet0_eta;
  float jet1_eta;
  float jet0_pt;

  float dphijj;
  float detajj;
  float drjj;
  
  //lep variables;
  float lep0_phi;
  float lep1_phi;
  float lep0_eta;
  float lep1_eta;
  float lep0_pt;

  float dphill;
  float detall;;
  float drll;
  
  double bdt;
  double weight;

  double GevCorrection=1;
  if(substring(feature,"Mjj") || substring(feature,"Mll")) {GevCorrection=1000;}
  std::cout<<"GEV correction\n";
  std::cout<<GevCorrection<<"\n";
  data_tree->ResetBranchAddresses();
  TH1F * f_hist= new TH1F((feature+"_hist_"+name).c_str(),name.c_str(),nbins,binning);
  f_hist->SetFillStyle(1001);
  f_hist->SetFillColor(kRed);
  float data;
  if(feature.compare("Mjj")!=0){
    cout<<"setting addresses on branch \n";
    data_tree->SetBranchAddress(feature.c_str(),&data);
  }
  data_tree->SetBranchAddress("weight",&weight);
  cout<<"looping over "<<feature<<"\n";

  //cuts
  data_tree->SetBranchAddress("OLV",&olv);
  data_tree->SetBranchAddress("nBJetsSubMV2c10",&Bjets);
  data_tree->SetBranchAddress("nJetsTight",&njets);
  data_tree->SetBranchAddress("centralJetVetoLeadpT",&CJV);
  data_tree->SetBranchAddress("mZ",&mZ);
  data_tree->SetBranchAddress("mtt",&mtt);
  data_tree->SetBranchAddress("Mjj",&mjj);
  data_tree->SetBranchAddress("DYjj",&DYjj);
  data_tree->SetBranchAddress("bdt_Zjets",&bdt);
  data_tree->SetBranchAddress("jet1_pt",&subJetpt);
  data_tree->SetBranchAddress("inSR",&inSR);
  data_tree->SetBranchAddress("inTopCR",&inTopCR);
  data_tree->SetBranchAddress("inWWCR",&inWWCR);
  data_tree->SetBranchAddress("inZjetsCR",&inZjetsCR);

    //jet
  data_tree->SetBranchAddress("jet0_phi",&jet0_phi);
  data_tree->SetBranchAddress("jet1_phi",&jet1_phi);
  data_tree->SetBranchAddress("jet0_eta",&jet0_eta);
  data_tree->SetBranchAddress("jet1_eta",&jet1_eta);
  data_tree->SetBranchAddress("jet0_pt",&jet0_pt);

  //lep variables;
  data_tree->SetBranchAddress("lep0_phi",&lep0_phi);
  data_tree->SetBranchAddress("lep1_phi",&lep1_phi);
  data_tree->SetBranchAddress("lep0_eta",&lep0_eta);
  data_tree->SetBranchAddress("lep1_eta",&lep1_eta);
  data_tree->SetBranchAddress("lep0_pt",&lep0_pt);
 for(int i=0; i < data_tree->GetEntries();i++){
   data_tree->GetEvent(i);
   bool bjet_cut=Bjets < 1;
   bool njets_cut=njets >= 2;
   bool cjv_cut=CJV < 20000;
   bool mcut= mtt/1000 < (mZ/1000 -25);
   bool mjj_cut=mjj > 200000;
   bool dyjj_cut= DYjj > 2.1;
   bool bdt_cut=bdt < 0.5;
   bool olv_cut = olv == 1;
   bool jet_pt_cut=subJetpt > 30;

   bool sr_sum=bjet_cut && bjet_cut && cjv_cut &&
     mcut && mjj_cut && dyjj_cut && bdt_cut
     && olv_cut && jet_pt_cut;
   
   bool sr_cut=inSR==1;
   bool top_cut=inTopCR==1;
   bool zjets_cut=inZjetsCR==1;
   bool ww_cut=inWWCR==1;
   
   if(!sr_sum && SelectRegion){
     continue;
   }
     
   dphijj=jet0_phi-jet1_phi;
   detajj=jet0_eta-jet1_eta;
   drjj=TMath::Power(TMath::Power(dphijj,2)+TMath::Power(detajj,2),0.5);

   dphill=lep0_phi-lep1_phi;
   detall=lep0_eta-lep1_eta;
   drll=TMath::Power(TMath::Power(dphill,2)+TMath::Power(detall,2),0.5);
   if(substring(feature,"DPhijj")){
     f_hist->Fill(dphijj,weight);
   }
   else if(feature.compare("DPhill")==0){
     f_hist->Fill(dphill,weight);

   }
   else if(feature.compare("DEtajj")==0){
     f_hist->Fill(detajj,weight);

   }
   else if(feature.compare("DEtall")==0){
     f_hist->Fill(detall,weight);
   }
   else if(feature.compare("DRjj")==0){
     f_hist->Fill(drjj,weight);

   }
   else if(feature.compare("DRll")==0){
     f_hist->Fill(drll,weight);
   }
   else if(feature.compare("Mjj")==0){
     f_hist->Fill(mjj/GevCorrection,weight);
   }
   else{
     f_hist->Fill(data/GevCorrection,weight);
   }
 }

 f_hist->Scale(scale);
 return f_hist;
}

TH1F * LoopTree(TTree * data_tree,string feature,int nbins,double min, double max,string name, double scale){
  std::cout<<"looping data tree 1\n";

  float data;
  float olv;
  float Bjets;
  float CJV;
  float mZ;
  float mtt;
  float mjj;
  float njets;
  float DYjj;
  float subJetpt;

  //regions
  float inSR;
  float inTopCR;
  float inWWCR;
  float inZjetsCR;
  
    
  double bdt;
  double weight;

  //jet variables
  float jet0_phi;
  float jet1_phi;
  float jet0_eta;
  float jet1_eta;
  float jet0_pt;

  //lep variables;
  float lep0_phi;
  float lep1_phi;
  float lep0_eta;
  float lep1_eta;
  float lep0_pt;
  
  data_tree->ResetBranchAddresses();
  TH1F * f_hist= new TH1F((feature+"_hist_"+name).c_str(),name.c_str(),nbins,min,max);
  f_hist->SetFillStyle(1001);
  f_hist->SetFillColor(kRed);
  
  cout<<"setting addresses on branch \n";
  data_tree->SetBranchAddress(feature.c_str(),&data);
  data_tree->SetBranchAddress("weight",&weight);
  cout<<"looping over "<<feature<<"\n";

  //jet variables
  
  //cuts
  data_tree->SetBranchAddress("OLV",&olv);
  data_tree->SetBranchAddress("nBJetsSubMV2c10",&Bjets);
  data_tree->SetBranchAddress("nJetsTight",&njets);
  data_tree->SetBranchAddress("centralJetVetoLeadpT",&CJV);
  data_tree->SetBranchAddress("mZ",&mZ);
  data_tree->SetBranchAddress("mtt",&mtt);
  data_tree->SetBranchAddress("Mjj",&mjj);
  data_tree->SetBranchAddress("DYjj",&DYjj);
  data_tree->SetBranchAddress("bdt_Zjets",&bdt);
  data_tree->SetBranchAddress("jet1_pt",&subJetpt);
  data_tree->SetBranchAddress("inSR",&inSR);
  data_tree->SetBranchAddress("inTopCR",&inTopCR);
  data_tree->SetBranchAddress("inWWCR",&inWWCR);
  data_tree->SetBranchAddress("inZjetsCR",&inZjetsCR);

  //jet
  data_tree->SetBranchAddress("jet0_phi",&jet0_phi);
  data_tree->SetBranchAddress("jet1_phi",&jet1_phi);
  data_tree->SetBranchAddress("jet0_eta",&jet0_eta);
  data_tree->SetBranchAddress("jet1_eta",&jet1_eta);
  data_tree->SetBranchAddress("jet0_pt",&jet0_pt);

  //lep variables;
  data_tree->SetBranchAddress("lep0_phi",&lep0_phi);
  data_tree->SetBranchAddress("lep1_phi",&lep1_phi);
  data_tree->SetBranchAddress("lep0_eta",&lep0_eta);
  data_tree->SetBranchAddress("lep1_eta",&lep1_eta);
  data_tree->SetBranchAddress("lep0_pt",&lep0_pt);
  for(int i=0; i < data_tree->GetEntries();i++){
    data_tree->GetEvent(i);
    
    bool bjet_cut=Bjets < 1;
    bool njets_cut=njets >= 2;
    bool cjv_cut=CJV < 20000;
    bool mcut= mtt/1000 < (mZ/1000 -25);
    bool mjj_cut=mjj > 200000;
    bool dyjj_cut= DYjj > 2.1;
    bool bdt_cut=bdt < 0.5;
    bool olv_cut = olv == 1;
    bool jet_pt_cut=subJetpt > 30;
    
    bool sr_sum=bjet_cut && bjet_cut && cjv_cut &&
      mcut && mjj_cut && dyjj_cut 
      && olv_cut && jet_pt_cut;
   
    bool sr_cut=inSR==1;
    bool top_cut=inTopCR==1;
    bool zjets_cut=inZjetsCR==1;
    bool ww_cut=inWWCR==1;
    
    if(!sr_sum )
      continue;

   
    std::cout<<data<<"\n";
    f_hist->Fill(data/1000,weight);
  }

 f_hist->Scale(scale);
 return f_hist;
}

//overloaded loop tree

TH1F * LoopTree(TTree * data_tree,string feature,TH1F * hist,string name){
  std::cout<< "looping data tree 3\n";
  float data;
  float olv;
  float Bjets;
  float CJV;
  float mZ;
  float mtt;
  float mjj;
  float njets;
  float DYjj;
  float subJetpt;

 
  //fiducial
  float lead_lep_pt;
  float sub_lead_lep_pt;
  float mll;
  float lead_lep_eta;
  float drll;
  float lead_jet_eta;
  float met;

  float lead_lep_phi;
  float sub_lep_eta;
  float sub_lep_phi;
  
  float lead_lep_flavor;
  float sub_lep_flavor;
  //regions
  float inSR;
  float inTopCR;
  float inWWCR;
  float inZjetsCR;
  
    
  double bdt;
  double weight;
  data_tree->ResetBranchAddresses();
  TH1F * f_hist=(TH1F*)hist->Clone();
  for(int i=0;i<=f_hist->GetNbinsX();i++){
    f_hist->SetBinContent(i,0);
  }
  f_hist->Reset();
  f_hist->SetName(name.c_str());
  f_hist->SetFillStyle(1001);
  f_hist->SetFillColor(kRed);
  
  cout<<"setting addresses on branch \n";
  //data_tree->SetBranchAddress(feature.c_str(),&data);
  data_tree->SetBranchAddress("weight",&weight);
  cout<<"looping over "<<feature<<"\n";

  //cuts
  data_tree->SetBranchAddress("OLV",&olv);
  data_tree->SetBranchAddress("nBJetsSubMV2c10",&Bjets);
  data_tree->SetBranchAddress("nJetsTight",&njets);
  data_tree->SetBranchAddress("centralJetVetoLeadpT",&CJV);
  data_tree->SetBranchAddress("mZ",&mZ);
  data_tree->SetBranchAddress("mtt",&mtt);
  data_tree->SetBranchAddress("Mjj",&mjj);
  data_tree->SetBranchAddress("DYjj",&DYjj);
  data_tree->SetBranchAddress("jet1_pt",&subJetpt);
  data_tree->SetBranchAddress("inSR",&inSR);
  data_tree->SetBranchAddress("inTopCR",&inTopCR);
  data_tree->SetBranchAddress("inWWCR",&inWWCR);
  data_tree->SetBranchAddress("inZjetsCR",&inZjetsCR);

  data_tree->SetBranchAddress("lep0_pt",&lead_lep_pt);
  data_tree->SetBranchAddress("lep1_pt",&sub_lead_lep_pt);
  data_tree->SetBranchAddress("Mll",&mll);
  data_tree->SetBranchAddress("lep0_eta",&lead_lep_eta);
  data_tree->SetBranchAddress("lep0_phi",&lead_lep_phi);
  data_tree->SetBranchAddress("lep1_eta",&sub_lep_eta);
  data_tree->SetBranchAddress("lep1_phi",&sub_lep_phi);

  data_tree->SetBranchAddress("jet0_eta",&lead_jet_eta);
  data_tree->SetBranchAddress("MET",&met);
  data_tree->SetBranchAddress("lep0_truthType",&lead_lep_flavor);
  data_tree->SetBranchAddress("lep1_truthType",&sub_lep_flavor);
  
  for(int i=0; i < data_tree->GetEntries();i++){
   data_tree->GetEvent(i);
   drll=pow(pow(lead_lep_eta-sub_lep_eta,2)+pow(lead_lep_phi-sub_lep_phi,2),0.5);
   bool bjet_cut=Bjets < 1;
   bool njets_cut=njets >= 2;
   bool cjv_cut=CJV < 20000;
   bool mcut= mtt/1000 < (mZ/1000 -25);
   bool mjj_cut=mjj > 200000;
   bool dyjj_cut= DYjj > 2.1;

   //fiducial
   bool olv_cut = olv == 1;
   bool jet_pt_cut=subJetpt > 30000;
   bool lead_lep_cut=lead_lep_pt > 20000;
   bool sub_lead_cut= sub_lead_lep_pt > 15000;
   bool mll_cut=mll > 10000;
   bool lep_eta_cut= lead_lep_eta< 2.5;
   bool drll_cut= drll > 0.1;
   bool jet_eta_cut= lead_jet_eta < 4.5;
   bool met_cut = met > 20000;
   bool OF=(int)lead_lep_flavor != (int)sub_lep_flavor;
   bool fiducial_cut=olv_cut &&jet_pt_cut && lead_lep_cut && sub_lead_cut &&
     mll_cut && lep_eta_cut &&jet_eta_cut && met_cut && njets_cut && OF && cjv_cut;
   
   bool sr_sum=bjet_cut && bjet_cut && cjv_cut &&
     mcut && mjj_cut && dyjj_cut
     && olv_cut && jet_pt_cut;
   
   bool sr_cut=inSR==1;
   bool top_cut=inTopCR==1;
   bool zjets_cut=inZjetsCR==1;
   bool ww_cut=inWWCR==1;

   if(!fiducial_cut)
     continue;
  

    f_hist->Fill(mjj/1000,weight);
 }

 return f_hist;
}

TTree* LoadTree(string tree_file,string tree_name){
  TFile * root_file=TFile::Open(tree_file.c_str());
  TTree * data_tree=(TTree*)root_file->Get(tree_name.c_str());
  //data_tree->SetDirectory(0);
  //root_file->Close();
  //delete root_file;
  return data_tree;
  
}

void SaveHist(TH1F * hist, string name){
  TFile * f= new TFile(name.c_str(),"RECREATE");
  f->cd();
  hist->Write();
  f->Write();
  f->Close();
  delete f;
}

TH1F * LoadTruthData(string feature, int bins, double * binning){
  string file="ntupls/truth/ntuple_HiggspT.root";
  TTree * hww_em=LoadTree(file,"HWW_em");
  TTree * hww_me=LoadTree(file,"HWW_me");
  double GevCorreation=1;
  if(substring(feature,"Mjj") ||substring(feature,"Mll") || substring(feature,"MET")) {
      feature=feature+"_truth";
    }
  TH1F * truth_hist_em=LoopTree(hww_em,feature,bins,binning,feature);
  TH1F * truth_hist_me=LoopTree(hww_me,feature,bins,binning,feature);

  /*
  float em_data;
  float me_data;
  double em_weight;
  double me_weight;
  
  hww_em->SetBranchAddress(feature.c_str(),&em_data);
  hww_me->SetBranchAddress(feature.c_str(),&me_data);

  hww_em->SetBranchAddress("weight",&em_weight);
  hww_me->SetBranchAddress("weight",&me_weight);

  TH1F * truth_hist_em=new TH1F((feature+"em_truth").c_str(),(feature+"_truth").c_str(),bins,binning);
  TH1F * truth_hist_me=new TH1F((feature+"me_truth").c_str(),(feature+"_truth").c_str(),bins,binning);

  int em_nEvents=hww_em->GetEntries();
  int me_nEvents=hww_me->GetEntries();

  for(int i=0;i < em_nEvents;i++){
    hww_em->GetEvent(i);
    truth_hist_em->Fill(em_data);
  }
  for(int i=0;i < me_nEvents;i++){
    hww_me->GetEvent(i);
    truth_hist_me->Fill(me_data);

  }
  DrawHist<TH1F>(truth_hist_em);
  */
  truth_hist_em->Add(truth_hist_me);

  return truth_hist_em;
  
}
