#include <algorithm>
#include <vector>
#include "TH1F.h"
#include "TH1.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TVectorD.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include <cstring>
#include "TMath.h"
#include <memory>
#include <fstream>
#include <TTree.h>
#include "TH2F.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"
#include "merge.h"
#include <unordered_map>
#include <TPaveLabel.h>
#include "unfold.h"
#include "validation_plots.h"
#include "tools.h"
using namespace TMath;
using namespace std;

string get_truth_name(string reco_name){
  std::unordered_map<std::string,std::string> truth_names;
  truth_names["Mjj"]="Mjj_truth";
  truth_names["Mll"]="Mll_truth";
  truth_names["DYjj"]="DYjj_truth";
  truth_names["DYll"]="DYll_truth";
  truth_names["pt_H"]="HIGGS_pT";
  truth_names["DPhill"]="DPhill_truth";
  truth_names["lep0_pt"]="lep0_pT";
  truth_names["lep1_pt"]="lep1_pT";
  truth_names["jet0_pt"]="jet0_pT";
  truth_names["jet1_pt"]="jet1_pT";
  truth_names["SignedDPhijj"]="SignedDPhijj_truth";
  truth_names["costhetastar"]="cosThetaStar_truth";
  truth_names["Ptll"]="Ptll_truth";
  truth_names["jet0_eta"]="jet0_y";
  truth_names["jet1_eta"]="jet1_y";
  truth_names["DPhijj"]="SignedDPhijj_truth";
  return truth_names[reco_name];
}

void draw_2d_bins(TH2F * hist,string dist){
  
  vector<TH1F*> oneProjections;
  int x_bins=hist->GetNbinsX();
  int y_bins=hist->GetNbinsY();
  for(int xbin=1;xbin<x_bins;xbin++){
    if(xbin <75) continue;
    oneProjections.push_back(new TH1F((dist+to_string(xbin)).c_str(),(dist+" "+to_string(xbin)).c_str(),distribution_maps[dist].bins,distribution_maps[dist].binning));
    for(int ybin=1;ybin<x_bins;ybin++){
      oneProjections[oneProjections.size()-1]->SetBinContent(ybin,hist->GetBinContent(xbin,ybin));
    }
    DrawHist<TH1F>(oneProjections[oneProjections.size()-1]);
  }
}

void normalize_2d_bin_width(TH2F * hist,string dist,string bdt_dist){

  int x_bins=hist->GetNbinsX();
  int y_bins=hist->GetNbinsY();
  double bin_width_x;
  double bin_width_y;
  double bin_area;
  for(int xbin=1;xbin<=x_bins;xbin++){
    for(int ybin=1;ybin<=y_bins;ybin++){
      bin_width_x=distribution_maps[bdt_dist].binning[xbin]-distribution_maps[bdt_dist].binning[xbin-1];
      bin_width_y=distribution_maps[dist].binning[ybin]-distribution_maps[dist].binning[ybin-1];
      if(bin_width_y>100)
        bin_width_y=bin_width_y/1000;
      bin_area=bin_width_x*bin_width_y;
      hist->SetBinContent(xbin,ybin,hist->GetBinContent(xbin,ybin)/bin_area);
    }
  }

}
struct sr_obs_binning get_binning(string obs){
  std::unordered_map<std::string,struct sr_obs_binning> bin_maps;
  bin_maps["Mjj"]={10,200000,600000};
  bin_maps["MT"]={10,15000,260000};
  bin_maps["Mll"]={10,10000,200000};
  bin_maps["costhetastar"]={10,0,1};
  bin_maps["DYll"]={10,0,2.5};
  bin_maps["DYjj"]={10,2.1,9};
  bin_maps["DPhill"]={10,0,3.1416};
  bin_maps["DPhijj"]={10,0,3.1416};
  bin_maps["SignedDPhijj"]={10,-3.1416,3.1416};
  bin_maps["pt_H"]={10,0,1000000};
  bin_maps["Ptll"]={10,0,500000};
  bin_maps["lep0_pt"]={10,22000,500000};
  bin_maps["lep1_pt"]={10,15000,200000};
  bin_maps["jet0_pt"]={10,30000,700000};
  bin_maps["jet1_pt"]={10,30000,350000};
  bin_maps["jet0_eta"]={10,-5,5};
  bin_maps["jet1_eta"]={10,-5,5};
  
  return bin_maps[obs];
}

void twoDHistogramsFiducial(std::string dist,TTree* truth,TTree * reco,string bdt_dist){

  //auto binning=get_binning(dist);
  
  TH2F *fakes=new TH2F(("fakes_"+bdt_dist+"_"+dist).c_str(),(bdt_dist+"  v. "+dist+"- Fakes").c_str(),distribution_maps[bdt_dist].bins,distribution_maps[bdt_dist].binning,distribution_maps[dist].bins,distribution_maps[dist].binning);

  TH2F * double_diff=new TH2F(("double_"+bdt_dist+"_"+dist).c_str(),(bdt_dist+" v. "+dist+"-Fiducial/Bin Area").c_str(),distribution_maps[bdt_dist].bins,distribution_maps[bdt_dist].binning,distribution_maps[dist].bins,distribution_maps[dist].binning);
  std::string reco_dist=dist;
  std::string truth_dist=get_truth_name(dist);

  draw_histograms(truth,reco,truth_dist,reco_dist, double_diff,fakes,bdt_dist);
  double_diff->SetStats(0);
  fakes->SetStats(0);
  draw_2d_bins(double_diff,dist);
  normalize_2d_bin_width(double_diff,dist,bdt_dist);
  normalize_2d_bin_width(fakes,dist,bdt_dist);
  double_diff->GetXaxis()->SetTitle(bdt_dist.c_str());
  double_diff->GetYaxis()->SetTitle(dist.c_str());
  DrawHist<TH2F>(double_diff);
  DrawHist<TH2F>(fakes);
  delete double_diff;
  delete fakes;
 
}

void draw_histograms(TTree * truth_tree,TTree * reco_tree,string truth_dist,string reco_dist, TH2F * double_diff,TH2F * fakes,string bdt_dist){
  TH1F *fiducial_bdt=new TH1F((bdt_dist+"_fid").c_str(),(bdt_dist+" fiducial").c_str(),distribution_maps[bdt_dist].bins,distribution_maps[bdt_dist].binning);
  TH1F *sr_bdt=new TH1F((bdt_dist+"_sr").c_str(),(bdt_dist+" sr").c_str(),distribution_maps[bdt_dist].bins,distribution_maps[bdt_dist].binning);

  
  TH2F *double_sr=(TH2F*)double_diff->Clone();
  double_sr->SetName((string(double_diff->GetName())+"_reco").c_str());
  double_sr->SetTitle((truth_dist+" vs "+bdt_dist+" - reco/ Bin Area").c_str());
  double_sr->SetStats(0);
  double_sr->GetXaxis()->SetTitle(bdt_dist.c_str());
  double_sr->GetYaxis()->SetTitle(reco_dist.c_str());
  std::unordered_map<int,int> truth_map=FillRunNumber(truth_tree,"event_number",true);
  std::unordered_map<int,int> reco_map=FillRunNumber(reco_tree,"eventNumber",false);
  float truth_data;
  float detector_data;
  float vbf_bdt;
  double truth_weight;
  double reco_weight;
  float inSR;

  truth_tree->SetBranchAddress(truth_dist.c_str(), &truth_data);
  reco_tree->SetBranchAddress(reco_dist.c_str(), &detector_data);
  reco_tree->SetBranchAddress(bdt_dist.c_str(), &vbf_bdt);
  truth_tree->SetBranchAddress("weight", &truth_weight);
  reco_tree->SetBranchAddress("weight",&reco_weight);
  reco_tree->SetBranchAddress("inSR",&inSR);

  std::cout<<"true tree"<<truth_tree->GetEntries()<<"truth map"<<truth_map.size()<<"\n";
  std::cout<<" reco tree"<<reco_tree->GetEntries()<<"reco map"<<reco_map.size()<<"\n";

  float corr=1000;
  bool TruthMatch=false;
  bool RecoMatch = false;
  int missed_events=0;
  int matched_events=0;
  int fake_events=0;
  for (auto & truth_event :truth_map ){
    if(reco_dist.compare("DPhijj")==0)
      truth_data=abs(truth_data);
    TruthMatch=true;
    truth_tree->GetEvent(truth_event.second);
    int eventNumber=truth_event.first;
    RecoMatch=IsInMap(reco_map,eventNumber);
    if(RecoMatch){
      reco_tree->GetEvent(reco_map[truth_event.first]);
      if((int)inSR!=1) continue;
      fiducial_bdt->Fill(vbf_bdt);
      double_diff->Fill(vbf_bdt,truth_data,truth_weight);
      double_sr->Fill(vbf_bdt,detector_data,reco_weight);
      matched_events++;
    }
    else{
      missed_events++;
    }
  }
  for (auto & reco_event :reco_map ){
    RecoMatch=true;
    reco_tree->GetEvent(reco_event.second);
    if((int)inSR!=1) continue;
    int eventNumber=reco_event.first;
    TruthMatch=IsInMap(truth_map,eventNumber);
    if(!TruthMatch){
      fakes->Fill(vbf_bdt,detector_data,reco_weight);
      //double_sr->Fill(vbf_bdt,detector_data,reco_weight);
      fake_events++;
    }
  }
  truth_map.clear();
  reco_map.clear();
  truth_tree->ResetBranchAddresses();
  reco_tree->ResetBranchAddresses();
  double_sr->Add(fakes);
  normalize_2d_bin_width(double_sr,reco_dist,bdt_dist);
  DrawHist<TH2F>(double_sr);
  DrawError<TH1F>(fiducial_bdt);
  /*
  TH2F *c_factor=(TH2F*)double_diff->Clone();
  c_factor->SetName((string(double_diff->GetName())+"_c_factors").c_str());
  c_factor->SetTitle((truth_dist+" vs vbf_bdt - c factor").c_str());
  c_factor->SetStats(0);
  c_factor->GetXaxis()->SetTitle("BDT VBF");
  c_factor->GetYaxis()->SetTitle(reco_dist.c_str());
  c_factor->Divide(double_sr);
  c_factor->SetMaximum(5);
  //DrawHist<TH2F>(c_factor);
  */
}
int eval_vbf_region(double bdt_vbf){
 
  int i=0;
  int region=0;
  region=region | (bdt_vbf < 0.50)<<i++;  
  region=region | (bdt_vbf >= 0.50 and bdt_vbf < 0.7)<<i++;
  region=region | (bdt_vbf >= 0.7 and bdt_vbf < 0.86)<<i++;
  region=region | (bdt_vbf >= 0.86 and bdt_vbf < 0.94)<<i++;
  region=region | (bdt_vbf >= 0.94 and bdt_vbf < 1.00)<<i++;
  
  return (int)log2(region);
    

}
int eval_topWW_region(double bdt_topww){
  int i=0;
  int region=0;
  region=region | (bdt_topww >= -1.0 and bdt_topww < -0.666)<<i++;  
  region=region | (bdt_topww >= -0.666 and bdt_topww < -0.333)<<i++;
  region=region | (bdt_topww >= -0.333 and bdt_topww < 0)<<i++;
  region=region | (bdt_topww >= 0.0 and bdt_topww < 0.333)<<i++;
  region=region | (bdt_topww >= 0.333 and bdt_topww < 0.666)<<i++;
  region=region | (bdt_topww >= 0.666 and bdt_topww < 1.0)<<i++;
  
  return (int)log2(region);


}
vector<TH1F*> draw_bdt_cuts(TTree *truth_tree,TTree* reco_tree,string dist,bool truth,string bdt_dist){

  vector<string> regions={"bdt_vbf<0.50","bdt_vbf >= 0.50 and bdt_vbf < 0.70","bdt_vbf >= 0.70 and bdt_vbf < 0.86","bdt_vbf >= 0.86 and bdt_vbf < 0.94","bdt_vbf >= 0.94 and bdt_vbf < 1.00"};
  //vector<string> regions={"bdt_vbf<0.50","bdt_vbf >= 0.50 and bdt_vbf < 0.86","bdt_vbf >= 0.86 and bdt_vbf < 1.00"};
  
  if(bdt_dist.compare("bdt_TopWWAll")==0){
    regions={"bdt_topww >= -1.0 and bdt_topww < -0.666","bdt_topww >= -0.666 and bdt_topww < -0.333","bdt_topww >= -0.333 and bdt_topww < 0","bdt_topww >= 0.0 and bdt_topww < 0.333","bdt_topww >= 0.333 and bdt_topww < 0.666","bdt_topww >= 0.666 and bdt_topww < 1.0"};
  }
  int number_of_regions=regions.size();
  vector<TH1F*> hist_cuts(number_of_regions);
  TH1F * total_events=new TH1F("total_events","total events",distribution_maps[dist].bins,distribution_maps[dist].binning);
  for(int i=0;i<number_of_regions;i++){
    if(truth)
      hist_cuts[i]=new TH1F((regions[i]+"_truth").c_str(),regions[i].c_str(),distribution_maps[dist].bins,distribution_maps[dist].binning);
    else
      hist_cuts[i]=new TH1F((regions[i]+"_reco").c_str(),regions[i].c_str(),distribution_maps[dist].bins,distribution_maps[dist].binning);
  }
  
   cout<<"creating maps\n";
  std::unordered_map<int,int> truth_map=FillRunNumber(truth_tree,"event_number",true);
  std::unordered_map<int,int> reco_map=FillRunNumber(reco_tree,"eventNumber",false);
  cout<<"setting data\n";
  float truth_data;
  float reco_data;
  float bdt;
  double truth_weight;
  double reco_weight;
  float inSR;
  float jet0_pt=0;
  float jet1_pt=0;
  float Mjj=0;
  float DPhill=0;
  float DYjj=0;
  
  truth_tree->SetBranchAddress(get_truth_name(dist).c_str(), &truth_data);
  reco_tree->SetBranchAddress(dist.c_str(), &reco_data);
  truth_tree->SetBranchAddress("jet0_pT", &jet0_pt);
  truth_tree->SetBranchAddress("jet1_pT", &jet1_pt);
  truth_tree->SetBranchAddress("Mjj_truth", &Mjj);
  truth_tree->SetBranchAddress("DYjj_truth", &DYjj);
  truth_tree->SetBranchAddress("DPhill_truth", &DPhill);

  reco_tree->SetBranchAddress(bdt_dist.c_str(), &bdt);
  truth_tree->SetBranchAddress("weight", &truth_weight);
  reco_tree->SetBranchAddress("weight",&reco_weight);
  reco_tree->SetBranchAddress("inSR",&inSR);
  // if(dist.compare("DPhill")!=0)
  //  reco_tree->SetBranchAddress("DPhill",&DPhill);
  // if(dist.compare("Mjj")!=0)
  //  reco_tree->SetBranchAddress("Mjj",&Mjj);
  bool TruthMatch=false;
  bool RecoMatch = false;
  int missed_events=0;
  double matched_events=0;
  double events_above_7=0;
  double events_above_8=0;
  double events_above_9=0;
  
  double fake_events=0;
  int current_region;
  cout<<"looping over truth map\n";
  vector<double> mjj_phase_space_1={0,0,0,0,0};
  vector<double> mjj_phase_space_2={0,0,0,0,0};
  vector<double> jet0pt_phase_space_1={0,0,0,0,0};
  vector<double> jet0pt_phase_space_2={0,0,0,0,0};
  vector<double> jet0pt_phase_space_3={0,0,0,0,0};

  vector<double> jet1pt_phase_space_1={0,0,0,0,0};
  vector<double> jet1pt_phase_space_2={0,0,0,0,0};

  vector<double> dyjj_phase_space_1={0,0,0,0,0};
  vector<double> dyjj_phase_space_2={0,0,0,0,0};

  vector<double> dphill_phase_space_1={0,0,0,0,0};
  vector<double> dphill_phase_space_2={0,0,0,0,0};

  double mjj_phase_space_1_total=0;
  double mjj_phase_space_2_total=0;
  double jet0pt_phase_space_1_total=0;
  double jet0pt_phase_space_2_total=0;
  double jet0pt_phase_space_3_total=0;

  double jet1pt_phase_space_1_total=0;
  double jet1pt_phase_space_2_total=0;
  double dyjj_phase_space_1_total=0;
  double dyjj_phase_space_2_total=0;

  double dphill_phase_space_1_total=0;
  double dphill_phase_space_2_total=0;


  
  for (auto & truth_event :truth_map ){
    TruthMatch=true;
    truth_tree->GetEvent(truth_event.second);
    if(dist.compare("DPhijj")==0){
      truth_data=abs(truth_data);
      cout<<"taking absolute value\n";
    }
    int eventNumber=truth_event.first;
    RecoMatch=IsInMap(reco_map,eventNumber);
    if(RecoMatch){
      reco_tree->GetEvent(reco_map[truth_event.first]);
      if((int)inSR!=1) continue;
      if(Mjj <450000 && dist.compare("Mjj")!=0) continue;
      if(DPhill>1.4 &&  dist.compare("DPhill")!=0) continue;
      if(reco_data<450000 && dist.compare("Mjj")==0) continue;
      if(reco_data >1.4 && dist.compare("DPhill")==0) continue;
      
      if(bdt_dist.compare("bdt_vbf")==0)
        current_region=eval_vbf_region(bdt);
      else if(bdt_dist.compare("bdt_TopWWAll")==0)
          current_region=eval_topWW_region(bdt);
      else{
          cout<<dist<<" not valid bdt\n";
          break;
        }
      if(truth && (DPhill<1.4) ){
        if(Mjj<450000){
          mjj_phase_space_1[current_region]+=truth_weight;
          mjj_phase_space_1_total+=truth_weight;
        }
        if(Mjj<1000000){
          mjj_phase_space_2[current_region]+=truth_weight;
          mjj_phase_space_2_total+=truth_weight;
        }
        if(DYjj<3.5){
          dyjj_phase_space_1[current_region]+=truth_weight;
          dyjj_phase_space_1_total+=truth_weight;
        }
        if(DYjj<4.5){
          dyjj_phase_space_2[current_region]+=truth_weight;
          dyjj_phase_space_2_total+=truth_weight;
        }
        if(jet0_pt<40000){
          jet0pt_phase_space_1[current_region]+=truth_weight;
          jet0pt_phase_space_1_total+=truth_weight;
        }
        if(jet0_pt<50000){
          jet0pt_phase_space_2[current_region]+=truth_weight;
          jet0pt_phase_space_2_total+=truth_weight;
        }
        if(jet0_pt<100000){
          jet0pt_phase_space_3[current_region]+=truth_weight;
          jet0pt_phase_space_3_total+=truth_weight;
        }
        if(jet1_pt<40000){
          jet1pt_phase_space_1[current_region]+=truth_weight;
          jet1pt_phase_space_1_total+=truth_weight;
        }  
        if(jet1_pt<50000){
          jet1pt_phase_space_2[current_region]+=truth_weight;
          jet1pt_phase_space_2_total+=truth_weight;
        }        
        if(DPhill>1.4){
          dphill_phase_space_1[current_region]+=truth_weight;
          dphill_phase_space_1_total+=truth_weight;
        }
        if(DPhill>1.57){
          dphill_phase_space_2[current_region]+=truth_weight;
          dphill_phase_space_2_total+=truth_weight;
        }
        hist_cuts[current_region]->Fill(truth_data,truth_weight);
        total_events->Fill(truth_data,truth_weight);
      }
      else if(!truth){ //if reco
        hist_cuts[current_region]->Fill(reco_data,reco_weight);
        total_events->Fill(reco_data,reco_weight);
      }
    }
    else{
      missed_events++;
    }
  }
  if(truth){
    std::cout<<"mjj phase space 1 \n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<mjj_phase_space_1[i]<<"\n";
    std::cout<<"mjj phase space 1 total\n";
    std::cout<<mjj_phase_space_1_total<<"\n";
    std::cout<<"*******************\n";
    
    std::cout<<"mjj phase space 2 \n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<mjj_phase_space_2[i]<<"\n";
    std::cout<<"mjj phase space 2 total\n";
    std::cout<<mjj_phase_space_2_total<<"\n";
    std::cout<<"*******************\n";

    std::cout<<"jet0 pt phase space 1\n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<jet0pt_phase_space_1[i]<<"\n";
    std::cout<<"jet0 phase space 1 total\n";
    std::cout<<jet0pt_phase_space_1_total<<"\n";
    std::cout<<"*******************\n";

    std::cout<<"jet0 pt phase space 2\n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<jet0pt_phase_space_2[i]<<"\n";
    std::cout<<"jet0 phase space 2 total\n";
    std::cout<<jet0pt_phase_space_2_total<<"\n";
    std::cout<<"*******************\n";

    std::cout<<"jet0 pt phase space 3\n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<jet0pt_phase_space_3[i]<<"\n";
    std::cout<<"jet0 phase space 3 total\n";
    std::cout<<jet0pt_phase_space_3_total<<"\n";
    std::cout<<"*******************\n";
    
    std::cout<<"jet1 pt phase space 1\n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<jet1pt_phase_space_1[i]<<"\n";
    std::cout<<"jet1 phase space 1 total\n";
    std::cout<<jet1pt_phase_space_1_total<<"\n";
    std::cout<<"*******************\n";

        std::cout<<"jet1 pt phase space 2\n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<jet1pt_phase_space_2[i]<<"\n";
    std::cout<<"jet1 phase space 2 total\n";
    std::cout<<jet1pt_phase_space_2_total<<"\n";
    std::cout<<"*******************\n";

    
    std::cout<<"DYjj phase 1 space \n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<dyjj_phase_space_1[i]<<"\n";
    std::cout<<"DYjj phase space 1 total\n";
    std::cout<<dyjj_phase_space_1_total<<"\n";
    std::cout<<"*******************\n";

    std::cout<<"DYjj phase 2 space \n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<dyjj_phase_space_2[i]<<"\n";
    std::cout<<"DYjj phase 2 space total\n";
    std::cout<<dyjj_phase_space_2_total<<"\n";
    std::cout<<"*******************\n";

    std::cout<<"DPhill phase space 1\n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<dphill_phase_space_1[i]<<"\n";
    std::cout<<"DPhill phase space 1 total\n";
    std::cout<<dphill_phase_space_1_total<<"\n";
    std::cout<<"*******************\n";

    std::cout<<"DPhill phase space 2\n";
    for(int i=0;i<5;i++)
      std::cout<<"Bin "<<i<<" Events "<<dphill_phase_space_2[i]<<"\n";
    std::cout<<"DPhill phase space 2 total\n";
    std::cout<<dphill_phase_space_2_total<<"\n";
    std::cout<<"*******************\n";

    
    std::cout<<"total events\n";
    std::cout<<total_events->Integral()<<"\n";
  }
  for (auto & reco_event :reco_map ){
    RecoMatch=true;
    reco_tree->GetEvent(reco_event.second);
    if((int)inSR!=1) continue;

    int eventNumber=reco_event.first;
    TruthMatch=IsInMap(truth_map,eventNumber);
    if(!TruthMatch){
      if(!truth){
        current_region=eval_topWW_region(bdt);
        //hist_cuts[current_region]->Fill(reco_data,reco_weight);
        //total_events->Fill(reco_data,reco_weight);
      }
    }
  }
  std::cout<<bdt_dist<<"\n";
  for(int i=0;i<number_of_regions;i++){
    std::cout<<regions[i]<<"\n";
    std::cout<<hist_cuts[i]->Integral()<<"\n";
   
  }
  for(int i=0;i<number_of_regions;i++){
    //double bin_width=(binning.high-binning.low)/(double)binning.bins;
    //if (bin_width > 100) bin_width=bin_width/1000;
    // hist_cuts[i]->Scale(1.0/hist_cuts[i]->Integral());
    for(int j=0;j<=hist_cuts[i]->GetNbinsX();j++){
      //hist_cuts[i]->SetBinContent(j,hist_cuts[i]->GetBinContent(j)/bin_width);
      //fiducial->SetBinError(i,pow(fiducial->GetBinContent(i)/bin_width,0.5));
      hist_cuts[i]->SetBinError(j,hist_cuts[i]->GetBinContent(j)*0.00001);
    }
  }
 
  //total_events->Scale(1.0/total_events->Integral());
  hist_cuts.push_back(total_events);
 
  truth_map.clear();
  reco_map.clear();
  truth_tree->ResetBranchAddresses();
  reco_tree->ResetBranchAddresses();

  //hist_cuts.erase(hist_cuts.begin());
  return hist_cuts;

}
void bdt_cut_skew(TTree* truth_tree,TTree* reco_tree,string dist,string bdt_dist){
  vector<TH1F*> hist_cuts=draw_bdt_cuts(truth_tree,reco_tree,dist,true,bdt_dist);
  vector<TH1F*> hist_cuts_reco=draw_bdt_cuts(truth_tree,reco_tree,dist,false,bdt_dist);

  TH1F * base =new TH1F((dist).c_str(),(dist).c_str(),distribution_maps[dist].bins,distribution_maps[dist].binning);
  base->GetYaxis()->SetRangeUser(0,20);
  TCanvas *cDraw= new TCanvas("cDraw", "cDraw",900,900);
  cDraw->cd();
  base->SetStats(0);
  TPad *pad1 = new TPad("pad1", "pad1",0.,.3,1.,1);
  pad1->Draw();
  pad1->cd();
  pad1->SetBottomMargin(0);
  base->SetTitle(dist.c_str());
  TH1F * bottom_base=(TH1F*)base->Clone();
  base->GetYaxis()->SetTitle("Estimated Data Events");
  base->Draw("E1");
  int color=1;
  vector<TH1F*> clones;
  for(int i=0;i<hist_cuts.size()-1;i++){
    if (color==5) color++;
    hist_cuts[i]->SetLineColor(color++);
    if(i >= 1 or  bdt_dist.compare("bdt_TopWWAll")==0)
      hist_cuts[i]->Draw("E1 SAME");
    clones.push_back((TH1F*)hist_cuts[i]->Clone());
   }
  gPad->BuildLegend(0.65,0.65,0.95,0.95,"");
  cDraw->cd();
  TPad *pad2 = new TPad("pad2", "pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.2);
  pad2->SetGridx(); // vertical grid
  pad2->Draw();
  pad2->cd();
  color=0;
  bottom_base->SetTitle("Ratio To Total SR");
  bottom_base->GetYaxis()->SetRangeUser(0,1);
  bottom_base->Draw("E1");
  for(int i=0;i<hist_cuts.size()-1;i++){
    clones[i]->Divide(hist_cuts[hist_cuts.size()-1]);
    if(i>=1 or bdt_dist.compare("bdt_TopWWAll")==0)
      clones[i]->Draw("E1 SAME");
  }
  cDraw->Print((dist+"bdt_cuts_truth_"+bdt_dist+".pdf").c_str());
  //delete pad1;
  //delete pad2;
  delete cDraw;

    /***************** reco ********************************/

  cout<<"starting reco\n";
  base->GetYaxis()->SetRangeUser(0,20);
  TCanvas *cDraw_reco= new TCanvas("cDraw_reco", "cDraw",900,900);
  cout<<"creating pads\n";
  cDraw_reco->cd();
  TPad* pad1_reco = new TPad("pad1_r", "pad1_r",0.,.3,1.,1);
  pad1_reco->Draw();
  pad1_reco->cd();
  pad1_reco->SetBottomMargin(0);
  base->SetStats(0);
  base->SetTitle((dist+" reco").c_str());
  base->Draw("E1");
  color=1;
  vector<TH1F*> reco_clones;

  cout<<"looping reco histograms\n";
  for(int i=0;i<hist_cuts_reco.size()-1;i++){
    if(color==5) color++;
    hist_cuts_reco[i]->SetLineColor(color++);
    if(i>=1 or bdt_dist.compare("bdt_TopWWAll")==0)
      hist_cuts_reco[i]->Draw("E1 SAME");
    reco_clones.push_back((TH1F*) hist_cuts_reco[i]->Clone());
  }
  cout<<"building legend\n";
  gPad->BuildLegend(0.65,0.65,0.95,0.95,"");
  cDraw_reco->cd();
  TPad* pad2_reco = new TPad("pad2_r", "pad2_r",0,0.05,1,0.3);
  pad2_reco->SetTopMargin(0.0);
  pad2_reco->SetBottomMargin(0.25);
  pad2_reco->SetGridx(); // vertical grid
  pad2_reco->Draw();
  pad2_reco->cd();
  color=1;
  bottom_base->SetTitle("Ratio To Total SR");
  bottom_base->GetYaxis()->SetRangeUser(0,1);
  cout<<"looping divided clones\n";

  bottom_base->Draw("E1");
  for(int i=0;i<hist_cuts.size()-1;i++){
    if (color==5) color++;
    reco_clones[i]->Divide(hist_cuts_reco[hist_cuts_reco.size()-1]);
    if(i>=1 or bdt_dist.compare("bdt_TopWWAll")==0)
      reco_clones[i]->Draw("E1 SAME");
  }
  cDraw_reco->Print((dist+"bdt_cuts_reco"+bdt_dist+".pdf").c_str());
  delete cDraw_reco;
  //delete pad1_reco;
  //delete pad2_reco;
    
  /***************** C factors ********************************/
  cout<<"starting c factors\n";
  base->GetYaxis()->SetRangeUser(0.6,1.8);
  TCanvas *cDraw_c_factor= new TCanvas("cDraw_c_factor", "cDraw",900,900);
  cDraw_c_factor->cd();
  base->SetStats(0);
  base->SetTitle((dist+" Reco / Truth").c_str());
  base->GetYaxis()->SetTitle("Reco / Truth");
  base->Draw("E1");
  for(int i=0;i<hist_cuts.size()-1;i++){
    TH1F * c_cuts=(TH1F*)hist_cuts[i]->Clone();
    c_cuts->Divide(hist_cuts_reco[i]);
    c_cuts->SetLineColor(hist_cuts_reco[i]->GetLineColor());
    if(i>=1 or bdt_dist.compare("bdt_TopWWAll")==0)
      c_cuts->Draw("E1 SAME");
  }
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  TPad* pad2_cfact = new TPad("pad2_cfact", "pad2_cfact",0,0.05,1,0.3);
  pad2_cfact->SetTopMargin(0.0);
  pad2_cfact->SetBottomMargin(0.25);
  pad2_cfact->SetGridx(); // vertical grid
  pad2_cfact->Draw();
  pad2_cfact->cd();
  bottom_base->SetTitle("Ratio To Total C Factors");
  bottom_base->GetYaxis()->SetRangeUser(0.6/.4,1.4/0.4);
  cout<<"looping divided clones\n";
  bottom_base->Draw("E1");
  for(int i=0;i<hist_cuts.size()-1;i++){
    TH1F * c_cuts=(TH1F*)hist_cuts[i]->Clone();
    c_cuts->Divide(hist_cuts_reco[i]);
    c_cuts->SetLineColor(hist_cuts_reco[i]->GetLineColor());
    if(i>1 or bdt_dist.compare("bdt_TopWWAll")==0){
      plot_cfactor_ratio(c_cuts,dist);
      c_cuts->Draw("E1 SAME");
    }
  }
  cDraw_c_factor->Print((dist+"bdt_cuts_c_factor"+bdt_dist+".pdf").c_str());

  
  delete cDraw_c_factor;

   for (auto p : hist_cuts)
     delete p;
   hist_cuts.clear();

   for (auto p : hist_cuts_reco)
     delete p;
   hist_cuts_reco.clear();
  
}
void plot_cfactor_ratio(TH1F * hist,string dist){
  TFile * c_factor_file=TFile::Open("c_factor_hists.root");
  std::map<string,string> c_file_naming;
  c_file_naming["costhetastar"]="cosstar";
  c_file_naming["pt_H"]="higgs_pt";
  c_file_naming["lep0_pt"]="Leading_lep";
  c_file_naming["jet0_pt"]="Leading_jet";
  c_file_naming["jet1_pt"]="sub_jet";
  c_file_naming["lep1_pt"]="sub_lep";
  c_file_naming["DYjj"]="DYjj";
  c_file_naming["DYll"]="DYll";
  c_file_naming["Mll"]="Mll";
  c_file_naming["Mjj"]="Mjj";
  c_file_naming["SignedDPhijj"]="DPhijj";
  c_file_naming["DPhill"]="DPhill";
  string ratio_hist_name=c_file_naming[dist]+"_Ratio";
  cout<<"getting ratio hist from file \n";
  cout<<ratio_hist_name<<"\n";
  TH1F * c_factor_hist=(TH1F*)c_factor_file->Get(ratio_hist_name.c_str());
  cout<<"dividing hists \n";

  hist->Divide(c_factor_hist);
  //DrawHist<TH1F>(c_factor_hist);
  //DrawHist<TH1F>(hist);
  cout<<"closing file \n";
  //delete c_factor_hist;
  c_factor_file->Close();
  //delete c_factor_file;


}


