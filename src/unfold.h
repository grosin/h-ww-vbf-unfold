#ifndef UNFOLD
#define UNFOLD

#include <algorithm>
#include <vector>
#include "TRandom3.h"
#include "TH1F.h"
#include "TH1.h"
#include "RooUnfoldBayes.h"
#include "RooUnfoldSvd.h"
#include "RooUnfoldBinByBin.h"
#include "RooUnfoldDagostini.h"
#include "RooUnfoldInvert.h"
#include <TH2F.h>
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include "TVectorD.h"
#include "RooUnfoldResponse.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TMatrixD.h"
#include "TCut.h"
#include "TTree.h"
#include <map>
#include <string>
#include <tuple>
#include <unordered_map>
extern const int nbins;
extern int min_bin;
extern int max_bin;
//prototypes

/****** main functions *************/
RooUnfoldResponse  UnfoldDist(int iterations,const char * dist,std::string merged_file="",bool Draw=false);
RooUnfoldResponse UnfoldSVD(int regularization,const char * dist, std::string merged_file,bool draw);
RooUnfoldResponse UnfoldInPlace(std::string dist,TTree* truth,TTree * reco);
TH1F * unfold_data(TH1F * data_hist,RooUnfoldResponse & response, int iters );
void fill_map();

/****** create response matrices *************/
RooUnfoldResponse create_response(TTree *data_tree, const char * truth_distribution,const char * detector_distribution,TH1F * truth_hist,TH1F * reco_hist);
RooUnfoldResponse create_response(TTree * truth_tree, TTree * reco_tree, std::string truth_distribution, std::string reco_distribution, TH1 * truth_hist,TH1 * reco_hist);
TH1F* fill_hist(TTree *data_tree,std::string dist,std::string name,bool bdt_weight=false);
void fill_background(RooUnfoldResponse & response,TTree * background,std::string feature);
void Fill_Fakes(TTree *merged,TTree *reco_tree,RooUnfoldResponse &matrix,TH1F *reco_hist,std::string feature);

/****** unfold *************/
RooUnfoldBayes unfold_bayes(RooUnfoldResponse &response, TH1* data,int iterations);
RooUnfoldSvd unfold_svd(RooUnfoldResponse &response, TH1* data,int iterations);
RooUnfoldBinByBin unfold_BinByBin(RooUnfoldResponse &response, TH1* reco);
RooUnfoldInvert unfold_invert(RooUnfoldResponse &response, TH1* data);

/****** draw *************/
void print_and_draw(TH1F *unfold, TH1F * TestHist, TH1F * RecoHist,const char *title ="\0");

/********* utility functions *****************/
void save_hist(TH1F * h, const char * filename,int iteration);
void print_matrix(const TMatrixD &response, const char *);
void print_matrix(TH2 *response,const char * distribution);
std::vector<double> GetMinMax(TTree * data_tree,const char * distribution);
double calculate_residuals(TH1F * unfolded, TH1F *truth);
double calculate_chi2(TH1F * , TH1F *,float sigma,std::string dist="undefined",int iterations=1);
void save_bin_error(TH1F* unfolded,TH1F* truth,std::string name,int iteration);
void reroll_hists(TH1F *truth, TH1F* reco,float sigma,RooUnfoldResponse & response);
void reweight_hists(TH1F *truth,float sigma=1.0);
void reweight_reco(TH1F * reco ,float sigma);


//unfolding tests
void throw_toys(RooUnfoldResponse &response, TH1* truth,TH1* reco,double sigma,int toys,int iterations);
void do_uncertainty(RooUnfoldResponse &response, TH1* truth,TH1* reco,double sigma,int toys,int iterations);
void calculate_coverage(RooUnfoldResponse &response, TH1* truth,TH1* reco,double sigma,int toys,int iterations);

  
void print_bins_nominal(TH1F * hist,const char* dist);
void itoa(float number,char*);
void unfolding_uncertanty(TH1F * unfolded,const char *dist, int iters);

static inline
bool StringInVec(std::string s,std::vector<std::string> &vec){
  return std::find(vec.begin(), vec.end(),s) != vec.end();
}

static inline
bool check_string(std::string substring,std::string full){
  return full.find(substring) != std::string::npos;
}

/******************define binning ******************/

struct distribution_container 
{  
  double* binning;
  int bins;
};

extern std::vector<std::string> scalars;
extern std::vector<std::string> leps;
extern std::vector<std::string> jets;

extern std::map<std::string,distribution_container> distribution_maps;

extern double binning_jet_deta[];extern int jet_deta_bins;
extern double binning_jet_dphi[];extern int jet_dphi_bins;
extern double binning_jet_dr[];extern int jet_dr_bins;
extern double binning_lead_jet_eta[];extern int lead_jet_eta_bins;
extern double binning_lead_jet_phi[];extern int lead_jet_phi_bins;
extern double binning_lead_jet_pt[];extern int jet_pt_bins;


extern double binning_lead_lep_pt[];extern int lep_pt_bins;
extern double binning_lep_deta[];extern int lep_deta_bins;
extern double binning_lep_dphi[];extern int lep_dphi_bins;
extern double binning_lep_dr[];extern int lep_dr_bins;
extern double binning_met[];extern int met_bins;
extern double binning_mjj[];extern int mjj_bins;
extern double binning_mll[];extern int mll_bins;
#endif

