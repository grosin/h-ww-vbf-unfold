#include <algorithm>
#include <vector>
#include "TRandom.h"
#include "TH1F.h"
#include "TH1.h"
#include "RooUnfoldBayes.h"
#include "RooUnfoldBinByBin.h"
#include "RooUnfoldDagostini.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include "TVectorD.h"
#include "RooUnfoldResponse.h"
#include "RooUnfoldSvd.h"
#include "RooUnfoldInvert.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include "unfold.h"
#include "TCut.h"
#include <cstring>
#include "TMath.h"
#include <memory>
#include <fstream>
#include <TTree.h>
#include "TH2F.h"
#include "TAxis.h"
#include <TRandom3.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <TGaxis.h>
#include "tools.h"
#include <tuple>
#include "merge.h"
#include <unordered_map>
#include <TPaveLabel.h>
using namespace TMath;
using namespace std;

std::vector<std::string> scalars ={"Mjj","Mll","MET","Detall","DPhill","DRll","DEtajj","DPhijj","DRjj","DYjj","DYll","higgs_pt","higgs_phi","higgs_eta","lep_pt_0","lep_pt_1","jet_pt_0","jet_pt_1","Ptll","MTll","ptTot","nJets","cosTheta"};
std::vector<std::string> vecs={"lead_lep_pt","sub_lep_pt","lead_jet_pt","sub_jet_pt"};

string to_upper(string st){
  transform(st.begin(), st.end(), st.begin(), ::toupper);
  return st;
}

void fill_map(){}
void itoa(float number, char* str) {
   sprintf(str, "%.1f", number); 
}

RooUnfoldResponse UnfoldDouble(std::string dist1,std::string dist2,std::string merged_file,int iterations){
  fill_map();
  std::cout<<"DEBUG:: in double unfolding dist\n";
  
  TFile* TruthVBFFile = TFile::Open(merged_file.c_str());
  TNtuple *truth_VBF = (TNtuple*)TruthVBFFile->Get("reco_truth_merge");


  const char * truth_distribution1=("t_"+dist1).c_str();
  const char * reco_distribution1=("r_"+dist1).c_str();

  const char * truth_distribution2=("t_"+dist2).c_str();;
  const char * reco_distribution2=("r_"+dist2).c_str();;
  
  TH1F *truth1_test=new TH1F(truth_distribution1,truth_distribution1,distribution_maps[dist1].bins,distribution_maps[dist1].binning);
  TH1F *reco1_test=new TH1F(reco_distribution1,reco_distribution1,distribution_maps[dist1].bins,distribution_maps[dist1].binning);
  std::cout<<"filling response with truth info\n";
  RooUnfoldResponse response=create_response(truth_VBF, truth_distribution1,reco_distribution1,truth1_test,reco1_test);
  return response;
 }
RooUnfoldResponse UnfoldSVD(int regularization,const char * dist, std::string merged_file,bool draw){
  std::cout<<"DEBUG:: in unfolding dist\n";
  
  TFile* TruthVBFFile = TFile::Open(merged_file.c_str());
  TNtuple *truth_VBF = (TNtuple*)TruthVBFFile->Get("reco_truth_merge");

  char t_result[1024];
  t_result[0]='\0';
  strcat(t_result, "t_");
  strcat(t_result,dist);
  const char * truth_distribution=t_result;
  char r_result[1024];
  r_result[0]='\0';
  strcat(r_result, "r_");
  strcat(r_result,dist);
  const char * reco_distribution=r_result;
  
  TH1F *truth_test=new TH1F(truth_distribution,truth_distribution,distribution_maps[dist].bins,distribution_maps[dist].binning);
  TH1F *reco_test=new TH1F(reco_distribution,reco_distribution,distribution_maps[dist].bins,distribution_maps[dist].binning);
  std::cout<<"filling response with truth info\n";
  RooUnfoldResponse response;
  response=create_response(truth_VBF, truth_distribution,reco_distribution,truth_test,reco_test);

  if(regularization ==0){
    auto unfolded=unfold_BinByBin(response,reco_test);
    if (draw){
      print_and_draw((TH1F*)unfolded.Hreco(),truth_test,reco_test,dist);
    }
  }
  else{
    auto unfolded=unfold_svd(response,reco_test,regularization);
    if (draw){
      print_and_draw((TH1F*)unfolded.Hreco(),truth_test,reco_test,dist);

    }
  }

  return response;
}

RooUnfoldResponse UnfoldInPlace(std::string dist,TTree* truth,TTree * reco){

  std::unordered_map<std::string,std::string> truth_names;
  truth_names["Mjj"]="Mjj_truth";
  truth_names["Mll"]="Mll_truth";
  truth_names["DYjj"]="DYjj_truth";
  truth_names["DYll"]="DYll_truth";
  truth_names["pt_H"]="HIGGS_pT";
  truth_names["DPhill"]="DPhill_truth";
  truth_names["lep0_pt"]="lep0_pT";
  truth_names["lep1_pt"]="lep1_pT";
  truth_names["jet0_pt"]="jet0_pT";
  truth_names["jet1_pt"]="jet1_pT";
  truth_names["SignedDPhijj"]="SignedDPhijj_truth";
  truth_names["costhetastar"]="cosThetaStar_truth";
  truth_names["Ptll"]="Ptll_truth";
  truth_names["jet0_eta"]="jet0_y";
  truth_names["jet1_eta"]="jet1_y";
  truth_names["DPhijj"]="SignedDPhijj_truth";
  
  TH1F *truth_test=new TH1F(("t_"+dist).c_str(),dist.c_str(),distribution_maps[dist].bins,distribution_maps[dist].binning);
  TH1F *reco_test=new TH1F(("r_"+dist).c_str(),dist.c_str(),distribution_maps[dist].bins,distribution_maps[dist].binning);
  std::cout<<"filling response matrix\n";
  std::string reco_dist=dist;
  std::string truth_dist;
  if(IsInMap(truth_names,dist)){
    truth_dist=truth_names[dist];
  }
  else{
    truth_dist=dist;
  }
  RooUnfoldResponse response=create_response(truth,reco,truth_dist,reco_dist, truth_test,reco_test);
  delete truth_test;
  delete reco_test;
  return response;
}

RooUnfoldResponse UnfoldDist(int iterations,const char * dist, std::string merged_file,bool draw){
  bool lead=true;
  std::cout<<"DEBUG:: in unfolding dist\n";
  
  TFile* TruthVBFFile = TFile::Open(merged_file.c_str());
  TNtuple *truth_VBF = (TNtuple*)TruthVBFFile->Get("reco_truth_merge");

  char t_result[1024];
  t_result[0]='\0';
  strcat(t_result, "t_");
  strcat(t_result,dist);
  const char * truth_distribution=t_result;
  char r_result[1024];
  r_result[0]='\0';
  strcat(r_result, "r_");
  strcat(r_result,dist);
  const char * reco_distribution=r_result;
  
  long entries=truth_VBF->GetEntries();
  TH1F *truth_test=new TH1F(truth_distribution,truth_distribution,distribution_maps[dist].bins,distribution_maps[dist].binning);
  TH1F *reco_test=new TH1F(reco_distribution,reco_distribution,distribution_maps[dist].bins,distribution_maps[dist].binning);
  std::cout<<"filling response with truth info\n";
  RooUnfoldResponse response;
  response=create_response(truth_VBF, truth_distribution,reco_distribution,truth_test,reco_test);


  if(iterations ==0){
    auto unfolded=unfold_BinByBin(response,reco_test);
    if (draw){
      print_and_draw((TH1F*)unfolded.Hreco(),truth_test,reco_test,dist);
    }
  }
  else{
    auto unfolded=unfold_bayes(response,reco_test,iterations);
    if (draw){
      print_and_draw((TH1F*)unfolded.Hreco(),truth_test,reco_test,dist);

    }
  }
  
  delete truth_VBF;
  TruthVBFFile->Close();
  delete TruthVBFFile;
  return response;
}

TH1F * unfold_data(TH1F * data_hist,RooUnfoldResponse & response, int iters ){
  RooUnfoldBayes unfolded;
  unfolded=unfold_bayes(response,data_hist,iters);
  TH1F * unfolded_hist=(TH1F*)unfolded.Hreco();
  return unfolded_hist;
}
void copy_hist(TH1 * hist1, TH1 *hist2){
  int bins=hist1->GetNbinsX();
  double bin_content;
  for(int i=0; i<=bins;i++){
    bin_content=hist1->GetBinContent(i);
    hist2->SetBinContent(i,bin_content);
    hist2->SetBinError(i,pow(bin_content,0.5));
  }
}
void calculate_coverage(RooUnfoldResponse &response, TH1* truth,TH1* reco,double sigma,int toys,int iterations){
  TH2 * migration=response.Hresponse();
  TH1F * reco_reweighted=(TH1F*)reco->Clone("reco_weighted");
  TH1F* truth_test_weighted=(TH1F*)truth->Clone("truth_test_weighted");
  TH1F *reco_test_weighted=(TH1F*)reco->Clone("reco_test_weighted");
  for(int i =0 ;i <toys;i++){
    copy_hist(reco,reco_test_weighted);
    copy_hist(truth,truth_test_weighted);
    
    reroll_hists(truth_test_weighted,reco_test_weighted,sigma,response);
    for(int i=0;i <100;i++){
      copy_hist(reco_test_weighted,reco_reweighted);
      reweight_hists(reco_reweighted,1);
      if(iterations == 0){
        auto unfolded=unfold_BinByBin(response,reco_reweighted);
        unfolded.SetVerbose(0);
        save_bin_error((TH1F*)unfolded.Hreco(),truth_test_weighted,std::string(truth->GetName())+"_coverage",iterations);
      }else{
        auto unfolded=unfold_bayes(response,reco_reweighted,iterations);
        unfolded.SetVerbose(0);
        save_bin_error((TH1F*)unfolded.Hreco(),truth_test_weighted,std::string(truth->GetName())+"_coverage",iterations);

      }
    }
  }
}

void throw_toys(RooUnfoldResponse &response, TH1* truth,TH1* reco,double sigma,int toys,int iterations){
  TH2 * migration=response.Hresponse();
  TH1F* truth_test_weighted=(TH1F*)truth->Clone("truth_test_weighted");
  TH1F *reco_test_weighted=(TH1F*)reco->Clone("reco_test_weighted");
  for(int i =0 ;i <toys;i++){
    copy_hist(reco,reco_test_weighted);
    copy_hist(truth,truth_test_weighted);
    reroll_hists(truth_test_weighted,reco_test_weighted,sigma,response);
    if(iterations == 0){
      RooUnfoldBinByBin unfolded=unfold_BinByBin(response,reco_test_weighted);
      int chi2=calculate_chi2((TH1F*)unfolded.Hreco(),truth_test_weighted,sigma,std::string(truth->GetName())+"_bias",iterations);
    }else{
      auto unfolded=unfold_bayes(response,reco_test_weighted,iterations);
      int chi2=calculate_chi2((TH1F*)unfolded.Hreco(),truth_test_weighted,sigma,std::string(truth->GetName())+"_bias",iterations);
    }
  }
  delete truth_test_weighted;
  delete reco_test_weighted;
}

void do_uncertainty(RooUnfoldResponse &response, TH1* truth,TH1* reco,double sigma,int toys,int iterations){
  TH1F* truth_test_weighted=(TH1F*)truth->Clone("truth_test_weighted");
  TH1F *reco_test_weighted=(TH1F*)reco->Clone("reco_test_weighted");
  for(int i =0 ;i <toys;i++){
    copy_hist(reco,reco_test_weighted);
    copy_hist(truth,truth_test_weighted);
    reweight_reco(reco_test_weighted,sigma);
    RooUnfoldBayes unfolded=unfold_bayes(response,reco_test_weighted,iterations);
    //save_hist((TH1F*)unfolded.Hreco(),"mjj_histogram.txt",iterations);
    int chi2=calculate_chi2((TH1F*)unfolded.Hreco(),truth_test_weighted,sigma,std::string(truth->GetName())+"_statistical",iterations);
  }
  delete truth_test_weighted;
  delete reco_test_weighted;
}

void reroll_hists(TH1F *truth, TH1F* reco,float sigma, RooUnfoldResponse &response){

  const TMatrixD migration=response.Mresponse();
  TVectorD fakes=response.Vfakes();
  int nbins=truth->GetSize();
  double bin_content=0;
  double bin_error=0;
  double fluctuation=0;
  double reco_bin_content=0;
  TMatrixDSym cov(nbins,nbins);
  //reweight truth
  for(int i=1;i<nbins+1;i++){
    bin_content=truth->GetBinContent(i);
    bin_error=TMath::Power(bin_content,0.5);
    fluctuation=RNG->Gaus(0,sigma*bin_error);
    truth->SetBinContent(i,bin_content+fluctuation);
  }
    //fold into reco
  for(int i=1;i<nbins-1;i++){
    double reco_migration_sum=0;
    for(int j=1;j<nbins-1;j++){
      reco_migration_sum+=truth->GetBinContent(j)*migration[i-1][j-1];
    }
    reco->SetBinContent(i,reco_migration_sum+fakes[i-1]);
  } 

}

void save_hist(TH1F * h, const char * filename,int iteration){
  std::ofstream histFile ;
  histFile.open(filename,ios::app);
  histFile<<iteration<<",";
   for (Int_t i=0;i<h->GetNbinsX();i++){
     histFile<<h->GetBinContent(i)<<",";
  }
   histFile<<"\n";
}
void reweight_hists(TH1F *hist,float sigma){
    int nbins=hist->GetSize();
    double bin_content=0;
    double bin_error=0;
    double fluctuation=0;
    for(int i=0;i<nbins;i++){
      bin_content=hist->GetBinContent(i);
      bin_error=TMath::Power(bin_content,0.5);
      fluctuation=RNG->PoissonD(bin_content);
      hist->SetBinContent(i,fluctuation);
      hist->SetBinError(i,bin_error);
    }
}

void reweight_reco(TH1F * reco ,float sigma){
  int nbins=reco->GetSize();
  double bin_error=0;
  double fluctuation=0;
  double reco_bin_content=0;
  //reweight reco
  for(int i=0;i<nbins;i++){
    reco_bin_content=reco->GetBinContent(i);
    bin_error=TMath::Power(reco_bin_content,0.5);
    fluctuation=RNG->Gaus(0,sigma*bin_error);
    if(reco_bin_content > 0)
      reco->SetBinContent(i,reco_bin_content+fluctuation);
  }  

}

     

RooUnfoldResponse create_response(TTree * truth_tree, TTree * reco_tree, std::string truth_distribution, std::string reco_distribution, TH1 * truth_hist,TH1 * reco_hist){
  std::cout<<truth_tree<<"\n";
  std::cout<<reco_tree<<"\n";
  std::unordered_map<int,int> truth_map=FillRunNumber(truth_tree,"event_number",true);
  std::unordered_map<int,int> reco_map=FillRunNumber(reco_tree,"eventNumber",false);
  float truth_data;
  float detector_data;
  float Mjj_truth=0;
  float DPhill_truth=0;
  float Mjj_reco=0;
  float DPhill_reco=0;
    
  double truth_weight;
  double reco_weight;
  float inSR;
  std::cout<<"truth distribution "<<truth_distribution<<"\n";
  std::cout<<"reco distribution "<<reco_distribution<<"\n";

  truth_tree->SetBranchAddress(truth_distribution.c_str(), &truth_data);
  if(truth_distribution.compare("Mjj_truth")!=0)
    truth_tree->SetBranchAddress("Mjj_truth", &Mjj_truth);
  if(truth_distribution.compare("DPhill_truth")!=0)
    truth_tree->SetBranchAddress("DPhill_truth", &DPhill_truth);

  reco_tree->SetBranchAddress(reco_distribution.c_str(), &detector_data);
  truth_tree->SetBranchAddress("weight", &truth_weight);
  reco_tree->SetBranchAddress("weight",&reco_weight);
  if(reco_distribution.compare("Mjj")!=0)
    reco_tree->SetBranchAddress("Mjj",&Mjj_reco);
  if(reco_distribution.compare("DPhill")!=0)
  reco_tree->SetBranchAddress("DPhill",&DPhill_reco);

  reco_tree->SetBranchAddress("inSR",&inSR);
  std::cout<<"true tree"<<truth_tree->GetEntries()<<"truth map"<<truth_map.size()<<"\n";
  std::cout<<" reco tree"<<reco_tree->GetEntries()<<"reco map"<<reco_map.size()<<"\n";

  RooUnfoldResponse response (reco_hist,truth_hist);
  response.UseOverflow(false);
  float corr=1000;
  bool TruthMatch=false;
  bool RecoMatch = false;
  int missed_events=0;
  int matched_events=0;
  int fake_events=0;
  for (auto & truth_event :truth_map ){
    truth_tree->GetEvent(truth_event.second);

    TruthMatch=true;
    if(Mjj_truth <450000 && truth_distribution.compare("Mjj_truth")!=0) continue;
    if(DPhill_truth>1.4 &&  truth_distribution.compare("DPhill")!=0) continue;
    if(truth_data<450000 && truth_distribution.compare("Mjj")==0) continue;
    if(truth_data >1.4 && truth_distribution.compare("DPhill")==0) continue;
    int eventNumber=truth_event.first;
    RecoMatch=IsInMap(reco_map,eventNumber);
    if(RecoMatch){
      reco_tree->GetEvent(reco_map[truth_event.first]);
      if((int)inSR!=1) continue;
      if(Mjj_reco <450000 && reco_distribution.compare("Mjj")!=0) continue;
      if(DPhill_reco>1.4 &&  reco_distribution.compare("DPhill")!=0) continue;
      if(detector_data<450000 && reco_distribution.compare("Mjj")==0) continue;
      if(detector_data >1.4 && reco_distribution.compare("DPhill")==0) continue;
      response.Fill(detector_data,truth_data,reco_weight);
      matched_events++;
    }
    else{
      response.Miss(truth_data,truth_weight);
      missed_events++;
    }
  }
  for (auto & reco_event :reco_map ){
    RecoMatch=true;
    reco_tree->GetEvent(reco_event.second);
    if((int)inSR!=1) continue;
    if(Mjj_reco <450000 && reco_distribution.compare("Mjj")!=0) continue;
    if(DPhill_reco>1.4 &&  reco_distribution.compare("DPhill")!=0) continue;
    if(detector_data<450000 && reco_distribution.compare("Mjj")==0) continue;
    if(detector_data >1.4 && reco_distribution.compare("DPhill")==0) continue;
    int eventNumber=reco_event.first;
    TruthMatch=IsInMap(truth_map,eventNumber);
    if(!TruthMatch){
      response.Fake(detector_data,reco_weight);
      fake_events++;
    }
  }
  truth_map.clear();
  reco_map.clear();
  truth_tree->ResetBranchAddresses();
  reco_tree->ResetBranchAddresses();
  return response;
}

RooUnfoldResponse create_response(TTree *data_tree, const char * truth_distribution,const char * detector_distribution,TH1F * truth_hist,TH1F * reco_hist){
  std::cout<<"creatning response matrix\n";
  std::cout<<detector_distribution<<"\n";
  std::cout<<truth_distribution<<"\n";

  double truth_data;
  double detector_data;

  double r_weight;
  double t_weight;

  int RecoMatch;
  int TruthMatch;


  data_tree->SetBranchAddress(truth_distribution, &truth_data);
  data_tree->SetBranchAddress(detector_distribution, &detector_data);
  data_tree->SetBranchAddress("t_weight", &t_weight);
  data_tree->SetBranchAddress("r_weight",&r_weight);
  data_tree->SetBranchAddress("recoMatch",&RecoMatch);
  data_tree->SetBranchAddress("truthMatch",&TruthMatch);
  
  Int_t truth_nevents = data_tree->GetEntries();
  
 
  bool truth_cut;
  bool reco_cut;
  RooUnfoldResponse response (reco_hist,truth_hist);
  response.UseOverflow(false);
  float corr=1000;
  
  for (Int_t i=0;i<truth_nevents;i++){
    data_tree->GetEvent(i);
    if(TruthMatch && RecoMatch){
      response.Fill(detector_data,truth_data,r_weight);
      truth_hist->Fill(truth_data,r_weight);
      reco_hist->Fill(detector_data,r_weight);
    }
    else if(TruthMatch){
      response.Miss(truth_data,t_weight);
      truth_hist->Fill(truth_data,t_weight);
    }
    else if(RecoMatch){
      response.Fake(detector_data,r_weight);
      reco_hist->Fill(detector_data,r_weight);
    }
  }
 
  return response;
}

void fill_background(RooUnfoldResponse & response,TTree * background,std::string feature){
  double weight;
  float data;
  background->SetBranchAddress("weight",&weight);
  background->SetBranchAddress(feature.c_str(),&data);
  Int_t nevents = background->GetEntries();
  for(int i=0;i<nevents;i++){
    background->GetEvent(i);
    response.Fake(data,weight);
  }
  background->ResetBranchAddresses();


}
TH1F* fill_hist(TTree *data_tree,std::string dist,std::string name,bool bdt_weight){
  
  double weight;
  float data;
  float bdt_vbf;
  float inSR;

  data_tree->SetBranchAddress(dist.c_str(), &data);
  data_tree->SetBranchAddress("weight",&weight);
  data_tree->SetBranchAddress("inSR",&inSR);

  if(bdt_weight)
    data_tree->SetBranchAddress("bdt_vbf",&bdt_vbf);
  Int_t truth_nevents = data_tree->GetEntries();
  TH1F* hist=new TH1F((dist+name).c_str(),(dist+" "+name).c_str(),distribution_maps[dist].bins,distribution_maps[dist].binning);
  
  for (Int_t i=0;i<truth_nevents;i++){
    data_tree->GetEvent(i);
    if((int)inSR!=1) continue;
    if(bdt_weight){
      bdt_vbf=(bdt_vbf+1) / 2;
      hist->Fill(data,weight*bdt_vbf);
    }
    else
      hist->Fill(data,weight);
  }
 
  return hist;
}
RooUnfoldBayes unfold_bayes(RooUnfoldResponse &response, TH1* reco,int iterations){
  RooUnfoldBayes unfold (&response, reco, iterations);
  return unfold;
}

RooUnfoldBinByBin unfold_BinByBin(RooUnfoldResponse &response, TH1* reco){
  RooUnfoldBinByBin unfold (&response, reco);
  return unfold;
}

RooUnfoldSvd unfold_svd(RooUnfoldResponse &response, TH1* data,int normalization){
  RooUnfoldSvd unfold (&response, data, normalization);
  return unfold;
}

RooUnfoldInvert unfold_invert(RooUnfoldResponse &response, TH1* data){
  RooUnfoldInvert unfold (&response, data,"","");
  return unfold;
}

void print_and_draw(TH1F* hReco, TH1F * Test_Truth_Hist,TH1F* Reco_Test,const char * title ){
  TCanvas *c1 = new TCanvas("c1", "c1",1000,1000);
  TPad *pad1 = new TPad("pad1", "pad1",0.,.3,1.,1);
  std::map<std::string,std::string> unfolding_features={
                                                        {"higgs_pt","Higgs Pt"},
                                                        { "MTll","MT^{ll}"},
                                                        {"ptTot","Total Pt"},
                                                        {"Ptll","Ptll"},
                                                        {"lep_pt_0","Leading Lep Pt"},
                                                        {"lep_pt_1","Subleading Lep Pt"},
                                                        {"DYll","DYll"},
                                                        {"DPhill","DPhill"},
                                                        {"cosTheta","Cos(#theta*)"},
                                                        {"Mjj","Mjj"},
                                                        {"jet_pt_0","Leading Jet Pt"},
                                                        {"jet_pt_1","Subleading Jet Pt"},
                                                        {"DYjj","DYjj"},
                                                        {"Mll","Mll"}};
  Test_Truth_Hist->SetTitle(unfolding_features[std::string(title)].c_str());
  pad1->SetTitle(unfolding_features[std::string(title)].c_str());
  pad1->SetBottomMargin(0);
  pad1->Draw();
  pad1->cd();
  hReco->SetMarkerColor(kBlack);
  hReco->SetMarkerStyle(kFullCircle);
  hReco->SetMarkerSize(0.8);
  Reco_Test->SetLineColor(kGreen);
  Test_Truth_Hist->SetTitle(unfolding_features[std::string(title)].c_str());
  Test_Truth_Hist->SetStats(0);
  auto legend = new TLegend(0.8,0.8,0.98,0.9);
  legend->AddEntry(Test_Truth_Hist,"Truth  distribution","f");
  legend->AddEntry(hReco,"Unfolded distribution","lep");
  legend->AddEntry(Reco_Test,"Reco distribution","f");
  Test_Truth_Hist->SetMinimum(0);
  Test_Truth_Hist->Draw();
  hReco->Draw("SAME");
  Reco_Test->Draw("SAME");
  TPaveLabel *plot_title = new TPaveLabel(0.4,0.90,0.68,0.95,unfolding_features[std::string(title)].c_str(),"brNDC");
  plot_title->Draw();
  std::cout<<"printing title\n";
  legend->Draw();
  Test_Truth_Hist->GetYaxis()->SetTitle("Expected Run 2 Events");
  //TGaxis *axis = new TGaxis( -5, 20, -5, 220, 20,220,510,"");
  //axis->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  //axis->SetLabelSize(15);
  //axis->Draw();
  c1->cd();
  TPad *pad2 = new TPad("pad2", "pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.2);
  pad2->SetGridx(); // vertical grid
  pad2->Draw();
  pad2->cd();

  TH1F *h3 = (TH1F*)Test_Truth_Hist->Clone("h3");
  h3->Divide(hReco);
  h3->Draw();

  Test_Truth_Hist->GetYaxis()->SetTitleSize(20);
  Test_Truth_Hist->GetYaxis()->SetTitleFont(43);
  Test_Truth_Hist->GetYaxis()->SetTitleOffset(1.55);
  h3->SetMaximum(1.5);
  h3->SetMinimum(0.5);
  h3->GetYaxis()->SetTitle("ratio unfold/truth");
  h3->SetStats(0);
  h3->GetYaxis()->SetNdivisions(505);
  h3->GetYaxis()->SetTitleSize(20);
  h3->GetYaxis()->SetTitleFont(43);
  h3->GetYaxis()->SetTitleOffset(1.55);
  h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetYaxis()->SetLabelSize(15);

   // X axis ratio plot settings
  h3->GetXaxis()->SetTitleSize(20);
  h3->GetXaxis()->SetTitleFont(43);
  h3->GetXaxis()->SetTitleOffset(4.);
  h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetXaxis()->SetLabelSize(15)  ;
  c1->Print((std::string(title)+"_validation.pdf").c_str());
  delete c1;

  
}


void print_matrix(const TMatrixD &response,const char * distribution){
  std::ofstream matrix_file ;
  matrix_file.open("unfolding_matrix.txt",ios::app);
  matrix_file<<"variable:"<<distribution<<"\n";
  
  int nrows=response.GetNrows();
  int ncols=response.GetNcols();
  matrix_file<<"size:"<<nrows<<","<<ncols<<"\n";

  for(int i=0;i<nrows;i++){
    for(int j=0;j<ncols;j++){
      std::cout<<response[i][j]<<" ";
      matrix_file<<response[i][j]<<" ";
    }
    std::cout<<"\n";
    matrix_file<<"\n";
  }
  matrix_file.close();
}
void print_matrix(TH2 *response,const char * distribution){
  std::ofstream matrix_file ;
  matrix_file.open("response_matrix.txt",ios::app);
  matrix_file<<"variable:"<<distribution<<"\n";
  
  int nrows=response->GetNbinsX();
  int ncols=response->GetNbinsY();
  matrix_file<<"size:"<<nrows<<","<<ncols<<"\n";

  for(int i=0;i<=nrows;i++){
    for(int j=0;j<=ncols;j++){
      std::cout<<response->GetBinContent(i,j)<<" ";
      matrix_file<<response->GetBinContent(i,j)<<" ";
    }
    std::cout<<"\n";
    matrix_file<<"\n";
  }
  matrix_file.close();
}


//get minimum and maximum value of a variable
std::vector<double> GetMinMax(TTree * VBF,const char * distribution){
  gROOT->SetBatch(true);
  long nevent = VBF->GetEntries();
  VBF->SetEstimate(nevent);
  VBF->Draw(distribution);//,distribution_cut);
  double * tree_data=VBF->GetV1();
  std::cout<<"calculating MinMax: "<<nevent<<"\n";
  double* data_min_bin=std::min_element(tree_data, tree_data+nevent);
  double* data_max_bin=std::max_element(tree_data,tree_data+nevent);
  gROOT->SetBatch(false);
  std::vector<double> MinMax(2);
  MinMax[0]=*data_min_bin;
  MinMax[1]=*data_max_bin;
  VBF->ResetBranchAddresses();
  return MinMax; 
}



double calculate_residuals(TH1F * unfolded, TH1F *truth){
  double residual=0;
  for (Int_t i=0;i<unfolded->GetNbinsX();i++){
    residual+=Abs(unfolded->GetBinContent(i)-truth->GetBinContent(i));
  }
  return residual;
}
               
double calculate_chi2(TH1F * unfolded, TH1F *truth, float sigma, std::string dist,int iters){
  double chi2=0;
  std::ofstream chiFile ;
  char sigma_buffer[10];
  itoa(sigma,sigma_buffer);
  std::string filename=dist+sigma_buffer+".txt";
  
  chiFile.open(filename.c_str(),ios::app);
  chiFile << dist<<",";
  chiFile << iters;
  chiFile<<",";
  double bin_residue(0);
  for (Int_t i=0;i<=unfolded->GetNbinsX();i++){
    bin_residue=unfolded->GetBinContent(i)-truth->GetBinContent(i);
    chiFile<<bin_residue;
    chiFile<<",";
    chiFile<<truth->GetBinContent(i);
    chiFile<<",";
    if(unfolded->GetBinError(i)>0)
      chi2+=Power(unfolded->GetBinContent(i)-truth->GetBinContent(i),2)/Power(unfolded->GetBinError(i),2);
  }
  chiFile<<chi2;
  
  chiFile<<"\n";
  chiFile.close();
  return chi2;
}
void unfolding_uncertanty(TH1F * unfolded,const char *dist, int iters){
  std::ofstream chiFile ;
  chiFile.open("unfolding_uncertinty.txt",ios::app);
  chiFile<<dist<<","<<iters<<",";

  for (Int_t i=0;i<unfolded->GetNbinsX();i++){
    chiFile<<unfolded->GetBinError(i)<<",";
  }
  chiFile<<"\n";
  chiFile.close();
}
void print_bins_nominal(TH1F * hist,const char* dist){
  
  std::ofstream chiFile ;
  chiFile.open("binning_nominal.txt",ios::app);
  chiFile<<(std::string(dist)+"_binning").c_str()<<":";
  for (Int_t i=0;i<hist->GetNbinsX();i++){
    chiFile<<hist->GetBinContent(i)<<",";
  }
  chiFile<<hist->GetBinContent(hist->GetNbinsX());

  chiFile<<"\n";
  chiFile.close();
}
bool is_file_empty (const std::string& name) {
    ifstream f(name.c_str());
    return !f.good();
}
void save_bin_error(TH1F* unfolded,TH1F* truth,std::string name,int iteration){
  std::ofstream chiFile ;
  std::string filename=name+".txt";
  bool file_is_empty=is_file_empty(filename);
  chiFile.open(filename.c_str(),ios::app);
  double bin_residue(0);
  if(file_is_empty){
    chiFile<<"Name,"<<"Iteration,";
    for (Int_t i=0;i<unfolded->GetNbinsX();i++){
       chiFile<<std::string("BinBias")+std::to_string(i)+",";
       chiFile<<std::string("UnfoldingError")+std::to_string(i)+",";
    }
    chiFile<<std::string("BinBias")+std::to_string(unfolded->GetNbinsX())+",";
    chiFile<<std::string("UnfoldingError")+std::to_string(unfolded->GetNbinsX());
    chiFile<<"\n";

  }
  chiFile<<name<<","<<std::to_string(iteration)<<",";
  for (Int_t i=0;i<unfolded->GetNbinsX();i++){
    bin_residue=unfolded->GetBinContent(i)-truth->GetBinContent(i);
    chiFile<<bin_residue;
    chiFile<<",";
    chiFile<<unfolded->GetBinError(i);
    chiFile<<",";
  }
  bin_residue=unfolded->GetBinContent(unfolded->GetNbinsX())-truth->GetBinContent(unfolded->GetNbinsX());
  chiFile<<bin_residue;
  chiFile<<",";
  chiFile<<unfolded->GetBinError(unfolded->GetNbinsX());
  chiFile<<"\n";
  chiFile.close();

}


