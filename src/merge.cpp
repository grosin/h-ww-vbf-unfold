#include <algorithm>
#include <vector>
#include "TRandom.h"
#include "TH1F.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TGraph.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include <cstring>
#include <TMath.h>
#include <memory>
#include <fstream>
#include <TTree.h>
#include "TH2F.h"
#include <TRandom.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unordered_map>
#include <iostream>
#include <TPad.h>
#include "merge.h"
#include "tools.h"
#include "fakes_cutflow.h"
using namespace TMath;


std::unordered_map<int,int> FillRunNumberWithCuts(TTree * Tree,const char * event_name, std::vector<cut> & cutflow){
  Tree->ResetBranchAddresses();
  std::unordered_map<int,int> EventToRunNumber;
  int runNum;
  std::cout<<"setting address\n";
  Tree->SetBranchAddress(event_name,&runNum);
  std::cout<<"getting n entries\n";
  int nEvents=Tree->GetEntries();
  std::cout<<"looping over truth event numbers\n";
  
  vector<double*> cut_memory(cutflow.size());
  vector<double> cut_variable(cutflow.size());
  for(int i=0;i<cutflow.size();i++){
    std::cout<<"setting cut variable to "<<cutflow[i].variable.c_str()<<"\n";
    cut_memory[i]=&cut_variable[i];
    Tree->SetBranchAddress(cutflow[i].variable.c_str(),&cut_variable[i]);
  }

  bool cut;
  int duplicate=0;
  for(int i=0; i < nEvents ;i++){
    Tree->GetEvent(i);
    for(int i=0;i<cutflow.size();i++){
      cut=check_cut( cutflow[i], cut_memory[i]);
      if(cut) break;
    }
    if(cut) continue;
    if(IsInMap(EventToRunNumber,(int)runNum)){
      duplicate++;
    }
    EventToRunNumber[(int)runNum]=i;
  }
  Tree->ResetBranchAddresses();
  std::cout<<"returning dictionary\n";
  std::cout<<"total duplicates "<<duplicate<<"\n";
  return EventToRunNumber;
}

std::unordered_map<int,int> FillRunNumber(TTree * recoTree,const char * event_name,bool truth){
  std::cout<<recoTree<<"\n";
  recoTree->ResetBranchAddresses();
  std::unordered_map<int,int> EventToRunNumber;
  int duplicates=0;
  if(truth){
    ULong64_t runNum;
    std::cout<<"setting address\n";
    recoTree->SetBranchAddress(event_name,&runNum);
    std::cout<<"getting n entries\n";
      int nEvents=recoTree->GetEntries();
      //EventToRunNumber.reserve(nEvents);
      std::cout<<"looping over truth event numbers\n";
      
      for(int i=0; i < nEvents ;i++){
        recoTree->GetEvent(i);
        if(IsInMap(EventToRunNumber,(int)runNum)){
          duplicates++;
        }
        EventToRunNumber[(int)runNum]=i;
      }
      recoTree->ResetBranchAddresses();
      std::cout<<"total duplicates "<<duplicates<<"\n";

      return EventToRunNumber;
  }
  else{
    int runNum;
    std::cout<<"setting address\n";
    recoTree->SetBranchAddress(event_name,&runNum);
    std::cout<<"getting n entries\n";
      int nEvents=recoTree->GetEntries();
      //EventToRunNumber.reserve(nEvents);
      std::cout<<"looping over reco event numbers\n";

      for(int i=0; i < nEvents ;i++){
        recoTree->GetEvent(i);
        if(IsInMap(EventToRunNumber,(int)runNum)){
          duplicates++;
        }
        EventToRunNumber[(int)runNum]=i;
      }
      recoTree->ResetBranchAddresses();
      std::cout<<"total duplicates "<<duplicates<<"\n";
        
      return EventToRunNumber;
  }

}

bool IsInMap(std::unordered_map<int,int> &mymap, int input){
  return !(mymap.find (input) == mymap.end());
}

bool IsInMap(std::unordered_map<std::string,std::string> &mymap, std::string input){
  return !(mymap.find (input) == mymap.end());
}

TTree * merge_trees(TTree *truthTree, TTree *recoTree){
  auto EventMap=FillRunNumber(recoTree,"eventNumber");
  TTree * merged=new TTree("reco_truth_merge","reco truth merge");

  int eventNumber;
  //define reco variables in our merged tree
  std::cout<<"defining merged reco variables\n";

  double m_r_weight;
  int recoMatch;
  int truthMatch;

  double m_r_MT;
  double m_r_MET;
  
  double m_r_Mll;
  double m_r_DEtall;
  double m_r_DRll;
  
  double m_r_Ptjj;
  double m_r_DPhijj;
  double m_r_DEtajj;
  double m_r_DYjj;
  double m_r_DRjj;
  double m_r_OLV;
  double m_r_h_pt;
  double m_r_h_phi;
  double m_r_h_eta;
  

  double m_r_lep_eta_0;
  double m_r_lep_eta_1;
  double m_r_lep_phi_0;
  double m_r_lep_phi_1;
  double m_r_lep_y_0;
  double m_r_lep_y_1;
  double m_r_lep_m_0;
  double m_r_lep_m_1;
  double m_r_lep_e_0;
  double m_r_lep_e_1;


  double m_r_jet_eta_0;
  double m_r_jet_eta_1;
  double m_r_jet_phi_0;
  double m_r_jet_phi_1;
  double m_r_jet_y_0;
  double m_r_jet_y_1;
  double m_r_jet_m_0;
  double m_r_jet_m_1;
  double m_r_jet_e_0;
  double m_r_jet_e_1;

  float m_r_inSR;

  //unfolding observables
  double m_r_MTll;
  double m_r_ptTot;
  double m_r_cosTheta;
  int m_r_nJets;
  double m_r_jet_pt_0;
  double m_r_jet_pt_1;
  double m_r_Ptll;
  double m_r_DPhill;
  double m_r_DYll;
  double m_r_Mjj;
  double m_r_lep_pt_0;
  double m_r_lep_pt_1;
  
  //set merge tree
  std::cout<<"setting merged reco variables\n";

  merged->Branch("eventNumber",&eventNumber,"eventNumber/I");
  merged->Branch("r_weight",&m_r_weight,"r_weight/D");
  merged->Branch("recoMatch",&recoMatch,"r_recoMatch/I");
  merged->Branch("truthMatch",&truthMatch,"r_truthMatch/I");
  merged->Branch("r_OLV",&m_r_OLV,"r_OLV/D");

  merged->Branch("r_MT",&m_r_MT,"r_MT/D");
  merged->Branch("r_MET",&m_r_MET,"r_MET/D");
  
  merged->Branch("r_Mll",&m_r_Mll,"r_Mll/D");
  merged->Branch("r_Ptll",&m_r_Ptll,"r_Ptll/D");
  merged->Branch("r_DPhill",&m_r_DPhill,"r_DPhill/D");
  merged->Branch("r_DEtall",&m_r_DEtall,"r_DEtall/D");
  merged->Branch("r_DYll",&m_r_DYll,"r_DYll/D");
  merged->Branch("r_DRll",&m_r_DRll,"r_DRll/D");
  
  merged->Branch("r_Mjj",&m_r_Mjj,"r_Mjj/D");
  merged->Branch("r_Ptjj",&m_r_Ptjj,"r_Ptjj/D");
  merged->Branch("r_DPhijj",&m_r_DPhijj,"r_DPhijj/D");
  merged->Branch("r_DEtajj",&m_r_DEtajj,"r_DEtajj/D");
  merged->Branch("r_DYjj",&m_r_DYjj,"r_DYjj/D");
  merged->Branch("r_DRjj",&m_r_DRjj,"r_DRjj/D");
  merged->Branch("r_nJets",&m_r_nJets,"r_nJets/I");
  
  merged->Branch("r_lep_pt_0",&m_r_lep_pt_0,"r_lep_pt_0/D");
  merged->Branch("r_lep_pt_1",&m_r_lep_pt_1,"r_lep_pt_1/D");
  merged->Branch("r_lep_eta_0",&m_r_lep_eta_0,"r_lep_eta_0/D");
  merged->Branch("r_lep_eta_1",&m_r_lep_eta_1,"r_lep_eta_1/D");
  merged->Branch("r_lep_phi_0",&m_r_lep_phi_0,"r_lep_phi_0/D");
  merged->Branch("r_lep_phi_1",&m_r_lep_phi_1,"r_lep_phi_1/D");
  merged->Branch("r_lep_y_0",&m_r_lep_y_0,"r_lep_y_0/D");
  merged->Branch("r_lep_y_1",&m_r_lep_y_1,"r_lep_y_1/D");
  merged->Branch("r_lep_m_0",&m_r_lep_m_0,"r_lep_m_0/D");
  merged->Branch("r_lep_m_1",&m_r_lep_m_1,"r_lep_m_1/D");
  merged->Branch("r_lep_e_0",&m_r_lep_e_0,"r_lep_e_0/D");
  merged->Branch("r_lep_e_1",&m_r_lep_e_1,"r_lep_e_1/D");

  merged->Branch("r_jet_pt_0",&m_r_jet_pt_0,"r_jet_pt_0/D");
  merged->Branch("r_jet_pt_1",&m_r_jet_pt_1,"r_jet_pt_1/D");
  merged->Branch("r_jet_eta_0",&m_r_jet_eta_0,"r_jet_eta_0/D");
  merged->Branch("r_jet_eta_1",&m_r_jet_eta_1,"r_jet_eta_1/D");
  merged->Branch("r_jet_phi_0",&m_r_jet_phi_0,"r_jet_phi_0/D");
  merged->Branch("r_jet_phi_1",&m_r_jet_phi_1,"r_jet_phi_1/D");
  merged->Branch("r_jet_y_0",&m_r_jet_y_0,"r_jet_y_0/D");
  merged->Branch("r_jet_y_1",&m_r_jet_y_1,"r_jet_y_1/D");
  merged->Branch("r_jet_m_0",&m_r_jet_m_0,"r_jet_m_0/D");
  merged->Branch("r_jet_m_1",&m_r_jet_m_1,"r_jet_m_1/D");
  merged->Branch("r_jet_e_0",&m_r_jet_e_0,"r_jet_e_0/D");
  merged->Branch("r_jet_e_1",&m_r_jet_e_1,"r_jet_e_1/D");
  //merged->Branch("r_inSR",&m_r_inSR,"r_inSR/F");
  merged->Branch("r_higgs_pt",&m_r_h_pt,"r_higgs_pt/D");
  merged->Branch("r_higgs_phi",&m_r_h_phi,"r_higgs_phi/D");
  merged->Branch("r_higgs_eta",&m_r_h_eta,"r_higgs_eta/D");

  merged->Branch("r_MTll",&m_r_MTll,"r_MTll/D");
  merged->Branch("r_ptTot",&m_r_ptTot,"r_ptTot/D");
  merged->Branch("r_cosTheta",&m_r_cosTheta,"r_cosTheta/D");
  
  double r_DRl0j0;
  double r_DRl1j1;
  double r_DRl0j1;
  double r_DRl1j0;

  double t_DRl0j0;
  double t_DRl1j1;
  double t_DRl0j1;
  double t_DRl1j0;
    
  merged->Branch("t_DRl0j0",&t_DRl0j0,"t_DRl0j0/D");
  merged->Branch("t_DRl1j0",&t_DRl1j0,"t_DRl1j0/D");
  merged->Branch("t_DRl0j1",&t_DRl0j1,"t_DRl0j1/D");
  merged->Branch("t_DRl1j1",&t_DRl1j1,"t_DRl1j1/D");
  merged->Branch("r_DRl0j0",&r_DRl0j0,"r_DRl0j0/D");
  merged->Branch("r_DRl1j0",&r_DRl1j0,"r_DRl1j0/D");
  merged->Branch("r_DRl0j1",&r_DRl0j1,"r_DRl0j1/D");
  merged->Branch("r_DRl1j1",&r_DRl1j1,"r_DRl1j1/D");

  std::cout<<"defining merged truth variables\n";

  double m_t_weight;


  double m_t_MT;
  double m_t_MET;
  
  double m_t_Mll;
  double m_t_Ptll;
  double m_t_DPhill;
  double m_t_DEtall;
  double m_t_DYll;
  double m_t_DRll;
  
  double m_t_Mjj;
  double m_t_Ptjj;
  double m_t_DPhijj;
  double m_t_DEtajj;
  double m_t_DYjj;
  double m_t_DRjj;
  int m_t_nJets;

  double m_t_h_pt;
  double m_t_h_phi;
  double m_t_h_eta;
  
  double m_t_lep_pt_0;
  double m_t_lep_pt_1;
  double m_t_lep_eta_0;
  double m_t_lep_eta_1;
  double m_t_lep_phi_0;
  double m_t_lep_phi_1;
  double m_t_lep_y_0;
  double m_t_lep_y_1;
  double m_t_lep_m_0;
  double m_t_lep_m_1;
  double m_t_lep_e_0;
  double m_t_lep_e_1;

  double m_t_jet_pt_0;
  double m_t_jet_pt_1;
  double m_t_jet_eta_0;
  double m_t_jet_eta_1;
  double m_t_jet_phi_0;
  double m_t_jet_phi_1;
  double m_t_jet_y_0;
  double m_t_jet_y_1;
  double m_t_jet_m_0;
  double m_t_jet_m_1;
  double m_t_jet_e_0;
  double m_t_jet_e_1;

  double m_t_MTll;
  double m_t_ptTot;
  double m_t_cosTheta;
  //set merge tree
  std::cout<<"setting merged truth variables\n";

  merged->Branch("t_weight",&m_t_weight,"t_weight/D");

  merged->Branch("t_MT",&m_t_MT,"t_MT/D");
  merged->Branch("t_MET",&m_t_MET,"t_MET/D");
  
  merged->Branch("t_Mll",&m_t_Mll,"t_Mll/D");
  merged->Branch("t_Ptll",&m_t_Ptll,"t_Ptll/D");
  merged->Branch("t_DPhill",&m_t_DPhill,"t_DPhill/D");
  merged->Branch("t_DEtall",&m_t_DEtall,"t_DEtall/D");
  merged->Branch("t_DYll",&m_t_DYll,"t_DYll/D");
  merged->Branch("t_DRll",&m_t_DRll,"t_DRll/D");
  
  merged->Branch("t_Mjj",&m_t_Mjj,"t_Mjj/D");
  merged->Branch("t_Ptjj",&m_t_Ptjj,"t_Ptjj/D");
  merged->Branch("t_DPhijj",&m_t_DPhijj,"t_DPhijj/D");
  merged->Branch("t_DEtajj",&m_t_DEtajj,"t_DEtajj/D");
  merged->Branch("t_DYjj",&m_t_DYjj,"t_DYjj/D");
  merged->Branch("t_DRjj",&m_t_DRjj,"t_DRjj/D");
  merged->Branch("t_nJets",&m_t_nJets,"t_nJets/I");
  
  merged->Branch("t_lep_pt_0",&m_t_lep_pt_0,"t_lep_pt_0/D");
  merged->Branch("t_lep_pt_1",&m_t_lep_pt_1,"t_lep_pt_1/D");
  merged->Branch("t_lep_eta_0",&m_t_lep_eta_0,"t_lep_eta_0/D");
  merged->Branch("t_lep_eta_1",&m_t_lep_eta_1,"t_lep_eta_1/D");
  merged->Branch("t_lep_phi_0",&m_t_lep_phi_0,"t_lep_phi_0/D");
  merged->Branch("t_lep_phi_1",&m_t_lep_phi_1,"t_lep_phi_1/D");
  merged->Branch("t_lep_y_0",&m_t_lep_y_0,"t_lep_y_0/D");
  merged->Branch("t_lep_y_1",&m_t_lep_y_1,"t_lep_y_1/D");
  merged->Branch("t_lep_m_0",&m_t_lep_m_0,"t_lep_m_0/D");
  merged->Branch("t_lep_m_1",&m_t_lep_m_1,"t_lep_m_1/D");
  merged->Branch("t_lep_e_0",&m_t_lep_e_0,"t_lep_e_0/D");
  merged->Branch("t_lep_e_1",&m_t_lep_e_1,"t_lep_e_1/D");
  merged->Branch("t_jet_pt_0",&m_t_jet_pt_0,"t_jet_pt_0/D");
  merged->Branch("t_jet_pt_1",&m_t_jet_pt_1,"t_jet_pt_1/D");
  merged->Branch("t_jet_eta_0",&m_t_jet_eta_0,"t_jet_eta_0/D");
  merged->Branch("t_jet_eta_1",&m_t_jet_eta_1,"t_jet_eta_1/D");
  merged->Branch("t_jet_phi_0",&m_t_jet_phi_0,"t_jet_phi_0/D");
  merged->Branch("t_jet_phi_1",&m_t_jet_phi_1,"t_jet_phi_1/D");
  merged->Branch("t_jet_y_0",&m_t_jet_y_0,"t_jet_y_0/D");
  merged->Branch("t_jet_y_1",&m_t_jet_y_1,"t_jet_y_1/D");
  merged->Branch("t_jet_m_0",&m_t_jet_m_0,"t_jet_m_0/D");
  merged->Branch("t_jet_m_1",&m_t_jet_m_1,"t_jet_m_1/D");
  merged->Branch("t_jet_e_0",&m_t_jet_e_0,"t_jet_e_0/D");
  merged->Branch("t_jet_e_1",&m_t_jet_e_1,"t_jet_e_1/D");
  merged->Branch("t_higgs_pt",&m_t_h_pt,"t_higgs_pt/D");
  merged->Branch("t_higgs_phi",&m_t_h_phi,"t_higgs_phi/D");
  merged->Branch("t_higgs_eta",&m_t_h_eta,"t_higgs_eta/D");

  merged->Branch("t_MTll",&m_t_MTll,"t_MTll/D");
  merged->Branch("t_ptTot",&m_t_ptTot,"t_ptTot/D");
  merged->Branch("t_cosTheta",&m_t_cosTheta,"t_cosTheta/D");

  //define reco tree variables
  std::cout<<"defining reco variables\n";

  int r_eventNumber;
  double r_weight;


  double r_MT;
  double r_MET;
  
  double r_Mll;
  double r_Ptll;
  double r_DPhill;
  double r_DEtall;
  double r_DYll;
  double r_DRll;
  
  double r_Mjj;
  double r_Ptjj;
  double r_DPhijj;
  double r_DEtajj;
  double r_DYjj;
  double r_DRjj;
  int r_nJets;
  
  double r_lep0_pt;
  double r_lep1_pt;
  double r_lep0_eta;
  double r_lep1_eta;
  double r_lep0_phi;
  double r_lep1_phi;
  double r_lep0_y;
  double r_lep1_y;
  double r_lep0_m;
  double r_lep1_m;
  double r_lep0_e;
  double r_lep1_e;

  double r_jet0_pt;
  double r_jet1_pt;
  double r_jet0_eta;
  double r_jet1_eta;
  double r_jet0_phi;
  double r_jet1_phi;
  double r_jet0_y;
  double r_jet1_y;
  double r_jet0_m;
  double r_jet1_m;
  double r_jet0_e;
  double r_jet1_e;
  double r_OLV;
  double r_h_pt;
  double r_h_phi;
  double r_h_eta;

  double r_MTll;
  double r_ptTot;
  double r_cosTheta;
  std::cout<<"setting reco variables\n";

  recoTree->SetBranchAddress("eventNumber",&r_eventNumber);
  recoTree->SetBranchAddress("weight",&r_weight);
  recoTree->SetBranchAddress("OLV",&r_OLV);

  recoTree->SetBranchAddress("MT",&r_MT);
  recoTree->SetBranchAddress("MET",&r_MET);
  
  recoTree->SetBranchAddress("Mll",&r_Mll);
  recoTree->SetBranchAddress("DPhill",&r_DPhill);
  //recoTree->SetBranchAddress("DEtall",&r_DEtall);

  recoTree->SetBranchAddress("DYll",&r_DYll);
  
  recoTree->SetBranchAddress("Mjj",&r_Mjj);
  recoTree->SetBranchAddress("jets",&r_nJets);
  recoTree->SetBranchAddress("lep0_pt",&r_lep0_pt);
  recoTree->SetBranchAddress("lep1_pt",&r_lep1_pt);
  recoTree->SetBranchAddress("lep0_eta",&r_lep0_eta);
  recoTree->SetBranchAddress("lep1_eta",&r_lep1_eta);
  recoTree->SetBranchAddress("lep0_phi",&r_lep0_phi);
  recoTree->SetBranchAddress("lep1_phi",&r_lep1_phi);
  recoTree->SetBranchAddress("lep0_E",&r_lep0_e);
  recoTree->SetBranchAddress("lep1_E",&r_lep1_e);

  recoTree->SetBranchAddress("jet0_pt",&r_jet0_pt);
  recoTree->SetBranchAddress("jet1_pt",&r_jet1_pt);
  recoTree->SetBranchAddress("jet0_eta",&r_jet0_eta);
  recoTree->SetBranchAddress("jet1_eta",&r_jet1_eta);
  recoTree->SetBranchAddress("jet0_phi",&r_jet0_phi);
  recoTree->SetBranchAddress("jet1_phi",&r_jet1_phi);

  recoTree->SetBranchAddress("jet0_E",&r_jet0_e);
  recoTree->SetBranchAddress("jet1_E",&r_jet1_e);

  recoTree->SetBranchAddress("DPhijj",&r_DPhijj);
  recoTree->SetBranchAddress("DEtajj",&r_DEtajj);
  recoTree->SetBranchAddress("DYjj",&r_DYjj);
  recoTree->SetBranchAddress("h_pt",&r_h_pt);
  recoTree->SetBranchAddress("h_phi",&r_h_phi);
  recoTree->SetBranchAddress("h_eta",&r_h_eta);

  recoTree->SetBranchAddress("MTllMET",&r_MTll);
  recoTree->SetBranchAddress("ptTot",&r_ptTot);
  recoTree->SetBranchAddress("CosStar",&r_cosTheta);
  recoTree->SetBranchAddress("Ptll",&r_Ptll);

  //float inSR;
  //recoTree->SetBranchAddress("inSR",&inSR);
  
  std::cout<<"defining truth variables\n";

  //set truth variables
  int t_eventNumber;
  double t_weight;
 

  double t_MT;
  double t_MET;
  
  double t_Mll;
  double t_Ptll;
  double t_DPhill;
  double t_DEtall;
  double t_DYll;
  double t_DRll;
  
  double t_Mjj;
  double t_Ptjj;
  double t_DPhijj;
  double t_DEtajj;
  double t_DYjj;
  double t_DRjj;
  
  double t_lep0_pt;
  double t_lep1_pt;
  double t_lep0_eta;
  double t_lep1_eta;
  double t_lep0_phi;
  double t_lep1_phi;
  double t_lep0_y;
  double t_lep1_y;
  double t_lep0_m;
  double t_lep1_m;
  double t_lep0_e;
  double t_lep1_e;

  double t_jet0_pt;
  double t_jet1_pt;
  double t_jet0_eta;
  double t_jet1_eta;
  double t_jet0_phi;
  double t_jet1_phi;
  double t_jet0_y;
  double t_jet1_y;
  double t_jet0_m;
  double t_jet1_m;
  double t_jet0_e;
  double t_jet1_e;

  double t_h_pt;
  double t_h_phi;
  double t_h_eta;  

  double t_MTll;
  double t_ptTot;
  double t_cosTheta;
  int t_nJets;

  std::cout<<"setting truth variables\n";

  truthTree->SetBranchAddress("eventNumber",&t_eventNumber);
  truthTree->SetBranchAddress("weight",&t_weight);
  truthTree->SetBranchAddress("MT",&t_MT);
  truthTree->SetBranchAddress("MET",&t_MET);
  std::cout<<"setting lep variables\n";
  truthTree->SetBranchAddress("Mll",&t_Mll);
  truthTree->SetBranchAddress("Ptll",&t_Ptll);
  truthTree->SetBranchAddress("DPhill",&t_DPhill);
  truthTree->SetBranchAddress("DEtall",&t_DEtall);
  truthTree->SetBranchAddress("DYll",&t_DYll);
    std::cout<<"setting jet variables\n";

  truthTree->SetBranchAddress("Mjj",&t_Mjj);
  truthTree->SetBranchAddress("lep0_pt",&t_lep0_pt);
  truthTree->SetBranchAddress("lep1_pt",&t_lep1_pt);
  truthTree->SetBranchAddress("lep0_eta",&t_lep0_eta);
  truthTree->SetBranchAddress("lep1_eta",&t_lep1_eta);
  truthTree->SetBranchAddress("lep0_phi",&t_lep0_phi);
  truthTree->SetBranchAddress("lep1_phi",&t_lep1_phi);
  truthTree->SetBranchAddress("lep0_y",&t_lep0_y);
  truthTree->SetBranchAddress("lep1_y",&t_lep1_y);
  truthTree->SetBranchAddress("lep0_m",&t_lep0_m);
  truthTree->SetBranchAddress("lep1_m",&t_lep1_m);
  truthTree->SetBranchAddress("lep0_e",&t_lep0_e);
  truthTree->SetBranchAddress("lep1_e",&t_lep1_e);
  std::cout<<"setting more jet variables\n";

  truthTree->SetBranchAddress("jet0_pt",&t_jet0_pt);
  truthTree->SetBranchAddress("jet1_pt",&t_jet1_pt);
  truthTree->SetBranchAddress("jet0_eta",&t_jet0_eta);
  truthTree->SetBranchAddress("jet1_eta",&t_jet1_eta);
  truthTree->SetBranchAddress("jet0_phi",&t_jet0_phi);
  truthTree->SetBranchAddress("jet1_phi",&t_jet1_phi);
  truthTree->SetBranchAddress("jet0_e",&t_jet0_e);
  truthTree->SetBranchAddress("jet1_e",&t_jet1_e);
  truthTree->SetBranchAddress("h_pt",&t_h_pt);
  truthTree->SetBranchAddress("h_phi",&t_h_phi);
  truthTree->SetBranchAddress("h_eta",&t_h_eta);
  truthTree->SetBranchAddress("jets",&t_nJets);
  truthTree->SetBranchAddress("DYjj",&t_DYjj);
  truthTree->SetBranchAddress("MTllMET",&t_MTll);
  truthTree->SetBranchAddress("ptTot",&t_ptTot);
  truthTree->SetBranchAddress("CosStar",&t_cosTheta);
  
  int nEvents=truthTree->GetEntries();
  std::cout<<"looping truth variables\n";

  TH1I * matched_eventNumbers=new TH1I("matched_event","matched_events",50,0,14000000);
  TH1I * reco_only_eventNumbers=new TH1I("reco_only__event","reco_only_events",50,0,14000000);
   TH1I * truth_only_eventNumbers=new TH1I("truth_event","truth_events",50,0,14000000);
   
  int mjjG0=0;
  for(int i=0; i < nEvents;i++){
    truthTree->GetEvent(i);
    eventNumber=t_eventNumber;
    truthMatch=true;
    if(IsInMap(EventMap,t_eventNumber)){
      recoTree->GetEvent(EventMap[t_eventNumber]);
      //m_r_inSR=inSR;
      //if(r_OLV==1 && m_r_DRll> 0.1 && r_MET > 20000 &&r_jet0_pt > 30000)
      matched_eventNumbers->Fill(t_eventNumber);
      recoMatch=true;
      m_t_weight=t_weight;
      m_t_MT=t_MT;
      m_t_MET=t_MET;
      m_r_OLV=r_OLV;

      m_t_Mll=t_Mll;
      m_t_Ptll=t_Ptll;
      m_t_DPhill=t_DPhill;
      m_t_DEtall=t_DEtall;
      m_t_DYll=t_DYll;
      m_t_DRll=Power(Power(m_t_DEtall,2)+Power(m_t_DPhill,2),0.5);

      m_t_Mjj=t_Mjj;
      m_t_Ptjj=t_Ptjj;
      m_t_DPhijj=t_DPhijj;
      m_t_DEtajj=t_DEtajj;
      m_t_DYjj=t_DYjj;
      m_t_DRjj=Power(Power(m_t_DEtajj,2)+Power(m_t_DPhijj,2),0.5);
      

  
      m_t_lep_pt_0=t_lep0_pt;
      m_t_lep_pt_1=t_lep1_pt;      ;      
      m_t_lep_eta_0=t_lep0_eta;
      m_t_lep_eta_1=t_lep1_eta;           
      m_t_lep_phi_0=t_lep0_phi;
      m_t_lep_phi_1=t_lep1_phi;      
      m_t_lep_y_0=t_lep0_y;
      m_t_lep_y_1=t_lep1_y;      
      m_t_lep_m_0=t_lep0_m;
      m_t_lep_m_1=t_lep1_m;      
      m_t_lep_e_0=t_lep0_e;
      m_t_lep_e_1=t_lep1_e;      

      m_t_jet_pt_0=t_jet0_pt;
      m_t_jet_pt_1=t_jet1_pt;      
      m_t_jet_eta_0=t_jet0_eta;
      m_t_jet_eta_1=t_jet1_eta;      
      m_t_jet_phi_0=t_jet0_phi;
      m_t_jet_phi_1=t_jet1_phi;      
      m_t_jet_y_0=t_jet0_y;
      m_t_jet_y_1=t_jet1_y;      
      m_t_jet_m_0=t_jet0_m;
      m_t_jet_m_1=t_jet1_m;      
      m_t_jet_e_0=t_jet0_e;
      m_t_jet_e_1=t_jet1_e;      

      
      m_t_h_pt=t_h_pt;
      m_t_h_phi=t_h_phi;
      m_t_h_eta=t_h_eta;

      m_t_MTll=t_MTll;
      m_t_ptTot=t_ptTot;
      m_t_cosTheta=t_cosTheta;
      m_t_nJets=t_nJets;
      
      t_DRl0j0=TMath::Power(TMath::Power(t_lep0_eta-t_jet0_eta,2)+TMath::Power(t_lep0_phi-t_jet0_phi,2),0.5);
      t_DRl1j0=TMath::Power(TMath::Power(t_lep1_eta-t_jet0_eta,2)+TMath::Power(t_lep1_phi-t_jet0_phi,2),0.5);
      t_DRl0j1=TMath::Power(TMath::Power(t_lep0_eta-t_jet1_eta,2)+TMath::Power(t_lep0_phi-t_jet1_phi,2),0.5);
      t_DRl1j1=TMath::Power(TMath::Power(t_lep1_eta-t_jet1_eta,2)+TMath::Power(t_lep1_phi-t_jet1_phi,2),0.5);

      m_r_weight=r_weight;
      m_r_MT=r_MT;
      m_r_MET=r_MET;
  
      m_r_Mll=r_Mll;
      m_r_Ptll=r_Ptll;
      m_r_DPhill=r_DPhill;
      m_r_DEtall=r_DEtall;
      m_r_DYll=r_DYll;
      m_r_DRll=Power(Power(m_r_DEtall,2)+Power(m_r_DPhill,2),0.5);

      m_r_Mjj=r_Mjj;
      m_r_Ptjj=r_jet0_pt+r_jet1_pt;
      m_r_DPhijj=r_DPhijj;
      m_r_DEtajj=r_DEtajj;
      m_r_DYjj=r_DYjj;
      m_r_DRjj=Power(Power(m_r_DEtajj,2)+Power(m_r_DPhijj,2),0.5);
  
      m_r_lep_pt_0=r_lep0_pt;
      m_r_lep_pt_1=r_lep1_pt;      
      m_r_lep_eta_0=r_lep0_eta;
      m_r_lep_eta_1=r_lep1_eta;
      m_r_lep_phi_0=r_lep0_phi;
      m_r_lep_phi_1=r_lep1_phi;      
      m_r_lep_y_0=r_lep0_y;
      m_r_lep_y_1=r_lep1_y;      
      m_r_lep_m_0=r_lep0_m;
      m_r_lep_m_1=r_lep1_m;      

      m_r_jet_pt_0=r_jet0_pt;
      m_r_jet_pt_1=r_jet1_pt;      
      m_r_jet_eta_0=r_jet0_eta;
      m_r_jet_eta_1=r_jet1_eta;
      m_r_jet_phi_0=r_jet0_phi;
      m_r_jet_phi_1=r_jet1_phi;
      m_r_jet_y_0=r_jet0_y;
      m_r_jet_y_1=r_jet1_y;      
      m_r_jet_m_0=r_jet0_m;
      m_r_jet_m_1=r_jet1_m;      
      m_r_h_pt=r_h_pt;
      m_r_h_phi=r_h_phi;
      m_r_h_eta=r_h_eta;

      m_r_MTll=r_MTll;
      m_r_ptTot=r_ptTot;
      m_r_cosTheta=r_cosTheta;
      m_r_nJets=r_nJets;
      r_DRl0j0=TMath::Power(TMath::Power(r_lep0_eta-r_jet0_eta,2)+TMath::Power(r_lep0_phi-r_jet0_phi,2),0.5);
      r_DRl1j0=TMath::Power(TMath::Power(r_lep1_eta-r_jet0_eta,2)+TMath::Power(r_lep1_phi-r_jet0_phi,2),0.5);
      r_DRl0j1=TMath::Power(TMath::Power(r_lep0_eta-r_jet1_eta,2)+TMath::Power(r_lep0_phi-r_jet0_phi,2),0.5);
      r_DRl1j1=TMath::Power(TMath::Power(r_lep1_eta-r_jet1_eta,2)+TMath::Power(r_lep1_phi-r_jet0_phi,2),0.5);
      EventMap.erase(t_eventNumber);
    }else{

      recoMatch=false;
      truth_only_eventNumbers->Fill(t_eventNumber);
      m_r_OLV=0;
      m_t_weight=t_weight;
      m_t_MT=t_MT;
      m_t_MET=t_MET;
  
      m_t_Mll=t_Mll;
      m_t_Ptll=t_Ptll;
      m_t_DPhill=t_DPhill;
      m_t_DEtall=t_DEtall;
      m_t_DYll=t_DYll;
      m_t_DRll=Power(Power(t_DPhill,2)+Power(t_DEtall,2),0.5);

      m_t_Mjj=t_Mjj;
      m_t_Ptjj=t_Ptjj;
      m_t_DPhijj=t_DPhijj;
      m_t_DEtajj=t_DEtajj;
      m_t_DYjj=t_DYjj;
      m_t_DRjj=Power(Power(m_t_DEtajj,2)+Power(m_t_DPhijj,2),0.5);
  
      m_t_lep_pt_0=t_lep0_pt;
      m_t_lep_pt_1=t_lep1_pt;      
      m_t_lep_eta_0=t_lep0_eta;
      m_t_lep_eta_1=t_lep1_eta;      
      m_t_lep_phi_0=t_lep0_phi;
      m_t_lep_phi_1=t_lep1_phi;      

      m_t_lep_y_0=t_lep0_y;
      m_t_lep_y_1=t_lep1_y;      
      m_t_lep_m_0=t_lep0_m;
      m_t_lep_m_1=t_lep1_m;      
      m_t_lep_e_0=t_lep0_e;
      m_t_lep_e_1=t_lep1_e;      

      m_t_jet_pt_0=t_jet0_pt;
      m_t_jet_pt_1=t_jet1_pt;      
      m_t_jet_eta_0=t_jet0_eta;
      m_t_jet_eta_1=t_jet1_eta;      
      m_t_jet_phi_0=t_jet0_phi;
      m_t_jet_phi_1=t_jet1_phi;      
      m_t_jet_y_0=t_jet0_y;
      m_t_jet_y_1=t_jet1_y;      
      m_t_jet_m_0=t_jet0_m;
      m_t_jet_m_1=t_jet1_m;      
      m_t_jet_e_0=t_jet0_e;
      m_t_jet_e_1=t_jet1_e;      

      m_t_h_pt=t_h_pt;
      m_t_h_phi=t_h_phi;
      m_t_h_eta=t_h_eta;
      m_t_MTll=t_MTll;
      m_t_ptTot=t_ptTot;
      m_t_cosTheta=t_cosTheta;
      m_t_nJets=t_nJets;

      t_DRl0j0=TMath::Power(TMath::Power(t_lep0_eta-t_jet0_eta,2)+TMath::Power(t_lep0_phi-t_jet0_phi,2),0.5);
      t_DRl1j0=TMath::Power(TMath::Power(t_lep1_eta-t_jet0_eta,2)+TMath::Power(t_lep1_phi-t_jet0_phi,2),0.5);
      t_DRl0j1=TMath::Power(TMath::Power(t_lep0_eta-t_jet1_eta,2)+TMath::Power(t_lep0_phi-t_jet1_phi,2),0.5);
      t_DRl1j1=TMath::Power(TMath::Power(t_lep1_eta-t_jet1_eta,2)+TMath::Power(t_lep1_phi-t_jet1_phi,2),0.5);

      m_r_weight=-101;
      m_r_MT=-101;
      m_r_MET=-101;
  
      m_r_Mll=-101;
      m_r_Ptll=-101;
      m_r_DPhill=-101;
      m_r_DEtall=-101;
      m_r_DYll=-101;
      m_r_DRll=-101;

      m_r_Mjj=-101;
      m_r_Ptjj=-101;
      m_r_DPhijj=-101;
      m_r_DEtajj=-101;
      m_r_DYjj=-101;
      m_r_DRjj=-101;
      m_r_nJets=-101;
  
      m_r_lep_pt_0=-101;
      m_r_lep_pt_1=-101;
      m_r_lep_eta_0=-101;
      m_r_lep_eta_1=-101;
      m_r_lep_phi_0=-101;
      m_r_lep_phi_1=-101;
      m_r_lep_y_0=-101;
      m_r_lep_y_1=-101;
      m_r_lep_m_0=-101;
      m_r_lep_m_1=-101;
      m_r_lep_e_0=-101;
      m_r_lep_e_1=-101;

      m_r_jet_pt_0=-101;
      m_r_jet_pt_1=-101;
      m_r_jet_eta_0=-101;
      m_r_jet_eta_1=-101;
      m_r_jet_phi_0=-101;
      m_r_jet_phi_1=-101;
      m_r_jet_y_0=-101;
      m_r_jet_y_1=-101;
      m_r_jet_m_0=-101;
      m_r_jet_m_1=-101;
      m_r_jet_e_0=-101;
      m_r_jet_e_1=-101;
      m_r_h_pt=-100;
      m_r_h_phi=-100;
      m_r_h_eta=-100;

      m_r_MTll=-100;
      m_r_ptTot=-100;
      m_r_cosTheta=-100;
      r_DRl0j0=-100;
      r_DRl1j0=-100;
      r_DRl0j1=-100;
      r_DRl1j1=-100;
      
    }
    merged->Fill();
  }//end of truth loop
  //loop over leftover reco events
  std::cout<<"looping leftover reco\n";
  std::cout<<EventMap.size()<<" Events\n";
  truthMatch=0;

  for(auto &iterator:EventMap){

    m_r_OLV=r_OLV;
    eventNumber=iterator.first;
    recoTree->GetEntry(iterator.second);
    recoMatch=1;
    truthMatch=0;
    m_r_weight=r_weight;
    m_r_MT=r_MT;
    m_r_MET=r_MET;

    m_r_Mll=r_Mll;
    m_r_Ptll=r_Ptll;
    m_r_DPhill=r_DPhill;
    m_r_DEtall=fabs(r_lep0_eta-r_lep1_eta);
    m_r_DYll=r_DYll;
    m_r_DRll=Power(Power(m_r_DEtall,2)+Power(m_r_DPhill,2),0.5);
    reco_only_eventNumbers->Fill(eventNumber);

    m_r_Mjj=r_Mjj;
    m_r_Ptjj=r_jet0_pt+r_jet1_pt;
    m_r_DPhijj=r_DPhijj;
    m_r_DEtajj=r_DEtajj;
    m_r_DYjj=r_DYjj;
    m_r_DRjj=Power(Power(m_r_DEtajj,2)+Power(m_r_DPhijj,2),0.5);
    
    m_r_lep_pt_0=r_lep0_pt;
    m_r_lep_pt_1=r_lep1_pt;    
    m_r_lep_eta_0=r_lep0_eta;
    m_r_lep_eta_1=r_lep1_eta;    
    m_r_lep_y_0=r_lep1_y;
    m_r_lep_y_1=r_lep1_y;    
    m_r_lep_m_0=r_lep1_m;
    m_r_lep_m_1=r_lep1_m;    
    m_r_lep_phi_0=r_lep0_phi;
    m_r_lep_phi_1=r_lep1_phi;
    
    m_r_jet_pt_0=r_jet0_pt;
    m_r_jet_pt_1=r_jet1_pt;    
    m_r_jet_eta_0=r_jet0_eta;
    m_r_jet_eta_1=r_jet1_eta;    
    m_r_jet_y_0=r_jet0_y;
    m_r_jet_y_1=r_jet1_y;    
    m_r_jet_m_0=r_jet0_m;
    m_r_jet_m_1=r_jet1_m;    
    m_r_jet_phi_0=r_jet0_phi;
    m_r_jet_phi_1=r_jet1_phi;
    m_r_h_pt=r_h_pt;
    m_r_h_phi=r_h_phi;
    m_r_h_eta=r_h_eta;
    m_r_MTll=r_MTll;
    m_r_ptTot=r_ptTot;
    m_r_cosTheta=r_cosTheta;
    m_r_nJets=r_nJets;

    r_DRl0j0=TMath::Power(TMath::Power(r_lep0_eta-r_jet0_eta,2)+TMath::Power(r_lep0_phi-r_jet0_phi,2),0.5);
    r_DRl1j0=TMath::Power(TMath::Power(r_lep1_eta-r_jet0_eta,2)+TMath::Power(r_lep1_phi-r_jet0_phi,2),0.5);
    r_DRl0j1=TMath::Power(TMath::Power(r_lep0_eta-r_jet1_eta,2)+TMath::Power(r_lep0_phi-r_jet0_phi,2),0.5);
    r_DRl1j1=TMath::Power(TMath::Power(r_lep1_eta-r_jet1_eta,2)+TMath::Power(r_lep1_phi-r_jet0_phi,2),0.5);
    
    m_t_weight=-101;
    m_t_MT=-101;
    m_t_MET=-101;
  
    m_t_Mll=-101;
    m_t_Ptll=-101;
    m_t_DPhill=-101;
    m_t_DEtall=-101;
    m_t_DYll=-101;
    m_t_DRll=-101;

    m_t_Mjj=-101;
    m_t_Ptjj=-101;
    m_t_DPhijj=-101;
    m_t_DEtajj=-101;
    m_t_DYjj=-101;
    m_t_DRjj=-101;
    m_t_nJets=-101;
      
    m_t_lep_pt_0=-101;
    m_t_lep_pt_1=-101;
    m_t_lep_eta_0=-101;
    m_t_lep_eta_1=-101;
    m_t_lep_phi_0=-101;
    m_t_lep_phi_1=-101;
    m_t_lep_y_0=-101;
    m_t_lep_y_1=-101;
    m_t_lep_m_0=-101;
    m_t_lep_m_1=-101;
    m_t_lep_e_0=-101;
    m_t_lep_e_1=-101;

    m_t_jet_pt_0=-101;
    m_t_jet_pt_1=-101;
    m_t_jet_eta_0=-101;
    m_t_jet_eta_1=-101;
    m_t_jet_phi_0=-101;
    m_t_jet_phi_1=-101;
    m_t_jet_y_0=-101;
    m_t_jet_y_1=-101;
    m_t_jet_m_0=-101;
    m_t_jet_m_1=-101;
    m_t_jet_e_0=-101;
    m_t_jet_e_1=-101;
    m_t_h_pt=-100;
    m_t_h_phi=-100;
    m_t_h_eta=-100;
    
    t_DRl0j0=-101;
    t_DRl1j0=-101;
    t_DRl0j1=-101;
    t_DRl1j1=-101;
    m_t_MTll=-100;
    m_t_ptTot=-100;
    m_t_cosTheta=-100;
    merged->Fill();
  }//end of reco only loop

  TCanvas *cav=new TCanvas();
  matched_eventNumbers->SetLineColor(kRed);
  reco_only_eventNumbers->SetLineColor(kBlue);
  truth_only_eventNumbers->SetLineColor(kGreen);
  double max=matched_eventNumbers->GetMaximum();
  matched_eventNumbers->GetYaxis()->SetRangeUser(100000,max);
  matched_eventNumbers->Draw();
  reco_only_eventNumbers->Draw("SAME");
  truth_only_eventNumbers->Draw("SAME");
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  cav->Print("EventNumberHist.pdf");
  delete cav;
  return merged;
}








