#include "merge.h"
#include <unordered_map>
#include "config.h"
#include <vector>
#include <string>
#include "systematics.h"
#include "unfold.h"
#include "tools.h"
#include "RooUnfoldResponse.h"
#include <TFile.h>
#include <TCanvas.h>
#include <TH1.h>
#include "RooUnfoldBayes.h"
#include "RooUnfoldSvd.h"
#include <TLegend.h>
#include <fstream>
#include <THStack.h>
#include <TStyle.h>


systematics::systematics(TTree * truth_nominal,TTree *reco_nominal){
  this->truth_nominal=truth_nominal;
  this->reco_nominal=reco_nominal;
}

void systematics::add_systematic(std::string name,TTree * systematic){
  std::vector<std::string> split_str=split(name,"_");
  std::string to_erase=split_str[0]+"_";
  size_t pos = name.find(to_erase);
  name.erase(pos, to_erase.length());
  std::vector<std::string> descrip=split(name,"__");
  if(descrip.size() < 2)
    return;
  if(substring(descrip[1],"up"))
     this->up_systs[descrip[0]]=systematic;
  else if(substring(descrip[1],"down"))
    this->down_systs[descrip[0]]=systematic;
  else
    std::cout<<"error::this is neither an up nor down variation\n";
}

systematics::~systematics(){
  up_systs.clear();
  down_systs.clear();
  delete this->truth_nominal;
  delete this->reco_nominal;
}

RooUnfoldResponse systematics::get_nominal_response(std::string variable){
  RooUnfoldResponse nominal=UnfoldInPlace(variable,this->truth_nominal,this->reco_nominal);
  return nominal;
}
RooUnfoldResponse systematics::get_up_response(std::string systematic,std::string variable){
  std::cout<<"RECO SYSTEMATIC UP: "<<this->up_systs[systematic]->GetName()<<"\n";
  RooUnfoldResponse up_response=UnfoldInPlace(variable,this->truth_nominal,this->up_systs[systematic]);
  delete up_systs[systematic];
  up_systs.erase(systematic);

  return up_response;
}
RooUnfoldResponse systematics::get_down_response(std::string systematic,std::string variable){
  std::cout<<"RECO SYSTEMATIC down: "<<this->down_systs[systematic]->GetName()<<"\n";
  RooUnfoldResponse down_response=UnfoldInPlace(variable,this->truth_nominal,this->down_systs[systematic]);
  delete down_systs[systematic];
  down_systs.erase(systematic);
  return down_response;
 
}

TH1F * systematics::get_up_reco_hist(std::string systematic,std::string variable,std::string sample){
  TH1F * hist=fill_hist(this->up_systs[systematic],variable,systematic+sample+"_up",true);
  delete up_systs[systematic];
  up_systs.erase(systematic);
  return hist;
}

TH1F* systematics::get_down_reco_hist(std::string systematic,std::string variable,std::string sample){
  TH1F * hist=fill_hist(this->down_systs[systematic],variable,systematic+sample+"_down",true);
  delete down_systs[systematic];
  down_systs.erase(systematic);
  return hist;

}
std::vector<std::string> systematics::get_systematics(){
  std::vector<std::string> keys
    ;
  for(auto &pair:up_systs){
    keys.push_back(pair.first);
  }
  return keys;
}

//add fakes
void add_systematic_background(RooUnfoldResponse & response,std::string sample,std::string systematic, std::string variable){
  std::cout<<"adding background "<<sample<<"\n";
  std::string syst_file="ntupls/systematics/"+sample+".root";
  TFile *sys_trees=TFile::Open(syst_file.c_str());
  std::vector<std::string> split_str=split(systematic,":");
  std::string syst_name=split_str[0];
  std::string var=split_str[1];
  std::cout<<"opening tree"<< sample+"_"+syst_name+"__1"+var<<"\n";
  TTree * syst_tree=(TTree*)sys_trees->Get((sample+"_"+syst_name+"__1"+var).c_str());
  fill_background(response,syst_tree,variable);
  std::cout<<"added background "<<sample+"_"+syst_name+"__1"+var<<"\n";

  delete syst_tree;
  sys_trees->Close();
  delete sys_trees;
}

void save_systematics_response(std::string variable,std::string sample){
  //reco trees
  std::string syst_file="ntupls/systematics/"+sample+".root";
  TFile *sys_trees=TFile::Open(syst_file.c_str());
  TTree * reco_nominal=(TTree*)sys_trees->Get((sample+"_nominal").c_str());
  systematics reco_systematics(NULL,reco_nominal);

  //save systematics
  
  TFile *reco_hists_file=TFile::Open((variable+"_unweighted_reco_systs.root").c_str(),"UPDATE");
  TH1F* reco_nominal_hist=fill_hist(reco_nominal,variable,sample+"_nominal",true);
  TDirectory *nominal_directory = reco_hists_file->mkdir("nominal");

  if(!(nominal_directory)){ //already exists
    reco_hists_file->cd("nominal");
    reco_nominal_hist->Write();
  }else{
    nominal_directory->cd();
    reco_nominal_hist->Write();
  }
  
  //loop over file
  TKey *key;
  TIter next(sys_trees->GetListOfKeys());  
  while ((key = (TKey *) next())) {
    TTree * sys_tree = (TTree*)key->ReadObj();
    reco_systematics.add_systematic(key->GetName(),sys_tree);
  }

  //list of systematics
  std::vector<std::string> print_systematics=reco_systematics.get_systematics();  
  for(auto &syst : print_systematics){
    reco_hists_file->cd();
    TDirectory *syst_directory = reco_hists_file->mkdir(syst.c_str());
    
    TH1F* reco_up_hist=reco_systematics.get_up_reco_hist(syst,variable,sample);
    TH1F* reco_down_hist=reco_systematics.get_down_reco_hist(syst,variable,sample);
    
    if(!(syst_directory)){ //already exists
      reco_hists_file->cd(syst.c_str());
      reco_up_hist->Write();
      reco_down_hist->Write();

    }else{
      syst_directory->cd();
      reco_up_hist->Write();
      reco_down_hist->Write();

    }
  }
  reco_hists_file->Close();
  delete reco_hists_file;
}

//syst is of the form systeamtic_name:up
TH1F* sum_samples(std::vector<std::string> &samples, std::string variable,std::string syst){

  int colors[]={kMagenta,kYellow,kBlue,kGreen,kOrange,kViolet,kSpring,kBlack};
  TH1F* total_hist=new TH1F((variable+syst).c_str(),(variable+" "+syst).c_str(),distribution_maps[variable].bins,distribution_maps[variable].binning);
  TFile * reco_hists=TFile::Open((variable+"_unweighted_reco_systs.root").c_str());
  //loop over file
  bool nominal=substring(syst,"nominal");
  std::string syst_name; //systematic name
  std::string var; //up or down
  if(!nominal){
    std::cout<<syst<<"\n";
    std::vector<std::string> split_str=split(syst,":");
    syst_name=split_str[0];
    var=split_str[1];
  }else{
    syst_name=syst;
  }
  TDirectory * directory=reco_hists->GetDirectory(syst_name.c_str());

  TKey *parent_key;
  std::cout<<"starting loop\n";
  TIter hist_iter(directory->GetListOfKeys());
  THStack * mcStack = new THStack("mcStack"," stacked");
  int i=0;
  if (StringInVec("vbf",samples))
    i=0;
  std::ofstream sample_comparison_file;
  sample_comparison_file.open("sample_comparisons.txt",std::ios::app);
  if(!nominal)
    sample_comparison_file<<syst;

  while ((parent_key = (TKey *) hist_iter())) {
    TH1F * hist = static_cast<TH1F*>(parent_key->ReadObj());
    std::string hist_name=std::string(parent_key->GetName());
    std::cout<<hist_name<<"\n";
    std::vector<std::string> split_name=split(hist_name,"_");
    std::string hist_sample=split_name[split_name.size()-2];
    std::string UpOrDown=split_name[split_name.size()-1];
    for(auto &sample :samples){
      if(substring(hist_sample,sample)){
        if(nominal){
          std::ofstream sample_systematic;
          sample_systematic.open((variable+"_"+sample+"_square_weighted_comparisons.txt").c_str(),std::ios::app);
          std::cout<<"sample: "<<sample<<" bin value:"<<hist->GetBinContent(1)<<"\n";

          total_hist->Add(hist);
          hist->SetName(sample.c_str());
          if(substring(sample,"vbf"))
            hist->SetFillColor(kBlue);
          else
            hist->SetFillColor(colors[i++ % 9]);
          hist->SetFillStyle(1001);
          mcStack->Add(hist);
          sample_systematic<<"nominal,nominal";
          for(int i=0;i<=hist->GetNbinsX();i++)
            sample_systematic<<","<<hist->GetBinContent(i);
          sample_systematic<<"\n";
          sample_systematic.close();
        }
        else if(substring(UpOrDown,var)){
          std::ofstream sample_systematic;
          sample_systematic.open((variable+"_"+sample+"_square_weighted_comparisons.txt").c_str(),std::ios::app);
          //std::cout<<"pushed: "<<sample<<"\n";
          //sample_comparison_file<<","<<hist->GetBinContent(1);
          hist->SetName(sample.c_str());
          hist->SetTitle(sample.c_str());

          //std::cout<<"filling variation "<<var<<"\n";
          total_hist->Add(hist);
          hist->SetFillColor(colors[i++ % 9]);
          hist->SetFillStyle(1001);
          mcStack->Add(hist);
          sample_systematic<<syst<<","<<var;
          for(int i=0;i<=hist->GetNbinsX();i++)
            sample_systematic<<","<<hist->GetBinContent(i);
          sample_systematic<<"\n";
          sample_systematic.close();
        }
        break; 
      }
    }
  }
  /*
  while ((parent_key = (TKey *) next())) {
    reco_hists->cd();
    TDirectoryFile * directory=(TDirectoryFile*)reco_hists->GetDirectory(parent_key->GetName());
    TIter hists_iter(directory->GetListOfKeys());
    while ((child_key = (TKey *) hists_iter())){
      std::cout<<child_key->GetName()<<"\n";
    }
  }
  */
  TCanvas * c1 = new TCanvas("c1","stacked hists",700,900);
  c1->cd();

  if (StringInVec("vbf",samples))
    mcStack->SetTitle((variable+" "+syst+" all").c_str());
  else
    mcStack->SetTitle((variable+" "+syst+" background").c_str());
  bool draw=false;
  if(draw){
  mcStack->Draw("hist");
  std::cout<<"drew hist canvas\n";

  gStyle->SetOptStat(0);
  //gPad->SetLogy(1);
  gPad->BuildLegend(0.75,0.75,0.95,0.95,"");
  if (StringInVec("vbf",samples))
    c1->Print((variable+"_"+syst+"_all_stacked.pdf").c_str());
  else
    c1->Print((variable+"_"+syst+"_background_stacked.pdf").c_str());
  reco_hists->Close();
  delete c1;
  delete reco_hists;
  }
  sample_comparison_file<<"\n";
  sample_comparison_file.close();
  return total_hist;
}

void create_reco_systematic(std::string variable){

  std::ofstream systematic_comparison_file;
  systematic_comparison_file.open((variable+"_systematic_comparisons.txt").c_str(),std::ios::app);
  
  std::string syst_file="ntupls/systematics/vbf.root";
  TFile *sys_trees=TFile::Open(syst_file.c_str());
  systematics reco_systematics(NULL,NULL);
  
  TKey *key;
  TIter next(sys_trees->GetListOfKeys());
  std::cout<<"starting loop\n";

  //loop over file
  /*
  while ((key = (TKey *) next())) {
    TTree * sys_tree = (TTree*)key->ReadObj();
    reco_systematics.add_systematic(key->GetName(),sys_tree);
  }
  */
  reco_systematics.add_systematic("vbf_FT_EFF_Eigen_B_0__1down",(TTree*)sys_trees->Get("vbf_FT_EFF_Eigen_B_0__1down"));
  reco_systematics.add_systematic("vbf_JET_Flavor_Composition__1down",(TTree*)sys_trees->Get("vbf_JET_Flavor_Composition__1down"));
  reco_systematics.add_systematic("vbf_JET_EtaIntercalibration_Modelling__1down",(TTree*)sys_trees->Get("vbf_JET_EtaIntercalibration_Modelling__1down"));
  reco_systematics.add_systematic("vbf_EL_EFF_ID_CorrUncertaintyNP14__1down",(TTree*)sys_trees->Get("vbf_EL_EFF_ID_CorrUncertaintyNP14__1down"));
 reco_systematics.add_systematic("vbf_JET_JER_EffectiveNP_1_MCsmear__1down",(TTree*)sys_trees->Get("vbf_JET_JER_EffectiveNP_1_MCsmear__1down"));
 reco_systematics.add_systematic("vbf_EL_EFF_ID_CorrUncertaintyNP14__1down",(TTree*)sys_trees->Get("FT_EFF_Eigen_B_1__1down"));
  
  
  reco_systematics.add_systematic("vbf_FT_EFF_Eigen_B_0__1up",(TTree*)sys_trees->Get("vbf_FT_EFF_Eigen_B_0__1up"));
  reco_systematics.add_systematic("vbf_JET_Flavor_Composition__1up",(TTree*)sys_trees->Get("vbf_JET_Flavor_Composition__1up"));
  reco_systematics.add_systematic("vbf_JET_EtaIntercalibration_Modelling__1up",(TTree*)sys_trees->Get("vbf_JET_EtaIntercalibration_Modelling__1up"));
  reco_systematics.add_systematic("vbf_EL_EFF_ID_CorrUncertaintyNP14__1up",(TTree*)sys_trees->Get("vbf_EL_EFF_ID_CorrUncertaintyNP14__1up"));
  reco_systematics.add_systematic("vbf_JET_JER_EffectiveNP_1_MCsmear__1up",(TTree*)sys_trees->Get("vbf_JET_JER_EffectiveNP_1_MCsmear__1up"));
  reco_systematics.add_systematic("vbf_EL_EFF_ID_CorrUncertaintyNP14__1up",(TTree*)sys_trees->Get("FT_EFF_Eigen_B_1__1up"));
  
  std::vector<std::string> print_systematics=reco_systematics.get_systematics();  
  std::vector<std::string> all_samples={"vbf","ggf","diboson","Vgamma","htt","vh","Zjets","top"};
  std::vector<std::string> back_samples={"ggf","diboson","Vgamma","htt","vh","Zjets","top"};
  std::vector<std::string> signal_samples={"vbf"};

  std::vector<std::string> vbf_only={"vbf"};
  TH1F * nominal_vbf=sum_samples(vbf_only, variable,"nominal");
  TH1F* nominal_data=sum_samples(all_samples, variable,"nominal");
  TH1F* background_nominal=sum_samples(back_samples, variable,"nominal");


  nominal_vbf->SetLineColor(kBlue);
  nominal_vbf->SetName((variable+"_nominal_vbf").c_str());
  DrawHist<TH1F>(nominal_vbf);

 
  systematic_comparison_file<<"systematic"<<",variation";
  for(int i=0;i<=nominal_vbf->GetNbinsX();i++)
    {
      systematic_comparison_file<<",bin"<<i;
    }
  systematic_comparison_file<<"\n";
  systematic_comparison_file<<"nominal"<<",nominal";  
  for(int i=0;i<=nominal_vbf->GetNbinsX();i++)
    {
      systematic_comparison_file<<","<<nominal_vbf->GetBinContent(i);
    }
  systematic_comparison_file<<"\n";
  
  background_nominal->Divide(nominal_vbf);
  background_nominal->SetName("nominal_b_s_ratio");
  
  DrawHist<TH1F>(background_nominal);

  for(auto &systematic : print_systematics){
    std::cout<<"fillling systematic"<<systematic<<"\n";
    TH1F* background_up=sum_samples(back_samples, variable,systematic+":up");
    TH1F* background_down=sum_samples(back_samples,variable,systematic+":down");
    TH1F * vbf_up_estimate=(TH1F*)nominal_data->Clone((systematic+"vbf_up_estimate").c_str());
    TH1F * vbf_down_estimate=(TH1F*)nominal_data->Clone((systematic+"vbf_down_estimate").c_str());
    TH1F * vbf_down_real=sum_samples(signal_samples,variable,systematic+":down");
    TH1F * vbf_up_real=sum_samples(signal_samples,variable,systematic+":up");

    vbf_up_estimate->Add(background_down,-1);
    vbf_down_estimate->Add(background_up,-1);

    //vbf_up_estimate->Divide(nominal_vbf);
    //vbf_down_estimate->Divide(nominal_vbf);
    
    vbf_up_estimate->SetLineColor(kRed);
    vbf_down_estimate->SetLineColor(kGreen);

    for(int i=0;i<vbf_down_estimate->GetNbinsX();i++){
      vbf_down_estimate->SetBinError(i,0);
      vbf_up_estimate->SetBinError(i,0);
    }
    vbf_up_estimate->SetTitle((systematic+" "+ variable+" distribution up estimate").c_str());
    vbf_down_estimate->SetTitle((systematic+ " "+variable+" distribution down estimate").c_str());
    DrawHist<TH1F>(vbf_up_estimate);
    DrawHist<TH1F>(vbf_down_estimate);
    
    vbf_down_real->Divide(background_down);
    vbf_down_real->SetName((systematic+"_s_b_ratio").c_str());
    DrawHist<TH1F>(vbf_down_real);
    vbf_up_real->Divide(background_up);
    vbf_up_real->SetName((systematic+"_s_b_ration").c_str());
    DrawHist<TH1F>(vbf_up_real);
    systematic_comparison_file<<systematic<<",up";
      
    for(int i=0;i<=nominal_vbf->GetNbinsX();i++)
      {
        systematic_comparison_file<<","<<vbf_up_estimate->GetBinContent(i);
      }
    systematic_comparison_file<<"\n";
    systematic_comparison_file<<systematic<<",down";
    for(int i=0;i<=nominal_vbf->GetNbinsX();i++)
      {
        systematic_comparison_file<<","<<vbf_down_estimate->GetBinContent(i);
      }
    systematic_comparison_file<<"\n";

  }
  systematic_comparison_file.close();  

}
std::vector<double> analyze_systematics(std::string variable,int iterations){
  std::cout<<"analyzing systematics\n";
  std::string truth_file_path="ntupls/truth/truth_ntuple_vbf_v21_c16a.root";
  std::string reco_file_path="ntupls/reco/reco_ntuple_vbf_v21_c16a.root";
  TFile *truth_file=TFile::Open(truth_file_path.c_str());
  //TFile *reco_file=TFile::Open(reco_file_path.c_str());
  TTree * truth_nominal=(TTree*)truth_file->Get("Vbf_Total");
  std::cout<<"opened trees\n";

  std::string syst_file="ntupls/systematics/vbf.root";
  TFile *sys_trees=TFile::Open(syst_file.c_str());
  TTree * reco_nominal=(TTree*)sys_trees->Get("vbf_nominal");
  systematics reco_systematics(truth_nominal,reco_nominal);

  TKey *key;
  TIter next(sys_trees->GetListOfKeys());
  std::cout<<"starting loop\n";

  //loop over file
  while ((key = (TKey *) next())) {
    TTree * sys_tree = (TTree*)key->ReadObj();
    reco_systematics.add_systematic(key->GetName(),sys_tree);
  }

  std::vector<std::string> print_systematics=reco_systematics.get_systematics();  
  std::ofstream syst_comparison_file;
  std::ofstream syst_reco;
  //nominal response
  RooUnfoldResponse nominal_resp=reco_systematics.get_nominal_response(variable);
  TH1F * reco_nom_hist=static_cast<TH1F*>(nominal_resp.Hmeasured());
  reco_nom_hist->SetName("mjj_Reco_nom");
  auto unfolded_nom=unfold_bayes(nominal_resp,reco_nom_hist,iterations);
  TH1F* unfolded_nom_hist=(TH1F*)unfolded_nom.Hreco();
  std::vector<double> unfold_sys_error(reco_nom_hist->GetNbinsX(),0);
  double error;

  //save results to file
  
  syst_comparison_file.open((variable+"_systematics_comparison.csv").c_str(),ios::app);
  syst_comparison_file<<"systematic";
  syst_comparison_file<<",UnfoldedOrReco";
  syst_comparison_file<<",UpOrDown";
  for(int i=0;i<=reco_nom_hist->GetNbinsX();i++){
    syst_comparison_file<<",Bin"<<i;
  }
  syst_comparison_file<<"\n";
  syst_comparison_file<<"nominal";
  syst_comparison_file<<",Unfolded";
  syst_comparison_file<<",nominal";

  for(int i=0;i<=reco_nom_hist->GetNbinsX();i++){
    syst_comparison_file<<","<<unfolded_nom_hist->GetBinContent(i);
  }
  syst_comparison_file<<"\n";
  
  syst_comparison_file<<"nominal";
  syst_comparison_file<<",reco";
  syst_comparison_file<<",nominal";

    for(int i=0;i<=reco_nom_hist->GetNbinsX();i++){
      syst_comparison_file<<","<<reco_nom_hist->GetBinContent(i);
    }
    syst_comparison_file<<"\n";
  std::vector<std::string> all_samples={"vbf","Vgamma","vh","ggf","Zjets","diboson","top"};
  std::vector<std::string> back_samples={"Vgamma","vh","ggf","Zjets","diboson","top"};
  TH1F* nominal_data=sum_samples(all_samples, "Mjj","nominal");

  for(auto &syst : print_systematics){
    std::cout<<"getting responses\n";
    std::cout<<"creating up and down responses \n";
    RooUnfoldResponse up_response=reco_systematics.get_up_response(syst,variable);
    RooUnfoldResponse down_response=reco_systematics.get_down_response(syst,variable);
    for(auto & sample : back_samples){
      std::cout<<"adding background sample:"<<sample<<"\n";

      add_systematic_background(up_response,sample,syst+":up",variable);
      add_systematic_background(down_response,sample,syst+":down",variable);
    }
    std::cout<<"unfolding systematics\n";

    RooUnfoldSvd unfolded_up=unfold_svd(up_response,nominal_data,2);
    RooUnfoldSvd unfolded_down=unfold_svd(down_response,nominal_data,2);


    TH1F* unfolded_up_hist=(TH1F*)unfolded_up.Hreco();
    unfolded_up_hist->SetName((variable+"unfolded_up").c_str());
    TH1F* unfolded_down_hist=(TH1F*)unfolded_down.Hreco();
    unfolded_down_hist->SetName((variable+"unfolded_down").c_str());
    TH1F * reco_up_hist=static_cast<TH1F*>(up_response.Hmeasured());
    reco_up_hist->SetName("mjj_Reco_up");
    TH1F * reco_down_hist=static_cast<TH1F*>(down_response.Hmeasured());
    reco_down_hist->SetName("mjj_Reco_down");

    TH1F * down_fakes=static_cast<TH1F*>(down_response.Hfakes());
    TH1F * up_fakes=static_cast<TH1F*>(up_response.Hfakes());
    down_fakes->SetName((syst+"down_fakes").c_str());
    up_fakes->SetName((syst+"up_fakes").c_str());

    
    syst_comparison_file<<syst;
    syst_comparison_file<<",Unfolded";
    syst_comparison_file<<",up";

    for(int i=0;i<=reco_nom_hist->GetNbinsX();i++){
      syst_comparison_file<<","<<unfolded_up_hist->GetBinContent(i);
    }
    syst_comparison_file<<"\n";
    syst_comparison_file<<syst;
    syst_comparison_file<<",Unfolded";
    syst_comparison_file<<",down";

    for(int i=0;i<=reco_nom_hist->GetNbinsX();i++){
      syst_comparison_file<<","<<unfolded_down_hist->GetBinContent(i);
    }
    syst_comparison_file<<"\n";
    
    syst_comparison_file<<syst;
    syst_comparison_file<<",reco";
    syst_comparison_file<<",up";
    reco_up_hist->Add(up_fakes,-1);
    reco_down_hist->Add(down_fakes,-1);

    for(int i=0;i<=reco_nom_hist->GetNbinsX();i++){
      syst_comparison_file<<","<<reco_up_hist->GetBinContent(i);
    }
    syst_comparison_file<<"\n";
    
    syst_comparison_file<<syst;
    syst_comparison_file<<",reco";
    syst_comparison_file<<",down";

    for(int i=0;i<=reco_nom_hist->GetNbinsX();i++){
      syst_comparison_file<<","<<reco_down_hist->GetBinContent(i);
    }
    syst_comparison_file<<"\n";
 
    std::cout<<"Drawing results\n";

    double nom_max=unfolded_nom_hist->GetMaximum();
    double up_max=unfolded_up_hist->GetMaximum();
    double down_max=unfolded_down_hist->GetMaximum();

    double nom_min=reco_nom_hist->GetMinimum();
    double up_min=reco_up_hist->GetMinimum();
    double down_min=reco_down_hist->GetMinimum();

    double hist_max=std::max(nom_max,std::max(up_max,down_max));
    double hist_min=std::min(nom_min,std::min(up_min,down_min));
    
    TCanvas *cav=new TCanvas();
    std::cout<<"created canvas\n";
    cav->cd();
    reco_up_hist->SetLineColor(kGreen);
    reco_up_hist->SetLineStyle(10);
    reco_down_hist->SetLineColor(kMagenta);
    reco_down_hist->SetLineStyle(1);
    reco_nom_hist->SetLineColor(kBlue);
    
    unfolded_nom_hist->SetLineColor(kYellow);
    unfolded_up_hist->SetLineColor(kOrange);
    unfolded_up_hist->SetLineStyle(10);
    unfolded_down_hist->SetLineColor(kRed);
    unfolded_down_hist->SetLineStyle(1);

    unfolded_up_hist->SetMinimum(-10);
    unfolded_up_hist->SetMaximum(60);
    unfolded_up_hist->SetStats(0);
    
    unfolded_up_hist->SetTitle((syst+" "+variable).c_str());
    unfolded_up_hist->Draw("");
    unfolded_down_hist->Draw("same");
    unfolded_nom_hist->Draw("same");
    reco_down_hist->Draw("same");
    reco_up_hist->Draw("same");
    reco_nom_hist->Draw("same");
    auto stat_legend = new TLegend(0.6,0.7,0.98,0.9);
    stat_legend->AddEntry(unfolded_nom_hist,(std::string("unfolded nom, Events: ")+std::to_string(unfolded_nom_hist->Integral())).c_str(),"f");
    stat_legend->AddEntry(unfolded_up_hist,(std::string("unfolded up, Events: ")+std::to_string(unfolded_up_hist->Integral())).c_str(),"f");
    stat_legend->AddEntry(unfolded_down_hist,(std::string("unfolded down, Event: ")+std::to_string(unfolded_down_hist->Integral())).c_str(),"f");
    stat_legend->AddEntry(reco_nom_hist,(std::string("reco nom, Events: ")+std::to_string(reco_down_hist->Integral())).c_str(),"f");
    stat_legend->AddEntry(reco_up_hist,(std::string("reco up , Events:")+std::to_string(reco_up_hist->Integral())).c_str(),"f");
    stat_legend->AddEntry(reco_down_hist,(std::string("reco down, Events: ")+std::to_string(reco_down_hist->Integral())).c_str(),"f");

    stat_legend->Draw();
    cav->Print((syst+variable+"_syst_comparisons.pdf").c_str());
    delete stat_legend;
    delete cav;

  }

  syst_comparison_file.close();
  syst_reco.close();
  return unfold_sys_error;
}

