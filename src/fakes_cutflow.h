#ifndef FAKES_CUTFLOW_H
#define FAKES_CUTFLOW_H

#include <iostream>
#include <TH1F.h>
#include <string>
#include <vector>
#include <TCanvas.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>
#include <TH2F.h>
#include <TTree.h>
#include <map>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TFolder.h>
#include <TRandom3.h>
#include <TProfile.h>
#include "unfold.h"
#include <fstream>
#include <TMath.h>


struct cut{
  std::string variable;
  float num;
  std::string comp;
};
void run_cutflow(TTree * truth_tree,TTree *reco_tree,std::string name);
void loop_fakes_ntuples(TTree *truth_tree,TTree *reco_tree, std::string name,std::vector<cut> &reco_cutflow,std::vector<cut> &truth_cutflow);

bool check_cut(cut & cut_value, float * cut_memory);
bool check_cut(cut & cut_value, double * cut_memory);
#endif
