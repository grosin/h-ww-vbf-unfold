#ifndef TOOLS_H
#define TOOLS_H
#include <iostream>
#include <TH1F.h>
#include <string>
#include <vector>
#include <TCanvas.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>
#include <TH2F.h>
#include <TTree.h>
#include <TTree.h>
#include <map>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TFolder.h>
#include "real_data_analysis.h"
#include <TRandom3.h>
#include <TMatrixDSym.h>

void DrawTree(TTree *tree, std::string type="pdf");
void GetBinByBinCFactors(TH1F * reco, TH1F * truth);
map<string,TH1F*> GetHistograms(TFolder *folder);
map<string,TH1F*> GetHistograms(TFolder *folder);
void loop_folder(TFolder * fold);
map<string,TH1F*> loop_file(TFile * histograms);
void analyze_histograms();
bool substring(std::string large_string,std::string sub_string);
std::string delete_string(const char *,const char*);
void density_histogram(TH1 *hist);
void draw_ratio(TH1 * Hist1,TH1* Hist2,const char * title );
std::vector<double> multivar_gaus(std::vector<double> &means,TMatrixDSym &cov);
template<typename T> inline
void DrawHist(T *hist, std::string type="pdf",int log=0){
  TCanvas *cDraw= new TCanvas("cDraw", "cDraw",900,900);
  cDraw->cd();
  std::string name=hist->GetName();
  name=name+"."+type;
  cDraw->SetWindowSize(900,900);
  hist->Draw("COLZ");
  gPad->SetLogy(log);
  cDraw->SetWindowSize(900,900);
  cDraw->Print(name.c_str());
  delete cDraw;
}
template<typename T> inline
void DrawError(T *hist, std::string type="pdf",int log=0){
  T * error=(T*)hist->Clone("error");
  for(int i=0;i<=hist->GetNbinsX();i++){
    error->SetBinContent(i,pow(hist->GetBinContent(i),0.5)/hist->GetBinContent(i));
    error->SetBinError(i,0);
  }
  TCanvas *cDraw= new TCanvas("cDraw", "cDraw",900,900);
  cout<<"creating pads\n";
  cDraw->cd();
  TPad* pad1 = new TPad("pad1_r", "pad1_r",0.,.3,1.,1);
  pad1->Draw();
  pad1->cd();
  pad1->SetBottomMargin(0);
  hist->Draw("E1");
  gPad->SetLogy(log);
  cDraw->cd();
  std::string name=hist->GetName();
  name=name+"."+type;

  TPad* pad2 = new TPad("pad2_r", "pad2_r",0,0.05,1,0.3);
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.25);
  pad2->SetGridx(); // vertical grid
  pad2->Draw();
  pad2->cd();
  error->SetTitle("Error");
  error->Draw("E1");
  cDraw->SetWindowSize(900,900);
  cDraw->Print(name.c_str());
  delete cDraw;

}
extern TRandom3 *RNG;


#endif /* TOOLS_H */
