#include <iostream>
#include <TH1F.h>
#include <string>
#include <vector>
#include <TCanvas.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLine.h>
#include <TH2F.h>
#include <TTree.h>
#include <TTree.h>
#include <map>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TFolder.h>
#include "real_data_analysis.h"
#include "tools.h"
#include  <TGaxis.h>
#include <TMath.h>
#include <TMatrixDSymEigen.h>
#include <TRandom3.h>
#include <TMatrixDSym.h>
TRandom3 *RNG = new TRandom3(0);

void draw_ratio(TH1 * Hist1,TH1* Hist2,const char * title ){

  TCanvas *c1 = new TCanvas("c1", "c1",1000,1000);
  TPad *pad1 = new TPad("pad1", "pad1",0.,.3,1.,1);
  pad1->SetBottomMargin(0);
  pad1->Draw();
  pad1->cd();
  if(title[0]!='\0'){
    Hist1->SetTitle(title);}
  auto legend = new TLegend(0.1,0.8,0.48,0.9);
  legend->AddEntry(Hist1,Hist1->GetTitle(),"f");
  legend->AddEntry(Hist2,Hist2->GetTitle(),"f");
  Hist1->SetMinimum(0);
  Hist1->Draw();
  Hist2->Draw("SAME");
  legend->Draw();
  Hist1->GetYaxis();
  TGaxis *axis = new TGaxis( -5, 20, -5, 220, 20,220,510,"");
  axis->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  axis->SetLabelSize(15);
  //axis->Draw();
  c1->cd();
  TPad *pad2 = new TPad("pad2", "pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.2);
  pad2->SetGridx(); // vertical grid
  pad2->Draw();
  pad2->cd();

  TH1F *h3 = (TH1F*)Hist2->Clone("h3");
  h3->Divide(Hist1);
  h3->Draw();

  Hist1->GetYaxis()->SetTitleSize(20);
  Hist1->GetYaxis()->SetTitleFont(43);
  Hist1->GetYaxis()->SetTitleOffset(1.55);
  h3->GetYaxis()->SetTitle("ratio");
  h3->SetStats(0);
  //h3->GetYaxis()->SetNdivisions(505);
  h3->GetYaxis()->SetTitleSize(20);
  h3->GetYaxis()->SetTitleFont(43);
  h3->GetYaxis()->SetTitleOffset(1.55);
  h3->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  h3->GetYaxis()->SetLabelSize(15);

   // X axis ratio plot settings
  //h3->GetXaxis()->SetTitleSize(20);
  //h3->GetXaxis()->SetTitleFont(43);
  //h3->GetXaxis()->SetTitleOffset(4.);
  //h3->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  //h3->GetXaxis()->SetLabelSize(15);
  c1->Print((std::string(title)+".pdf").c_str());
}
void DrawTree(TTree *tree, std::string type){
  TCanvas *cDraw= new TCanvas("cDraw", "cDraw",900,900);
  cDraw->cd();
  
  std::string name="Mjj";
  name=name+"."+type;
  tree->Draw("Mjj");
  cDraw->Print(name.c_str());
  delete cDraw;
}

void GetBinByBinCFactors(TH1F * reco, TH1F * truth){
  double total_truth=0;
  double total_reco=0;
  double reco_content=0;
  double truth_content=0;
   for (Int_t i=0;i<truth->GetNbinsX();i++){
     reco_content=reco->GetBinContent(i);
     truth_content=truth->GetBinContent(i);
     std::cout<<reco_content/truth_content<<" \n";
     total_truth+=truth_content;
     total_reco+=reco_content;
  }
   std::cout<<total_reco/total_truth<<"\n";
}
map<string,TH1F*> GetHistograms(TFolder *folder){

  map<string,TH1F*> histMap;
  histMap["Mjj"]=static_cast<TH1F*>(folder->FindObjectAny("Mjj"));
  histMap["Mll"]=static_cast<TH1F*>(folder->FindObjectAny("Mll"));
  histMap["nJet"]=static_cast<TH1F*>(folder->FindObjectAny("nJet"));
  histMap["DPhill"]=static_cast<TH1F*>(folder->FindObjectAny("DPhill"));
  histMap["MET"]=static_cast<TH1F*>(folder->FindObjectAny("MET"));
  return histMap;
}


void loop_folder(TFolder * fold){
  TObject *obj;
  TIter next(fold->GetListOfFolders());
  while ((obj = (TObject *) next())) {
    TClass *cl = gROOT->GetClass(obj->ClassName());
    //printf(" found object:%s\n",obj->GetName());
    //std::cout<<"class name "<<obj->ClassName()<<"\n";
    if(cl->InheritsFrom("TH1")){
       printf("object:%s\n",obj->GetName());
      }
    else{
      std::cout<<"class name "<<obj->GetName()<<"\n";
      loop_folder((TFolder*)obj);
    }
  }
      
}

map<string,TH1F*> loop_file(TFile * histograms){
  TObject *obj;
  TKey *key;
  TIter next( histograms->GetListOfKeys());
  map<string,TH1F*> histmap;
  while ((key = (TKey *) next())) {
    TClass *cl = gROOT->GetClass(key->GetClassName());
    obj = histograms->Get(key->GetName()); // copy object to memory
    printf(" found object:%s\n",key->GetName());
    std::cout<<"class is folder "<<cl->IsFolder()<<"\n";
    std::cout<<"class is hist "<<cl->InheritsFrom("TH1")<<"\n";
    TFolder* cutFolder=(TFolder*)(static_cast<TFolder*>(obj)->FindObjectAny("CutVBF_2jet"));
    histmap=GetHistograms(cutFolder);
    return histmap;
  }
}


void draw_map(map<string,TH1F*> &histmap){
  for (auto & x : histmap)
    {
      std::cout<<x.first<<"\n";
      DrawHist<TH1F>(x.second);
    }
}

bool substring(std::string large_string,std::string sub_string){
  bool results=large_string.find(sub_string) != std::string::npos;
  return results;
    
}

std::string delete_string(const char * full,const char * sub_string){
  std::string large_string(full);
  std::string sub=std::string(sub_string);
  auto pos=large_string.find(sub);
  if(pos == std::string::npos){
    std::cout<<"substring not found\n";
    return "";
  }
  else{
    large_string.erase(pos,sub.length());
    return large_string;
  }
}
//divide by bin width
void density_histogram(TH1 *hist){
  double bin_content;
  double bin_width;
  double max=hist->GetXaxis()->GetXmax();
  for(int i=0 ;i <=hist->GetNbinsX();i++){
    bin_content=hist->GetBinContent(i);
    bin_width=hist->GetBinWidth(i);
    std::cout<<"bin width "<<bin_width<<" max "<<max<<"\n";
    hist->SetBinContent(i,bin_content/bin_width*1000);
    hist->SetBinError(i,TMath::Power(bin_content,0.5)/bin_width);
  }
  hist->SetMinimum(0);
}
void print_vector(std::vector<double> &vec){
  for(auto &i: vec){
    std::cout<<i<<"\n";
  }
}
void print_matrix(TMatrixD &mat){
  int size=mat.GetNrows();
  for(int i=0;i<size;i++){
    for(int j=0;j<size;j++){
      std::cout<<mat[i][j]<<" ";
    }
    std::cout<<"\n";
  }

}
std::vector<double> multivar_gaus(std::vector<double> &means,TMatrixDSym &cov){
  int size=means.size();
  TMatrixDSymEigen eigen_cov(cov);
  std::vector<double> initial_values(size);
  for(int i=0;i<size;i++)
    initial_values[i]=RNG->Gaus(0,1);

  TMatrixD eigen_matrix=eigen_cov.GetEigenVectors();
  TVectorD eigen_values=eigen_cov.GetEigenValues();
  TMatrixD half_eigen(size,size);
  for(int i=0;i<size;i++){
    for(int j=0;j<size;j++){
      if(i==j){
        if(eigen_values[i] < 0)
          std::cout<<"Warning: negative eigenvalues in covariance matrix \n";
        half_eigen[i][j]=pow(abs(eigen_values[i]),0.5);
      }
      else{
        half_eigen[i][j]=0;
      }
    }
  }

  //scaling matrix -multiply half eigen by eigen vectors
  TMatrixD scale=eigen_matrix*half_eigen;
  std::vector<double> scaled_values(size);

  //scale initial values
  for(int i=0;i<size;i++){
    double scaled_value=0;
    for(int j=0;j<size;j++){
      scaled_value+=initial_values[j]*scale[i][j];
    }
    scaled_values[i]=scaled_value;
  }

  //shift means
    for(int i=0;i<size;i++){
      scaled_values[i]+=means[i];
    }
    return scaled_values ;
}

double gaussian_kernel(double x_center,double x_rebined,double smoothing){
  double gaus=TMath::Exp(-1 * TMath::Power(x_center-x_rebined,2) / (2*TMath::Power(smoothing,2)));
  return gaus;

}

TH1F * rebin_hist(TH1F* syst,TH1F  * nominal){
  

}
//returns a smoothed histogram
TH1F* gaussian_smoothing(TH1F * syst_histogram){
  
  

}
