#ifndef CONFIG_H
#define CONFIG_H
#include  <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "unfold.h"

bool isNewSection(std::string line);
std::vector<std::string> split(std::string full_string,std::string delim);
void read_binning(std::string binning_file);

                                                                 
#endif
