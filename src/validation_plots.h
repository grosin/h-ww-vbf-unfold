#ifndef VALIDATION
#define VALIDATION
#include <algorithm>
#include <vector>
#include "TH1F.h"
#include "TH1.h"
#include "TFile.h"
#include "TNtuple.h"
#include "TVectorD.h"
#include <iostream>
#include "TCanvas.h"
#include "TLegend.h"
#include "TROOT.h"
#include <cstring>
#include "TMath.h"
#include <memory>
#include <fstream>
#include <TTree.h>
#include "TH2F.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"
#include "merge.h"
#include <unordered_map>
#include <TPaveLabel.h>
using namespace TMath;
using namespace std;

struct sr_obs_binning 
{
  int bins;
  double low;
  double high;
};

string get_truth_name(string reco_name);
struct sr_obs_binning get_binning(string obs);
  

void twoDHistogramsFiducial(std::string dist,TTree* truth,TTree * reco,string bdt_dist);
void draw_histograms(TTree * truth_tree,TTree * reco_tree,string truth_dist,string reco_dist, TH2F * double_diff,TH2F * fakes,string bdt_dist);
void bdt_cut_skew(TTree* truth_tree,TTree* reco_tree,string dist,string bdt_dist);
vector<TH1F*> draw_bdt_cuts(TTree *truth_tree,TTree* reco_tree,string dist,bool truth,string bdt_dist);
void plot_cfactor_ratio(TH1F * hist,string dist);
#endif

