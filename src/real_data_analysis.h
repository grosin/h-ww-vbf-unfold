#pragma once
#include <TTree.h>
#include <vector>
#include <string>
#include <TH1F.h>
#include <TFile.h>
#include <iostream>
#include <TPad.h>
#include <TGraph.h>
#include <fstream>
#include <TObject.h>
#include <TKey.h>
#include <TList.h>
#include <TROOT.h>
#include <TFolder.h>
#include <map>
#include <TNtuple.h>
#include <string>
using namespace std;

void draw_map(map<string,TH1F*> &histmap);
TH1F * LoopTree(TTree * data_tree,string feature,int nbins,double min, double max,string name="",double scale=1);

TH1F * LoopTree(TTree * data_tree,string feature,int nbins,double * binning,string name="",double scale=1, bool SelectRegion=false);
TTree* LoadTree(string tree_file,string tree_name);
TH1F * LoopTree(TTree * data_tree,string feature,TH1F * hist,string name);
void SaveHist(TH1F * hist, string name);
TH1F * LoadTruthData(string feature, int bins, double * binning);
