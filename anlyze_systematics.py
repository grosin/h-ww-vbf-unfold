import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
pd.options.display.max_rows = 200

systematics=pd.read_csv("Mjjreco_systematics_comparison.csv")
systematics['RecoImpact']=systematics["reco_up"]-systematics["reco_down"]
systematics.sort_values(by="RecoImpact",inplace=True,ascending=False)
print(systematics.head(5))

with open("reco_systematics_impact.txt", 'w') as fo:
    fo.write(systematics.to_string(index=False))
    
