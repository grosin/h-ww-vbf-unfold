import numpy as np

stat_file="matrix_projection_stat.txt"
new_file=open("matrix_projection_stat_uncert.txt","w")

with open("matrix_projection_stat.txt","r") as df:
    for line in df:
        print(line)
        if(line!="\n"):
            line_uncert=[]
            line_data=line.split(",")
            line_uncert.append(line_data[0])
            line_uncert.append(line_data[1])
            for s_val in line_data[2:]:
                val=float(s_val)
                if val==0:
                    line_uncert.append("0")
                else:
                    line_uncert.append(str(round(val**0.5 / val,4)))
            new_file.write(",".join(line_uncert))
            new_file.write("\n")
        else:
            new_file.write("\n")
            
new_file.close()
