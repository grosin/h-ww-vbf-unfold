import uproot
from fluctuation_profile import cycler
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from analyze_matrices import label_matrix
from syst_studies import *
from systematics_studies import systematics,bin_dict,variation_by_systematic,my_naming,create_histogram,nominal_uncertainty,stacked_plot
from systematics_studies import get_all_systematics
from binnings import systematics_set

sigma_dict={"top":0.25,"Zjets":1.0,"diboson":2.0}

def combine_hist(xerr,x_centers,y_data,index1,index2,uncert):
    if index2 != index1+1:
        raise ValueError ("second index must be consecutive to first index")
    new_uncert=np.sqrt(uncert[index1]**2+uncert[index2]**2)
    new_x_centers=((x_centers[index1]-xerr[index1]) + (x_centers[index2]+xerr[index2])) / 2
    new_xerr=xerr[index1]+xerr[index2]
    new_y_data=y_data[index1]+y_data[index2]

    new_xerr_arr=np.zeros(len(xerr)-1)
    new_x_centers_arr=np.zeros(len(xerr)-1)
    new_y_data_arr=np.zeros(len(xerr)-1)
    new_uncert_arr=np.zeros(len(xerr)-1)

    new_xerr_arr[:index1]=xerr[:index1]
    new_xerr_arr[index1]=new_xerr
    new_xerr_arr[index2:]=xerr[index2+1:]

    new_x_centers_arr[:index1]=x_centers[:index1]
    new_x_centers_arr[index1]=new_x_centers
    new_x_centers_arr[index2:]=x_centers[index2+1:]

    new_y_data_arr[:index1]=y_data[:index1]
    new_y_data_arr[index1]=new_y_data
    new_y_data_arr[index2:]=y_data[index2+1:]

    new_uncert_arr[:index1]=uncert[:index1]
    new_uncert_arr[index1]=new_uncert
    new_uncert_arr[index2:]=uncert[index2+1:]


    
    return (new_xerr_arr,new_x_centers_arr,new_y_data_arr,new_uncert_arr )


                           
def rebin_dist(xerr,x_values,y_syst,y_nominal,uncert,iterations=100,name=""):
    cycler.reset()
    uncert=uncert*y_nominal
    sigmas=np.abs(y_syst - y_nominal) / uncert
    smallest_sigma,index=np.min(sigmas),np.argmin(sigmas)
    i=0
    #color=cycler.get_color()        
    #plt.errorbar(x_values,y_nominal,xerr=xerr,yerr=uncert,fmt=",",color=color,label=f"nominal_{0}")
    #plt.errorbar(x_values,y_syst / y_nominal,xerr=xerr,yerr=uncert / y_nominal,fmt=",",label=f"syst_{0}")
    plt.title(f"{name} Rebinned 1 Sigma")
    plt.errorbar(x_values,np.ones(len(x_values)),xerr=xerr,yerr=uncert / y_nominal,fmt=",",label=f"nominal_rebin {i}")
    plt.errorbar(x_values,y_syst / y_nominal,xerr=xerr,fmt=",",label=f"syst_rebin {i}")
    plt.legend()
    plt.show()
    while smallest_sigma < 1.0 and len(sigmas) > 1 and i < iterations:
        index1=index
        if index==0:
            index1=index
        elif index==len(sigmas)-1:
            index1=index-1
        else:
            nearest_neighbors=(sigmas[index-1],sigmas[index+1])
            combined_bin=np.argmin(nearest_neighbors)
            index1=index-(1-combined_bin)
        xerr1,x_values1,y_syst,uncert1=combine_hist(xerr,x_values,y_syst,index1,index1+1,uncert)
        xerr,x_values,y_nominal,uncert=combine_hist(xerr,x_values,y_nominal,index1,index1+1,uncert)
        sigmas=np.abs(y_syst - y_nominal) / uncert
        smallest_sigma,index=np.min(sigmas),np.argmin(sigmas)
        #color=cycler.get_color()
        i+=1

    plt.title(f"{name} Rebinned 1 Sigma")
    plt.errorbar(x_values,np.ones(len(x_values)),xerr=xerr,yerr=uncert / y_nominal,fmt=",",label=f"nominal_rebin {i}")
    plt.errorbar(x_values,y_syst / y_nominal,xerr=xerr,fmt=",",label=f"syst_rebin {i}")
    plt.legend()
    plt.show()
    #plt.savefig(f"{name}_rebinned_1_sigma.pdf")
    #plt.cla()
    #plt.close()

    return xerr,x_values,y_syst,y_nominal

def gaussian_smooth(x_data,y_data,x_rebinned,y_rebinned,smoothing=1.0):
    smoothing=np.sum(x_data)/len(x_data) *smoothing
    kernel_data=np.zeros((len(x_data),len(x_data)))
    smoothed_data=np.zeros(len(x_data))
    for i,x_position in enumerate(x_data):
        print(f"x rebinned {x_rebinned} x position {x_position} smoothing {smoothing}")
        kernel=np.exp(-(x_rebinned - x_position) ** 2 / (2 * smoothing ** 2))
        print(f"Kernel = {kernel}")
        kernel=kernel/np.sum(kernel)
        smoothed_data[i]=np.sum(y_rebinned*kernel)
    return smoothed_data

def smooth_single_variation(xerr,x,variation,nominal,uncert,sigma=1.0):
    variation_ratio=variation / nominal
    xerr1,x1,syst1,nominal1=rebin_dist(xerr,x,variation,nominal,uncert)
    syst1_ratio=syst1 / nominal1
    smoothed_var=gaussian_smooth(x,variation_ratio,x,variation_ratio,smoothing=sigma)
    #plt.errorbar(x,syst1_ratio,xerr=xerr,fmt=",")
    return smoothed_var*nominal

def smooth_proc(observables,proc,sigma=1,combine=0):
    hist_data=variation_by_systematic(observables,systematics[0:2],[proc])
    uncert_dict=nominal_uncertainty(observables,proc)
    for obs_name,obs_data in hist_data.items():
        binning=np.array(bin_dict[my_naming[obs_name]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[1:]-binning[:-1])/2
        nominal=obs_data["nominal"][proc]
        uncert=uncert_dict[obs_name]
        for syst_name,syst_data in obs_data.items():
            if  syst_name=="nominal":
                continue
            up_syst=syst_data["up"][proc] 
            down_syst=syst_data["down"][proc]  
            xerr1,x1,up_syst1,nominal1=rebin_dist(xerr,x,up_syst,nominal,uncert=uncert,name=f"{obs_name}_up_{syst_name}_{proc}")
            xerr2,x2,down_syst1,nominal2=rebin_dist(xerr,x,down_syst,nominal,uncert=uncert,name=f"{obs_name}_down_{syst_name}_{proc}")
            '''
            plt.errorbar(x1,np.ones(len(x1)),xerr=xerr1,yerr=np.sqrt(nominal1)/nominal1,fmt="r,",label=f"up comb nominal")
            plt.errorbar(x1,up_syst1/nominal1,xerr=xerr1,fmt="b,",label=f"up comb {combine} bins")
            plt.title(f"{obs_name} {syst_name} {proc} combined {combine}")

            plt.legend()
            plt.savefig(f"{obs_name}_{syst_name}_{proc}_combined_{combine}_up.pdf")
            plt.cla()
            plt.close()
            
            plt.errorbar(x2,np.ones(len(x2)),xerr=xerr2,yerr=np.sqrt(nominal2)/nominal2,fmt="r,",label="down comb nominal")
            plt.errorbar(x2,down_syst1/nominal2,xerr=xerr2,fmt="b,",label=f"down comb {combine} bins")
            plt.title(f"{obs_name} {syst_name} {proc} combined {combine}")

            plt.legend()
            plt.savefig(f"{obs_name}_{syst_name}_{proc}_combined_{combine}_down.pdf")
            plt.cla()
            plt.close()
            '''
            up_syst=up_syst/nominal
            down_syst=down_syst/nominal
            up_syst1=up_syst1/nominal1
            down_syst1=down_syst1/nominal2
            
            up_smooth1=gaussian_smooth(x,up_syst,x1,up_syst1,0.5)
            down_smooth1=gaussian_smooth(x,down_syst,x2,down_syst1,0.5)

            up_smooth2=gaussian_smooth(x,up_syst,x1,up_syst1,0.75)
            down_smooth2=gaussian_smooth(x,down_syst,x2,down_syst1,0.75)

            up_smooth3=gaussian_smooth(x,up_syst,x1,up_syst1,1.0)
            down_smooth3=gaussian_smooth(x,down_syst,x2,down_syst1,1.0)


            up_smooth4=gaussian_smooth(x,up_syst,x1,up_syst1,1.25)
            down_smooth4=gaussian_smooth(x,down_syst,x2,down_syst1,1.25)

            up_smooth5=gaussian_smooth(x,up_syst,x1,up_syst1,1.5)
            down_smooth5=gaussian_smooth(x,down_syst,x2,down_syst1,1.5)
            
            plt.errorbar(x,up_syst,xerr=xerr,yerr=uncert,fmt="b,",label="up syst")
            plt.plot(x,up_smooth1,"b:",label="smoothing=0.5")
            plt.plot(x,up_smooth3,"b-",label="smoothing=1.0")
            plt.plot(x,up_smooth5,"b--",label="smoothing=1.5")
            plt.legend()
            plt.ylabel("Systematic / nominal")
            plt.title(f"{obs_name} {syst_name} {proc} Up Variation")
            plt.savefig(f"smoothed_{obs_name}_{syst_name}_{proc}_combined_{combine}_up.pdf")
            plt.cla()
            plt.close()
            
            plt.errorbar(x,down_syst,xerr=xerr,yerr=uncert,fmt="r,",label="down syst")
            plt.plot(x,down_smooth1,"r:",label="smoothing=0.5")
            plt.plot(x,down_smooth3,"r-",label="smoothing=1.0")
            plt.plot(x,down_smooth5,"r--",label="smoothing=1.5")
            plt.legend()
            plt.ylabel("Systematic / nominal")
            plt.title(f"{obs_name} {syst_name} {proc} Down Variation")
            plt.savefig(f"smoothed_{obs_name}_{syst_name}_{proc}_combined_{combine}_down.pdf")
            plt.cla()
            plt.close()
            #plt.savefig(f"{obs_name}_{syst_name}_{proc}_smoothing_{sigma}.pdf")
            #plt.cla()
            #plt.close()
            

def read_sigmas():
    set1,set2=systematics_set()
    hist_data=variation_by_systematic(["DYjj","Mjj","Mll","DYll"],set1,["Zjets"])
    sigma_dict={}
    for obs_name,obs_data in hist_data.items():
        binning=np.array(bin_dict[my_naming[obs_name]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[1:]-binning[:-1])/2
        zjets_nominal=obs_data["nominal"]["Zjets"]
        uncert=np.sqrt(zjets_nominal)
        for syst_name,syst_data in obs_data.items():
            if  syst_name=="nominal":
                continue
            zjets_up=syst_data["up"]["Zjets"]
            zjets_down=syst_data["down"]["Zjets"]
            sigmas=np.abs(zjets_up - zjets_nominal) / uncert
            sigma_dict[syst_name]=np.sum(sigmas)

def loop_and_smooth_systematic(observables,systematics,backgrounds,title=""):
    #smooth_single_variation(xerr,x,variation,nominal)
    nominal_data={obs:{} for obs in observables}
    nominal_histograms_data=create_histogram(observables,"nominal",processes=backgrounds)["no_cuts"]
    for obs in observables:
        nominal_data[obs]={label:back for label,back in nominal_histograms_data[obs]["background_dict"].items()}
        nominal_data[obs]["signal"]=nominal_histograms_data[obs]["signal"]
    syst_histogram={obs:{sys:{} for sys in systematics} for obs in observables}
    for syst in systematics:
        syst_data_histograms_up=create_histogram(observables,syst+"__1up",processes=backgrounds)["no_cuts"]
        syst_data_histograms_down=create_histogram(observables,syst+"__1down",processes=backgrounds)["no_cuts"]

        for obs in observables:
            syst_histogram[obs][syst]['up']={label:back for label,back in syst_data_histograms_up[obs]["background_dict"].items()}
            syst_histogram[obs][syst]['up']["signal"]=syst_data_histograms_up[obs]["signal"]
            syst_histogram[obs][syst]['down']={label:back for label,back in syst_data_histograms_down[obs]["background_dict"].items()}
            syst_histogram[obs][syst]['down']["signal"]=syst_data_histograms_down[obs]["signal"]

    all_samples=backgrounds+["vbf"]
    uncertainties={process:nominal_uncertainty(observables,process) for process in all_samples}
    for obs in observables:
        nominal_full_data=np.zeros(len(nominal_data[obs]["signal"]))
        nominal_full_data+=nominal_data[obs]["signal"]
        print("nominal hist")
        print(nominal_full_data)
        background_list=[]
        for back_name,back_hist in nominal_data[obs].items():
            if back_name=="signal":
                continue
            nominal_full_data+=back_hist
            background_list.append(back_hist)
        binning=np.array(bin_dict[my_naming[obs]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[1:]-binning[:-1]) /2
        stacked_plot(binning,nominal_data[obs]["signal"],backgrounds=background_list,labels=backgrounds,title="Nominal Data")
        total_up_systematics=np.zeros(len(x))
        total_down_systematics=np.zeros(len(x))

        total_up_systematics_smoothed=np.zeros(len(x))
        total_down_systematics_smoothed=np.zeros(len(x))


        for sys in systematics:
            estimated_background_up=np.zeros(len(x))
            estimated_background_down=np.zeros(len(x))

            estimated_background_up_smoothed=np.zeros(len(x))
            estimated_background_down_smoothed=np.zeros(len(x))
            for back in backgrounds:
                nominal=nominal_data[obs][back]
                syst_up=syst_histogram[obs][sys]["up"][back]
                syst_down=syst_histogram[obs][sys]["down"][back]
                sigma=sigma_dict[back]
                smoothed_up=smooth_single_variation(xerr,x,syst_up,nominal,uncertainties[back][obs],sigma=sigma)
                smoothed_down=smooth_single_variation(xerr,x,syst_down,nominal,uncertainties[back][obs],sigma=sigma)
                estimated_background_up_smoothed+=smoothed_up
                estimated_background_down_smoothed+=smoothed_down
                estimated_background_up+=syst_up
                estimated_background_down+=syst_down
                print(back)
                print(estimated_background_up)
                
                plt.errorbar(x,nominal,xerr=xerr,fmt="b,",label="nominal")
                plt.errorbar(x,syst_up,xerr=xerr,fmt="r,",label="up")
                plt.errorbar(x,syst_down,xerr=xerr,fmt="g,",label="down")
                plt.plot(x,smoothed_up,"r")
                plt.plot(x,smoothed_down,"g")
                plt.legend()
                plt.title(f"{sys} {back}")
                plt.savefig(f"{obs}_{sys}_{back}_smoothed.pdf")
                plt.cla()
                plt.close()
            
            
            plt.errorbar(x,estimated_background_down,xerr=xerr,fmt=",",label="total down varation")
            plt.errorbar(x,estimated_background_up,xerr=xerr,fmt=",",label="total up varation")
            plt.errorbar(x,estimated_background_up_smoothed,xerr=xerr,fmt=",",label="total up varation smooth")
            plt.errorbar(x,estimated_background_down_smoothed,xerr=xerr,fmt=",",label="total down varation smooth")

            plt.legend()
            plt.title(f"{obs} {sys} Total Background")
            plt.savefig(f"estimated_{sys}_{obs}_total_background.pdf")
            plt.cla()
            plt.close()

            estimated_signal_up_smooth=nominal_full_data-estimated_background_down_smoothed
            estimated_signal_down_smooth=nominal_full_data-estimated_background_up_smoothed

            estimated_signal_up=nominal_full_data-estimated_background_down
            estimated_signal_down=nominal_full_data-estimated_background_up
            plt.errorbar(x,nominal_data[obs]["signal"],xerr=xerr,yerr=uncertainties["vbf"][obs]*nominal_data[obs]["signal"],capsize=3,fmt="b,",label="nominal")
            plt.errorbar(x,estimated_signal_up,xerr=xerr,fmt="r,",label="estimated up")
            plt.errorbar(x,estimated_signal_down,xerr=xerr,fmt="g,",label="estimated down")
            plt.errorbar(x,estimated_signal_up_smooth,xerr=xerr,fmt="c,",label="estimated up smooth")
            plt.errorbar(x,estimated_signal_down_smooth,xerr=xerr,fmt="m,",label="estimated down smooth")

            plt.legend()
            plt.title(f"{obs} {sys} {backgrounds[0]}")
            plt.savefig(f"estimated_{sys}_{obs}_{title}_smoothed.pdf")
            plt.cla()
            plt.close()
            up_syst_band=np.maximum(estimated_signal_up-nominal_data[obs]["signal"],estimated_signal_down-nominal_data[obs]["signal"])
            down_syst_band=np.minimum(estimated_signal_up-nominal_data[obs]["signal"],estimated_signal_down-nominal_data[obs]["signal"])
            up_syst_band[up_syst_band<0]=0
            down_syst_band[down_syst_band>0]=0
            

            up_syst_band_smooth=np.maximum(estimated_signal_up_smooth-nominal_data[obs]["signal"],estimated_signal_down_smooth-nominal_data[obs]["signal"])
            down_syst_band_smooth=np.minimum(estimated_signal_up_smooth-nominal_data[obs]["signal"],estimated_signal_down_smooth-nominal_data[obs]["signal"])
            up_syst_band_smooth[up_syst_band_smooth<0]=0
            down_syst_band_smooth[down_syst_band_smooth>0]=0
            
            total_up_systematics+=up_syst_band**2
            total_down_systematics+=down_syst_band**2

            total_up_systematics_smoothed+=up_syst_band_smooth**2
            total_down_systematics_smoothed+=down_syst_band_smooth**2

        total_up_systematics=np.sqrt(total_up_systematics)
        total_down_systematics=np.sqrt(total_down_systematics)

        total_up_systematics_smoothed=np.sqrt(total_up_systematics_smoothed)
        total_down_systematics_smoothed=np.sqrt(total_down_systematics_smoothed)

        up_fill=np.zeros(2*len(x))
        down_fill=np.zeros(2*len(x))
        up_fill[::2]=total_up_systematics
        up_fill[1::2]=total_up_systematics
        down_fill[::2]=total_down_systematics
        down_fill[1::2]=total_down_systematics
        nominal_fill=np.zeros(2*len(x))
        nominal_fill[::2]=nominal_data[obs]["signal"]
        nominal_fill[1::2]=nominal_data[obs]["signal"]
        
        x_fill=np.zeros(len(x)*2)
        x_fill[::2]=binning[:-1]
        x_fill[1::2]=binning[1:]
        baseline=np.ones(len(x))
        plt.errorbar(x,nominal_data[obs]["signal"],xerr=xerr,yerr=np.sqrt(nominal_data[obs]["signal"]),capsize=3,fmt="b,",label="nominal")
        plt.fill_between(x_fill,nominal_fill-down_fill,nominal_fill+up_fill,facecolor='m',alpha=1.0,linewidth=0,edgecolor="k",linestyle='--',label="total Syst not smoothed")
        #plt.legend()
        #plt.title(f"{obs} Total Uncertainty not smoothed")
        #plt.savefig(f"{obs}_total_systematic_uncertainty_not_smoothed.pdf")
        #plt.cla()
        #plt.close()


        up_fill_smooth=np.zeros(2*len(x))
        down_fill_smooth=np.zeros(2*len(x))
        up_fill_smooth[::2]=total_up_systematics_smoothed
        up_fill_smooth[1::2]=total_up_systematics_smoothed
        down_fill_smooth[::2]=total_down_systematics_smoothed
        down_fill_smooth[1::2]=total_down_systematics_smoothed
        #plt.errorbar(x,nominal_data[obs]["signal"],xerr=xerr,yerr=uncertainties["vbf"][obs]*nominal_data[obs]["signal"],capsize=3,fmt="b,",label="nominal")
        plt.fill_between(x_fill,nominal_fill-down_fill_smooth,nominal_fill+up_fill_smooth,facecolor='c',alpha=0.5,linewidth=0,edgecolor="k",linestyle='--',label="total Syst smoothed")
        plt.legend()
        plt.title(f"{obs} Total Uncertainty")
        plt.savefig(f"{obs}_total_systematic_uncertainty_smoothed_{title}.pdf")
        plt.cla()
        plt.close()

if __name__=="__main__":
    #smooth_proc(["DYjj"],"Zjets",0.5,0)
    #smooth_proc(["DYjj"],"top",0.5,0)
    #smooth_proc(["DYjj"],"diboson",0.5,0)
    all_up,all_down=systematics_set()
    all_systs=all_up | all_down
    all_systs=list(all_systs)
    loop_and_smooth_systematic(["DYjj"],systematics,["Zjets","diboson","top"])

    '''
    for proc in ["Zjets","top","diboson"]:
        for sig in [0.25,0.5,0.75,1.0,1.25,1.5,1.75]:
            for comb in [0,1,2,3,4,5]:
                smooth_proc(proc,sig,comb)
    '''
    '''
    for proc in ["Zjets","top"]:
        for com in [1,2,3,4,5,None]:
            smooth_proc(proc,1.0,com)                                            
    '''
