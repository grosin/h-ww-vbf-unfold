import re

observable_dict={}
with open("observable_cfactors.txt") as cfactor_file:
    current_obs=''
    for line in cfactor_file:
        if line[0] not in ('c','-'):
            current_obs=line.strip()
            observable_dict[current_obs]={"binning":[],"cfactors":[],"oneOver":[],"diffXsec":[]}
        else:
            line_data=re.findall(r"\d+\.?\d+",line)
            line_data=[float(i) for i in line_data]
            if line_data==[]:
                continue
            
            if observable_dict[current_obs]["binning"]==[]:
                observable_dict[current_obs]["binning"].append(line_data[0])
                observable_dict[current_obs]["binning"].append(line_data[1])
                observable_dict[current_obs]["cfactors"].append(line_data[2])
                observable_dict[current_obs]["oneOver"].append(line_data[3])
                observable_dict[current_obs]["diffXsec"].append(line_data[4])
            else:
                observable_dict[current_obs]["binning"].append(line_data[1])
                observable_dict[current_obs]["cfactors"].append(line_data[2])
                observable_dict[current_obs]["oneOver"].append(line_data[3])
                observable_dict[current_obs]["diffXsec"].append(line_data[4])

print(observable_dict)
