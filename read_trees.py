import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd




pd.options.display.max_rows = 100
cuts=["njets","eta","leppt","mll","met","lepjetsep","jetpt","drll","cjv","olv"]
#pd.options.display.max_columns = 10
list1=["r_DPhijj","t_DPhijj",
       "r_DEtajj","t_DEtajj",
       "r_DPhill","t_DPhill",
       "r_Ptll","t_Ptll",
       "r_DRl0j0","t_DRl0j0",
       "eventNumber",
       "truthMatch","recoMatch"]

list4=["r_MT","t_MT",
       "r_MET","t_MET",
       "r_higgs_pt","t_higgs_pt",
       "r_Mjj","t_Mjj",
       "r_Mll","t_Mll",
       "eventNumber",
       "truthMatch","recoMatch"]

list2=["r_lep_pt_0","t_lep_pt_0",
       "r_lep_eta_0","t_lep_eta_0",
       "r_lep_phi_0","t_lep_phi_0",
       "r_jet_pt_0","t_jet_pt_0",
       "r_jet_eta_0","t_jet_eta_0",
       "r_jet_phi_0","t_jet_phi_0",
       "eventNumber",
       "truthMatch","recoMatch"]

list3=["r_lep_pt_1","t_lep_pt_1",
       "r_lep_eta_1","t_lep_eta_1",
       "r_lep_phi_1","t_lep_phi_1",
       "r_jet_pt_1","t_jet_pt_1",
       "r_jet_eta_1","t_jet_eta_1",
       "r_jet_phi_1","t_jet_phi_1",
       "eventNumber",
       "truthMatch","recoMatch"]

def get_tree(cut,col_list):
    merged_tree = uproot.open(f"ntupls/merged/{cut}_merged__total.root")["reco_truth_merge"]
    panda_frame=merged_tree.pandas.df(col_list)
    panda_frame = panda_frame.sample(frac=1).reset_index(drop=True)
    return panda_frame

def print_frame(panda_frame,frame_file):
    panda_frame=panda_frame.drop("eventNumber",axis=1)
    print(panda_frame.head(5))
    with open(frame_file, 'w') as fo:
        fo.write(panda_frame.head(100).__repr__())



def fakes_per_cut():
    fakes=[]
    for cut in cuts:
        df=get_tree(cut)
        fake_df=df[np.logical_and(df["truthMatch"]==0,df["recoMatch"]==1)]
        fakes.append(len(fake_df)/len(df))
    x=[i for i in range(len(fakes))]
    plt.plot(x,fakes,".--")
    plt.xticks(x,cuts)
    plt.title("fakes per cut")
    plt.show()
    
def correlate_cuts(cut1,cut2,col_list,i):
    df1=get_tree(cut1,col_list)
    df2=get_tree(cut2,col_list)
    df1=df1[~df1.eventNumber.isin(df2.eventNumber)].dropna()
    print_frame(df1.head(100),f"passed_{cut1}_failed_{cut2}_{i}.txt")
    print(len(df1))

def write_tree():
    data=pd.read_csv("Yll_variations.csv")
    cols=data.columns[2:]
    with uproot.recreate("yll_ntuple.root") as f:
        f["Yll_variations"] = uproot.newtree({col: np.int32 for col in cols})
        f["Yll_variations"].extend({col: np.array(data[col].values,dtype=np.int32) for col in cols})

write_tree()

                                    
'''
    
col_list=[list1,list2,list3,list4]
j=0
for ls in col_list:
    for i in range(1,len(cuts)):
        correlate_cuts(cuts[i-1],cuts[i],ls,j)
    j+=1

'''
                                             
