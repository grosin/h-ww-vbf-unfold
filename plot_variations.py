import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import configparser
try:
    import uproot as root
except:
    import ROOT as root
#plt.style.use('ggplot')

def file_to_df(file_name):
    data=pd.read_csv(file_name,header=None)
    cols=len(data.columns)
    col_names=["dist","iterations"]
    for i in range(1,cols-3,2):
        col_names.append("Bin_"+str(math.trunc((i+1)/2))+"_var")
        col_names.append("Bin_"+str(math.trunc((i+1)/2))+"_content")
    col_names.append("chi2")
    data.columns=col_names
    data=data.fillna(0)
    print(data.head(5))
    return data
def load_matrices(matrix_file):
    matrices={}
    with open(matrix_file) as fl:
        distribution=""
        for line in fl:
            if line.split(":")[0]=="variable":
                distribution=line.split(":")[1].rstrip()
                matrices[distribution]=[]
            else:
                try:
                    print(line.rstrip().split(",")[:-1])
                    line_data=[float(i) for i in line.rstrip().split(",")[:-1]]
                    if line_data:
                        matrices[distribution].append(line_data)
                except ValueError:
                    matrices[distribution]=np.array(matrices[distribution])
    return matrices

def loop_variations(iters,bin_num,data):
    selected_data=data[data["iterations"]==iters]
    bin_content=np.mean(selected_data["Bin_"+str(bin_num)+"_content"])
    bin_content+=0.0001
    bin_vars=selected_data["Bin_"+str(bin_num)+"_var"]
    std=np.std(bin_vars)
    mean=np.mean(bin_vars)
    return (bin_content,mean,std)

# create per bin gaussian histogram
def variations_hist(iters,bin_num,data):
    selected_data=data[data["iterations"]==iters]
    bin_content=np.mean(selected_data["Bin_"+str(bin_num)+"_content"])
    bin_content+=0.0001
    bin_vars=selected_data["Bin_"+str(bin_num)+"_var"]
    plt.hist(bin_vars)
    plt.title("Histogram of toys for bin "+str(bin_num))
    plt.xlabel("Unfolded - Truth Value")
    plt.show()

    
def loop_means(iters,bin_num,data):
    selected_data=data[data["iterations"]==iters]
    bin_content=np.mean(selected_data["Bin_"+str(bin_num)+"_content"])
    bin_content+=0.0001
    bin_vars=selected_data["Bin_"+str(bin_num)+"_var"]
    bin_vars=bin_vars
    mean=np.mean(bin_vars)
    return mean

def mean_data(data):
    bins=int((len(data.columns)-3)/2)
    iters=5
    means=np.zeros((bins,iters))
    for i in range(bins):
        for j in range(iters):
            means[i,j]=loop_means(j+1,i+1,data)

    return means

def uncertainty_vs_iteration(data,title_s=""):
    bins=int((len(data.columns)-3)/2)
    iters=5
    error_data=np.zeros((bins,iters))
    

    for i in range(bins):
        for j in range(iters):
            error_data[i,j]=loop_variations(j+1,i+1,data)


    x=np.linspace(1,iters,iters-1)
    plt.xlabel("Iteration")
    plt.ylabel("Statistical Variation / Bin Content")
    plt.title(str(title_s)+" Statistical Varation")
    for i in range(bins):
        plt.plot(x,error_data[i,1:],".-",label="Bin "+str(i+1))
    plt.legend()
    plt.show()
    return error_data

def uncertainty_vs_bin(data,title_s=""):
    bins=int((len(data.columns)-3)/2)
    iters=5
    error_data=np.zeros((bins,iters))
    

    for i in range(bins):
        for j in range(iters):
            error_data[i,j]=loop_variations(j+1,i+1,data)


    x=np.linspace(1,bins-1,bins-2)
    plt.xlabel("Bins Number")
    plt.ylabel("Statistical Variation / Bin Content")
    plt.title(str(title_s)+" Statistical Varation")
    for i in range(iters):
        plt.plot(x,error_data[2:,i],".-",label="Iteration "+str(i+1))
    plt.legend()
    plt.show()
    #plt.close()
    return error_data

def plot_hist_with_error(hist_file,error_data,means_data):
    root_file=root.open(hist_file)
    hists=dict(root_file.classes())
    for key,value in hists.items():
        class_name=root_file[key].__class__.__name__
        if  class_name=='TH1F':
            edges=root_file[key].edges
            label=''
            #print(root_file[key].edges)
            #print(root_file[key].values)
            print(root_file[key].name.decode())
            bin_centers = 0.5 * (edges[:-1] + edges[1:])
            if 'r' in root_file[key].name.decode():
                label='reco'
                plt.bar(bin_centers, root_file[key].values,width=(bin_centers[-1]-bin_centers[0])/len(bin_centers),label=label,zorder=10)
            else:
                label='truth'
                unfolded_values=means_data[:,3]+root_file[key].values
                plt.errorbar(bin_centers, unfolded_values,yerr=error_data[:,3],fmt=".",label="unfolded")
                plt.bar(bin_centers, root_file[key].values,width=(bin_centers[-1]-bin_centers[0])/len(bin_centers),label=label,zorder=1)
    plt.legend()
    plt.show()

def plot_matrices():
    matrices=load_matrices("response_matrices.txt")
    for var,matrix in matrices.items():
        plt.imshow(matrix)
        plt.title(var)
        plt.xlabel("reco")
        plt.ylabel("truth")
        plt.colorbar()
        plt.savefig(str(var)+"_response_matrix.pdf")
        plt.cla()
        plt.clf()
        plt.close()

#bias as a function of bin content

def stat_uncertainty():
    parser=configparser.ConfigParser()
    parser.read("binning_config.ini")
    sections=parser.sections()
    for section in sections:
        data_file=f"t_{section}_sig_1.0statistics_data.txt"
        x=[float(i) for i in parser[section]['binning'].split(",")]

        #for fil,name in zip(files,names):
        try:
            data=file_to_df(data_file)
        except Exception as e:
            print(e)
            print(f"skipping {section}")
            continue
        
        print(data.head(5))
        #variations_hist(3,5,data)
        col_nums=int((len(data.columns)-3)/2)
        iteration_nums=5
        print(col_nums)
        print(x)
        error_values=np.zeros((iteration_nums,col_nums-1))
        for iteration in range(iteration_nums):
            max_event=0
            for col in range(1,col_nums):
                truth,mean,std=loop_variations(iteration,col+1,data)
                error_values[iteration,col-1]=std
                if(col==1):
                    plt.errorbar((x[col-1]+x[col])/2000,truth,xerr=(x[col-1]-x[col])/2000,fmt="b.",label="Truth")
                    plt.errorbar((x[col-1]+x[col])/2000,mean+truth,xerr=(x[col-1]-x[col])/2000,yerr=std,capsize=3,fmt="r.",label="Unfolded")
                    if(max_event<truth+std):
                        max_event=truth+std
                    plt.ylim(0,max_event)
                else:
                    plt.errorbar((x[col-1]+x[col])/2000,truth,xerr=(x[col-1]-x[col])/2000,fmt="b.")
                    plt.errorbar((x[col-1]+x[col])/2000,mean+truth,xerr=(x[col-1]-x[col])/2000,yerr=std,capsize=3,fmt="r.")
                    if(max_event<truth+std):
                        max_event=truth+std
                    plt.ylim(0,max_event)

            plt.xlabel("GeV")
            plt.ylabel("Events")
            plt.title(f"{section} Unfolding Statistical Error {iteration} Iterations")
            plt.legend()    
            plt.savefig(f"{section}_{iteration}_unfolding_uncertainty.pdf")
            plt.cla()
            plt.close()

        for col in range(col_nums-1):
            truth,mean,std=loop_variations(3,col+2,data)
            plt.plot(np.arange(len(error_values[:,col])),error_values[:,col]/truth,"r--.")
            plt.xlabel("Iteration")
            plt.ylabel("Statistical Error / Bin Content")
            plt.title(f"{section} Statistical Uncertainty vs. Iteration Bin {col+1}")
            plt.savefig(f"{section}_{col}_statistical_uncertainty.pdf")
            plt.cla()
            plt.close()

if __name__=="__main__":
    file_name="t_Mjj_sig_1.0statistics_data.txt"
    df=file_to_df(file_name)
    for i in range(1,8):
        plt.hist(df[f"Bin_{i}_var"],bins=30)
        plt.show()
