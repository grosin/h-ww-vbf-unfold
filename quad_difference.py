import numpy as np
import re

def read_cross_sections(xsec_file_name):
    observables={}
    current=""
    with open(xsec_file_name) as xsec_file:
        for line in xsec_file:
            if line[:3]=="bin":
                numbers=re.findall(r"\d+\.\d+",line)
                observables[current].append([float(num) for num in numbers])
            else:
                current=line.rstrip()
                observables[current]=[]
    return observables


def calculate_diff(constant,floating):
    for bin_num in range(len(constant)):
        xsec=constant[bin_num][0]
        const_up=constant[bin_num][1]
        const_down=constant[bin_num][2]
        float_up=floating[bin_num][1]
        float_down=floating[bin_num][2]
        #print(f"float up {float_up} const up {const_up} float down {float_down} const down {const_down}")
        print(f"+{round(np.sqrt(float_up**2-const_up**2)/xsec,3)} - {round(np.sqrt(float_down**2-const_down**2)/xsec,3)}")
        
bbb_const=read_cross_sections("gamma_comparisons/constant_gamma/bbb/cross_sections.txt")
bbb_const_float=read_cross_sections("gamma_comparisons/const_vbf_float_back/bbb/cross_sections.txt")
bbb_float=read_cross_sections("gamma_comparisons/floating/bbb/cross_sections.txt")
bbb_float_const=read_cross_sections("gamma_comparisons/float_vbf_const_back/bbb/cross_sections.txt")

mat_const=read_cross_sections("gamma_comparisons/constant_gamma/mat/cross_sections.txt")
mat_const_float=read_cross_sections("gamma_comparisons/const_vbf_float_back/mat/cross_sections.txt")
mat_float=read_cross_sections("gamma_comparisons/floating/mat/cross_sections.txt")
mat_float_const=read_cross_sections("gamma_comparisons/float_vbf_const_back/mat/cross_sections.txt")

print(bbb_const)
for key in bbb_const:
    print(key)
    print("BBB Uncertainty Difference")
    calculate_diff(bbb_const[key],bbb_float[key])
    print("Matrix Uncertainty Difference")
    calculate_diff(mat_const[key],mat_float[key])
print("***********************************************")
for key in bbb_const:
    print(key)
    print("BBB Uncertainty Difference")
    calculate_diff(bbb_const[key],bbb_float_const[key])
    print("Matrix Uncertainty Difference")
    calculate_diff(mat_const[key],mat_float_const[key])
