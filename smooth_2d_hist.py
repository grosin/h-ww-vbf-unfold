import numpy as np
import matplotlib.pyplot as plt
from matplotlib import image
from analyze_matrices import label_matrix,label_matrix_axis

class combinable_2dhist:
    def __init__(self,x_edges,y_edges,z_values):
        self.x_edges=x_edges
        self.y_edges=y_edges
        self.x_grid,self.y_grid=np.meshgrid(x_edges,y_edges)
        self.x_centers=(self.x_grid[1:,:-1]+self.x_grid[1:,1:])/2
        self.y_centers=(self.y_grid[:-1,1:]+self.y_grid[1:,1:])/2
        self.z_values=z_values
        self.bin_map=np.arange(0,(len(x_edges)-1)*(len(y_edges)-1),1).reshape(len(y_edges)-1,len(x_edges)-1)


    #index1 bin absorbs index2
    def combine_bins(self,index1,index2):
        index1_map=np.where(self.bin_map==index1)
        index2_map=np.where(self.bin_map==index2)
        self.z_values[index1_map]+=self.z_values[index2_map][0]
        self.z_values[index2_map]=self.z_values[index1_map][0]
        new_x_center=(self.x_centers[index1_map][0]*len(index1_map)+self.x_centers[index2_map][0]*len(index2_map))/(len(index1_map)+len(index2_map))
        new_y_center=(self.y_centers[index1_map][0]*len(index1_map)+self.y_centers[index2_map][0]*len(index2_map))/(len(index1_map)+len(index2_map))
        self.x_centers[index1_map]=new_x_center
        self.x_centers[index2_map]=new_x_center

        self.y_centers[index1_map]=new_y_center
        self.y_centers[index2_map]=new_y_center

        self.bin_map[index2_map]=index1

    def get_bin(self,index):
        return self.bin_map[index]
    
    def __repr__(self):
        return str(self.bin_map)+ "\n" +str(self.z_values)
    def number_of_bins(self):
        return len(np.unique(self.bin_map))
    
    def get_neighbors(self,bin_index):
        index_map=np.where(self.bin_map==bin_index)
        neighbor_array=[[],[]]
        for x_ind,y_ind in zip(index_map[0],index_map[1]):
            if x_ind % self.bin_map.shape[0] !=0:
                if self.bin_map[x_ind-1,y_ind]!=self.bin_map[x_ind,y_ind]:
                    neighbor_array[0].append(x_ind-1)
                    neighbor_array[1].append(y_ind)
                    
            if x_ind % self.bin_map.shape[0] !=self.bin_map.shape[0]-1:
                if self.bin_map[x_ind+1,y_ind]!=self.bin_map[x_ind,y_ind]:
                    neighbor_array[0].append(x_ind+1)
                    neighbor_array[1].append(y_ind)
                    
            if y_ind % self.bin_map.shape[1] !=0:
                if self.bin_map[x_ind,y_ind-1]!=self.bin_map[x_ind,y_ind]:
                    neighbor_array[0].append(x_ind)
                    neighbor_array[1].append(y_ind-1)
                    
            if y_ind % self.bin_map.shape[1] !=self.bin_map.shape[1]-1:
                if self.bin_map[x_ind,y_ind+1]!=self.bin_map[x_ind,y_ind]:
                    neighbor_array[0].append(x_ind)
                    neighbor_array[1].append(y_ind+1)

        return tuple(neighbor_array)
    def display_hist(self):
        plt.xticks(np.linspace(0,len(self.x_edges),len(self.x_edges)+1),np.round(self.x_edges,1))
        plt.yticks(np.linspace(0,len(self.y_edges),len(self.y_edges)+1),np.round(self.y_edges,1))
        plt.imshow(self.z_values)
        plt.show()

        
def rebin_hists_2d(syst : combinable_2dhist,nominal:combinable_2dhist):
    uncert=np.sqrt(nominal.z_values)
    uncert+=0.5
    sigmas=np.abs(syst.z_values - nominal.z_values) / uncert
    smallest_sigma,index=np.min(sigmas),np.unravel_index(np.argmin(sigmas),sigmas.shape)
    f,(x1,x2)=plt.subplots(1,2)
    x1.imshow(nominal.bin_map,cmap="prism")
    x2.imshow(sigmas)
    label_matrix_axis(sigmas,x2)
    label_matrix_axis(nominal.bin_map,x1)
    x1.set_title("Initial Bin Map")
    x2.set_title("Inital Sigma")
    plt.show()
    while smallest_sigma < 2 and nominal.number_of_bins() > 1 :
        uncert=np.sqrt(nominal.z_values)
        uncert+=0.5
        sigmas=np.abs(syst.z_values - nominal.z_values) / uncert
        smallest_sigma,index=np.min(sigmas),np.unravel_index(np.argmin(sigmas),sigmas.shape)
        bin_index=nominal.get_bin(index)
        bin_neighbors=nominal.get_neighbors(bin_index)
        neighbor_dict={sigma:index for sigma,index in zip(sigmas[bin_neighbors],zip(bin_neighbors[0],bin_neighbors[1]))}
        smallest_sigma_neighbor=np.min(sigmas[bin_neighbors])
        neighbor_index=nominal.get_bin(neighbor_dict[smallest_sigma_neighbor])
        nominal.combine_bins(bin_index,neighbor_index)
        syst.combine_bins(bin_index,neighbor_index)
        uncert=np.sqrt(nominal.z_values)
        sigmas=np.abs(syst.z_values - nominal.z_values) / uncert
        smallest_sigma,index=np.min(sigmas),np.unravel_index(np.argmin(sigmas),sigmas.shape)
        
        #f,(x1,x2)=plt.subplots(1,2)
        #x1.imshow(nominal.bin_map,cmap="prism")
        #x2.imshow(sigmas)
        #x2.set_title(f"Sigmas ,combined {bin_index} and {neighbor_index}")
        #x1.set_title(f"Bin Map ,combined {bin_index} and {neighbor_index}")
        #label_matrix_axis(sigmas,x2,dec=1)
        #label_matrix_axis(nominal.bin_map,x1)
        #plt.show()
        print(f"smallest sigma = {smallest_sigma}, number of bins = {nominal.number_of_bins()}")
        
    #f,(x1,x2)=plt.subplots(1,2)
    #x1.imshow(nominal.bin_map,cmap="prism")
    #x2.imshow(sigmas)
    #x2.set_title(f"Sigmas ,Final")
    #x1.set_title(f"Bin Map ,Final")
    
    #label_matrix_axis(sigmas,x2,dec=1)
    #label_matrix_axis(nominal.bin_map,x1)
    #plt.show()
    unique_bins,unique_indices=np.unique(nominal.bin_map,return_index=True)
    x_centers=nominal.x_centers.flatten()[unique_indices]
    y_centers=nominal.y_centers.flatten()[unique_indices]
    nominal_z_values=nominal.z_values.flatten()[unique_indices]
    syst_z_values=syst.z_values.flatten()[unique_indices]

    return x_centers,y_centers,nominal_z_values,syst_z_values
def gaussian_smooth_2d(x_centers,y_centers,z_values,rebinned_x_centers,rebinned_y_centers,rebinned_z_values,smooth_parameter=1):
    x_sizes=np.sum(x_centers[0,1:]-x_centers[0,:-1])/(x_centers.shape[1])
    y_sizes=np.sum(y_centers[1:,0]-y_centers[:-1,0])/(y_centers.shape[0]) 
    smoothing=smooth_parameter*np.sqrt(x_sizes**2+y_sizes**2)
    smoothed_data=np.zeros((x_centers.shape[0],x_centers.shape[1]),dtype=np.float64)
    print(f" smoothing  values  {smoothing}")
    for i in range(x_centers.shape[0]):
        for j in range(x_centers.shape[1]):
            kernel=np.exp(-((x_centers[i,j]-rebinned_x_centers) ** 2+(y_centers[i,j]-rebinned_y_centers)**2) / (2 * smoothing ** 2))
            kernel=kernel/np.sum(kernel)
            smoothed_data[i,j]=np.sum(rebinned_z_values*kernel)
    return smoothed_data

def rebin_test():
    nominal=np.random.multivariate_normal(mean=[0,0],cov=np.array([[10,-3],[-3,10]]),size=10000)
    syst=np.random.multivariate_normal(mean=[0,0],cov=np.array([[8,5],[5,8]]),size=10000)
    nom_z_values,nom_x_edges,nom_y_edges=np.histogram2d(nominal[:,0],nominal[:,1],bins=([-10,-8,-6,-4,-2,0,2,4,6,8,10],[-8,-6,-4,-2,0,2,4,6,8]))
    syst_z_values,syst_x_edges,syst_y_edges=np.histogram2d(syst[:,0],syst[:,1],bins=([-10,-8,-6,-4,-2,0,2,4,6,8,10],[-8,-6,-4,-2,0,2,4,6,8]))
    nominal_hist=combinable_2dhist(nom_x_edges,nom_y_edges,nom_z_values.transpose()+0.5)
    syst_hist=combinable_2dhist(syst_x_edges,syst_y_edges,syst_z_values.transpose()+0.5)
    nominal_hist.display_hist()
    syst_hist.display_hist()
    rebin_hists_2d(syst_hist,nominal_hist)
    '''

    test_hist=combinable_2dhist(x_edges,y_edges,z_values)
    print(test_hist)
    print(test_hist.get_neighbors(33))
    print(test_hist.bin_map[test_hist.get_neighbors(33)])

    test_hist.combine_bins(33,34)
    test_hist.combine_bins(33,10)
    
    print(test_hist)
    print(test_hist.get_neighbors(10))
    print(test_hist.bin_map[test_hist.get_neighbors(33)])         
    '''
def smoothing_test(smoothing):
    lisa = image.imread('/home/guy/Pictures/mona_lisa.jpeg')
    x_size=lisa.shape[0]
    y_size=lisa.shape[1]
    x_array=np.arange(0,x_size,1)
    y_array=np.arange(0,y_size,1)
    x_centers,y_centers=np.meshgrid(x_array,y_array)

    lisa_red=lisa[:,:,0]
    lisa_green=lisa[:,:,1]
    lisa_blue=lisa[:,:,2]
    #plt.imshow(lisa_red)
    #plt.show()
    smoothed_lisa_red=gaussian_smooth_2d(x_centers,y_centers,lisa_red.transpose(),smoothing)
    smoothed_lisa_green=gaussian_smooth_2d(x_centers,y_centers,lisa_green.transpose(),smoothing)
    smoothed_lisa_blue=gaussian_smooth_2d(x_centers,y_centers,lisa_blue.transpose(),smoothing)

    new_image=np.dstack((smoothed_lisa_red.transpose(),smoothed_lisa_green.transpose(),smoothed_lisa_blue.transpose()))

    new_image=new_image.astype(int)
    plt.imshow(new_image)
    plt.savefig(f"mona_lisa_smoothing_{smoothing}.pdf")
    plt.cla()
    plt.close()
    print("Saved figure!")

def smoothing_with_rebinning(smoothing):
    nominal=np.random.multivariate_normal(mean=[0,0],cov=np.array([[10,-3],[-3,10]]),size=10000)
    syst=np.random.multivariate_normal(mean=[0,0],cov=np.array([[8,5],[5,8]]),size=10000)
    nom_z_values,nom_x_edges,nom_y_edges=np.histogram2d(nominal[:,0],nominal[:,1],bins=([-10,-8,-6,-4,-2,0,2,4,6,8,10],[-8,-6,-4,-2,0,2,4,6,8]))
    syst_z_values,syst_x_edges,syst_y_edges=np.histogram2d(syst[:,0],syst[:,1],bins=([-10,-8,-6,-4,-2,0,2,4,6,8,10],[-8,-6,-4,-2,0,2,4,6,8]))
    nom_z_values+=0.5
    syst_z_values+=0.5
    nominal_hist=combinable_2dhist(nom_x_edges,nom_y_edges,nom_z_values.transpose())
    syst_hist=combinable_2dhist(syst_x_edges,syst_y_edges,syst_z_values.transpose())
    x_centers,y_centers=nominal_hist.x_centers,nominal_hist.y_centers
    sigma_values=np.absolute(nominal_hist.z_values-syst_hist.z_values)/np.sqrt(nominal_hist.z_values)
    rebinned_x_centers,rebinned_y_centers,nominal_rebinned,syst_rebinned=rebin_hists_2d(syst_hist,nominal_hist)
    rebinned_sigma_values=np.absolute(nominal_rebinned-syst_rebinned)/np.sqrt(nominal_rebinned)
    smoothed_data=gaussian_smooth_2d(x_centers,y_centers,sigma_values,rebinned_x_centers,rebinned_y_centers,rebinned_sigma_values,smooth_parameter=smoothing)
    f,(x1,x2)=plt.subplots(1,2)
    x1.imshow(sigma_values)
    x2.imshow(smoothed_data)
    print(type(x1))
    plt.suptitle(f"Smoothing = {smoothing}")
    x1.set_title("|Syst - Nominal| / Nominal before smoothing")
    x2.set_title("|Syst - Nominal| / Nominal after smoothing")
    label_matrix_axis(sigma_values,x1,dec=1)
    label_matrix_axis(smoothed_data,x2,dec=1)
    plt.show()

if __name__=="__main__":
    smoothing_with_rebinning(0.1)
    smoothing_with_rebinning(0.5)
    smoothing_with_rebinning(1)
    smoothing_with_rebinning(2)

    #rebin_test()
