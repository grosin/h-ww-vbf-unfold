import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from numpy import linalg as LA
from random import choices
from random import random

def print_matrix(matrix):
    plt.matshow(matrix)
    for i in range(matrix.shape[1]):
        for j in range(matrix.shape[0]):
            c = round([j,i],0)
            plt.text(i, j, str(c), va='center', ha='center')
        plt.colorbar()
        plt.title(f"{name}")
        plt.savefig(f"{name}_matrix.pdf")
        #plt.show()

def get_data():
    merged_tree = uproot.open(f"ntupls/merged/unfolding_olv_merged_total.root")["reco_truth_merge"]
    panda_frame=merged_tree.pandas.df(["t_higgs_pt","r_higgs_pt","recoMatch","truthMatch"])
    panda_frame[['t_higgs_pt','r_higgs_pt']] =  panda_frame[['t_higgs_pt','r_higgs_pt']].div(1000,axis=0) #to gev
    return panda_frame.round(0).astype(int)

def condition(truth,reco,binning):
    matrix,xedges,yedges=np.histogram2d(truth,reco,binning)
    return LA.cond(matrix)

def eff_purity(truth,reco,binning):
    matrix,xedges,yedges=np.histogram2d(truth,reco,binning)
    diagonal=np.diag(matrix)
    effeciency=diagonal/matrix.sum(axis=0)
    purity=diagonal/matrix.sum(axis=1)
    return effeciency

def optimize_binning(truth,reco,nbins=10,min_bin=0,max_bin=250):
    bin_size=int((max_bin-min_bin)/nbins)
    bins=[i*bin_size for i in range(nbins+1)]
    bins_x=np.arange(1,nbins)
    eff=eff_purity(truth,reco,bins)
    for i in range(2000):
        weights=(eff[1:]+eff[:-1])/2
        bin_num=choices(bins_x,weights)[0]
        old_value=bins[bin_num]
        min_value=bins[bin_num-1]
        max_value=bins[bin_num+1]
        bin_value=min_value+int(round(random()*(max_value-min_value),0))
        bins[bin_num]=bin_value
        new_eff=eff_purity(truth,reco,bins)
        if(np.std(new_eff) > np.std(eff)):
            print(bins)
            eff=new_eff
        else:
            bins[bin_num]=old_value
    return bins


df=get_data()
print(df.head(5))
truth=df[np.logical_and(df['truthMatch']==1,df['recoMatch']==1)]['t_higgs_pt']
reco=df[np.logical_and(df['truthMatch']==1,df['recoMatch']==1)]['r_higgs_pt']
binning_recomendations=[]
for i in range(3,10):
    binning_recomendations.append(optimize_binning(truth,reco,nbins=i))

print("------------------------------------")
for recommendation in binning_recomendations:
    print(recommendation)

