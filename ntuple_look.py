import uproot
import numpy as np
import matplotlib.pyplot as plt

hist_name="CRTop_0_bdt_TopWWAll"
file_name="ntupls/hist_v2_StefBinning_v21_r21_28_09_2020_v1_noBDTCut_SRL_ggFCRZeroOneJet_noSys_ABCD_TopCRinSR_decorr1JetSys_Mjj.root"

file_data=uproot.open(file_name)
for key, hist in  file_data.items():
    if "CRTop" in str(key):
        print(hist.values)
        
