import uproot_methods.classes.TH1
import numpy as np
import matplotlib.pyplot as plt
import types
import uproot

class MyTH1(uproot_methods.classes.TH1.Methods, list):
    def __init__(self, variable_bins, values, title=""):
        self._fXaxis = types.SimpleNamespace()
        self._fXaxis._fXbins = types.SimpleNamespace()

        self._fXaxis._fNbins = len(values)+1
        self._fXaxis._fXmin = min(variable_bins)
        self._fXaxis._fXmax = max(variable_bins)
        self._fXaxis._fXbins=[i for i in variable_bins]
        for x in values:
            self.append(float(x))
        self._fTitle = title
        self._classname = "TH1F"
        

def numpy_to_hist(values,edges,name):
    return MyTH1(edges,values,name)

def hist_from_data(data,edges,name,weights):
    values,edges=np.histogram(data,bins=edges,weights=weights)
    return  MyTH1(edges,values,name)

def write_histograms(dict_of_histograms,filename):
    with uproot.recreate(filename) as f:
        for name,hist in dict_of_histograms.items():
            f[name]=hist


def get_trees_in_file(root_file):
    tree_file = uproot.open(root_file)
    trees=tree_file.keys()
    tree_file.close()
    return trees

def get_list_of_systematics():
    root_file="ntupls/systematics/vbf.root"
    trees=get_trees_in_file(root_file)
    #extract the name from proces_name_of_sys__1up;1
    systematics=["_".join(str(syst).split("__")[0].split("_")[1:]) for syst in trees if "nominal" not in str(syst)]
    return systematics
if __name__=="__main__":
    
    with uproot.recreate("test_root_file.root") as f:
        data=np.random.normal(size=1000)
        values,edges=np.histogram(data,bins=[-2,-1,0,0.5,1])
        hist=MyTH1(edges,values,"gaus")
        print(values)
        print(edges)
        f["gaus"]=hist
    
    hist=uproot.open("test_root_file.root")['gaus']
    print(vars(hist))
    '''
    from effeciency import bin_dict,get_background_signal_data,unfold_obs,my_naming,backgrounds

    variations=("up","down")
    pref="__1"
    systematics=get_list_of_systematics()
    histograms={obs:{} for obs in unfold_obs}
    print(histograms)
    for syst in systematics[:2]:
        for var in variations:
            vbf,back=get_background_signal_data(syst+pref+var)
            for obs in unfold_obs:
                binning=np.array(bin_dict[my_naming[obs]])
                histograms[obs][syst+var]={"vbf":hist_from_data(vbf[obs],binning,obs+"_"+syst+"_"+var+"_vbf",vbf["weight"])}
                for name,data in back.items():
                    histograms[obs][syst+var][name]=hist_from_data(data[obs],binning,obs+"_"+syst+"_"+var+"_"+name,data["weight"])
                
    
    for obs,data in histograms.items():
        filename=f"{obs}_sytematics.root"
        with uproot.recreate(filename) as f:
            for syst,hist in data.items():
                name_vbf=syst+"_vbf"
                f[name_vbf]=hist["vbf"]
                for back in backgrounds:
                    name_back=syst+"_"+back
                    f[name_back]=hist[back]
    '''
