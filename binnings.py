import configparser
import numpy as np
def get_bin_dict(overflow=False):
    config=configparser.ConfigParser()
    config.read('binning_config.ini')
    bin_data={}
    for section in config.sections():
        split_bins=config[section]["binning"].split(",")
        bin_array=[float(bin_edge) for bin_edge in split_bins]
        bin_data[section]=bin_array
        if(overflow):
            bin_data.append(np.inf)
    return bin_data

def validate_binnings():
    binning_dict=get_bin_dict()
    for obs,bins in binning_dict.items():
        print(f"validating {obs}")
        valid=True
        for i in range(len(bins)-1):
            if bins[i] >= bins[i+1]:
                print(f"Issue with bin {i}: {bins[i]} >= {bins[i+1]}")
                valid=False
        if valid:
            print("Validation completed")
            
def systematics_set():
    up_down_set={
        "FT_EFF_Eigen_B_0",
        "JET_Flavor_Composition",
        "JET_EtaIntercalibration_Modelling",
        "EL_EFF_ID_CorrUncertaintyNP14",
        "FT_EFF_Eigen_B_1",
        "MUON_SAGITTA_RESBIAS",
        "JET_EffectiveNP_Modelling1",
        "MUON_EFF_ISO_SYS",
        "MUON_EFF_RECO_SYS",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP3",
        "JET_Pileup_RhoTopology",
        "FT_EFF_Eigen_Light_0",
        "JET_BJES_Response",
        "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
        "JET_Pileup_PtTerm",
        "JET_JvtEfficiency",
        "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
        "JET_EtaIntercalibration_TotalStat",
        "EL_EFF_ID_CorrUncertaintyNP11",
        "MUON_EFF_RECO_STAT",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP8",
        "EG_SCALE_ALL",
        "FT_EFF_Eigen_C_0",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP6",
        "MUON_EFF_TrigStatUncertainty",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP12",
        "EL_EFF_ID_CorrUncertaintyNP10",
        "JET_EffectiveNP_Mixed1",
        "MUON_EFF_TTVA_SYS",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP4",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP5",
        "JET_EffectiveNP_Modelling2",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP7",
        "MUON_EFF_ISO_STAT",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP17",
        "MUON_EFF_TTVA_STAT",
        "JET_EtaIntercalibration_NonClosure_posEta",
        "JET_EffectiveNP_Detector1",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP15",
        "JET_EffectiveNP_Statistical4",
        "JET_EffectiveNP_Mixed3",
        "JET_EtaIntercalibration_NonClosure_negEta",
        "EL_EFF_ID_CorrUncertaintyNP8",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP16",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP13",
        "FT_EFF_Eigen_C_1",
        "EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP14",
        "JET_Pileup_OffsetNPV",
        "EL_EFF_ID_CorrUncertaintyNP7",
        "EL_EFF_ID_CorrUncertaintyNP6",
        "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
        "JET_EffectiveNP_Modelling4",
        "JET_EffectiveNP_Statistical6",
        "EG_RESOLUTION_ALL",
        "FT_EFF_Eigen_Light_3",
        "JET_EffectiveNP_Statistical3",
        "JET_EffectiveNP_Detector2",
        "JET_PunchThrough_MC16",
    }
    down_up_set={
        "EL_EFF_ID_CorrUncertaintyNP4",
        "EL_EFF_ID_CorrUncertaintyNP3",
        "EL_EFF_ID_CorrUncertaintyNP5",
        "FT_EFF_extrapolation",
        "FT_EFF_Eigen_Light_2",
        "FT_EFF_extrapolation_from_charm",
        "FT_EFF_Eigen_C_2",
        "FT_EFF_Eigen_Light_1",
        "JET_EffectiveNP_Statistical1",
        "EL_EFF_ID_CorrUncertaintyNP9",
        "JET_EffectiveNP_Statistical2",
        "JET_EffectiveNP_Statistical5",
        "EL_EFF_ID_CorrUncertaintyNP13",
        "MUON_ID",
        "JET_EffectiveNP_Mixed2",
        "JET_EffectiveNP_Modelling3",
        "JET_Pileup_OffsetMu",
        "MUON_SCALE",
        "EL_EFF_ID_CorrUncertaintyNP12",
        "MUON_EFF_TrigSystUncertainty",
        "PRW_DATASF",
        "FT_EFF_Eigen_B_2",
        "JET_Flavor_Response",
        "MUON_MS",
    }
    return up_down_set,down_up_set
if __name__=="__main__":
    validate_binnings()
