import ROOT
import sys
from array import array
from collections import OrderedDict

def GetHistograms(testtree,mycuts,varname,low,high):
    #testShist = ROOT.TH1F("testShist",varname,10,low,high)
    VarHistDict = OrderedDict()
    VarHistDict["All"] = ROOT.TH1F("All",varname,10,low,high)
    for bincut in mycuts:
        VarHistDict[bincut] = ROOT.TH1F(bincut,varname,10,low,high)
    for entryNum in range(0,testtree.GetEntries()):
        testtree.GetEntry(entryNum)
        bdt_vbf = getattr(testtree,"bdt_vbf")
        weight = getattr(testtree,"weight")
        inSR = getattr(testtree,"inSR")
        myvar = getattr(testtree,varname)
        VarHistDict["All"].Fill(myvar,weight*inSR)
        for bincut in mycuts:
            if eval(bincut):
                VarHistDict[bincut].Fill(myvar,weight*inSR)
    for x in VarHistDict:
        VarHistDict[x].Scale(1/VarHistDict[x].Integral())
        VarHistDict[x].SetDirectory(0)
        VarHistDict[x].SetStats(0)
        #VarHistDict[x].SetAxisRange(0.5,0.95,"X")
        VarHistDict[x].SetAxisRange(0.0,1,"Y")

    return VarHistDict

def HistoPlotter(HistDict,varname,plotname):

    c1 = ROOT.TCanvas('c1')
    c1.cd()
    c1.Clear()
    pad1 = ROOT.TPad("pad1","pad1",0,0.3,1,1)
    pad1.SetBottomMargin(0)
    pad1.Draw()
    pad1.cd()
    legend = ROOT.TLegend(0.15,0.5,0.5,0.9)
    j=1
    ratio = {}
    for x in HistDict:
        myhist = HistDict[x]
        if j==10 : j=11
        myhist.SetLineColor(j)
        #myhist.SetLineStyle(5)
        myhist.Draw("HIST SAME")
        j+=1
        ratio[x]=myhist.Clone()
        ratio[x].Divide(HistDict["All"])
        ratio[x].SetTitle("")
        ratio[x].GetXaxis().SetLabelSize(0.12)
        ratio[x].GetYaxis().SetLabelSize(0.1)
        ratio[x].GetXaxis().SetTitleSize(0.15)
        ratio[x].GetYaxis().SetTitleSize(0.15)
        ratio[x].GetXaxis().SetTitleOffset(0.7)
        ratio[x].GetYaxis().SetTitleOffset(0.3)
        ratio[x].GetYaxis().SetTitle("Bin/All")
        ratio[x].GetXaxis().SetTitle(varname)
        ratio[x].GetYaxis().SetRangeUser(0,2)
        ratio[x].GetYaxis().SetNdivisions(207)
        legend.AddEntry(myhist,x.replace("bdt_vbf", "D_{VBF}").replace("and",","))

    legend.Draw("SAME")
    c1.cd()
    pad2 = ROOT.TPad("pad2","pad2",0,0.05,1,0.3)
    pad2.SetTopMargin(0)
    pad2.SetBottomMargin(0.25)
    pad2.Draw()
    pad2.cd()
    for x in HistDict:
        ratio[x].Draw("SAME")

    c1.SaveAs(plotname)
    c1.Close()



if __name__ == "__main__":
    #AllCuts = ["bdt_vbf >= 0.00 and bdt_vbf < 0.50","bdt_vbf >= 0.50 and bdt_vbf < 0.70","bdt_vbf >= 0.70 and bdt_vbf < 0.86","bdt_vbf >= 0.86 and bdt_vbf < 0.88","bdt_vbf >= 0.88 and bdt_vbf < 0.90","bdt_vbf >= 0.90 and bdt_vbf < 0.92","bdt_vbf >= 0.92 and bdt_vbf < 0.94","bdt_vbf >= 0.94 and bdt_vbf < 0.96","bdt_vbf >= 0.96 and bdt_vbf < 1.00"]
    AllCuts = ["bdt_vbf >= 0.00 and bdt_vbf < 0.50","bdt_vbf >= 0.50 and bdt_vbf < 0.70","bdt_vbf >= 0.70 and bdt_vbf < 0.88","bdt_vbf >= 0.88 and bdt_vbf < 0.92","bdt_vbf >= 0.92 and bdt_vbf < 0.96","bdt_vbf >= 0.96 and bdt_vbf < 1.00"]
    VariableNames = [["Mjj",[200000,6000000]]]#,["MT",[15000,260000]],["Mll",[10000,200000]],["costhetastar",[0,1]],["DYll",[0,2.5]],["DYjj",[2.1,9]],["DPhill",[0,3.1416]],["Ptll",[0,500000]],["pt_H",[0,600000]],["lep0_pt",[22000,500000]],["lep1_pt",[15000,200000]],["jet0_pt",[30000,700000]],["jet1_pt",[30000,350000]],["SignedDPhijj",[-3.1416,3.1416]],["sumOfCentralitiesL",[0,2]],["jet0_eta",[-5,5]],["jet1_eta",[-5,5]],["DPhijj",[0,3.1416]],["ptTot",[0,100000]]]
    print("It begins...")
    filename = "ntupls/reco/2jet.root"
    treefile = ROOT.TFile.Open(filename,"READ")
    tree = treefile.Get("vbf_nominal")
    print (AllCuts)
    HistDictList = [OrderedDict() for x in range(len(VariableNames))]
    i=0
    for VariableName in VariableNames:
        HistDictList[i] = GetHistograms(tree,AllCuts,VariableName[0],VariableName[1][0],VariableName[1][1])
        HistoPlotter(HistDictList[i],VariableName[0],VariableName[0]+".pdf")
        i+=1
    treefile.Close()
    #HistDict = {"All":GetHistograms("All"),"MT":GetHistograms("MT"),"DPhijj":GetHistograms("DPhijj"),"DPhill":GetHistograms("DPhill"),"DYjj":GetHistograms("DYjj"),"DYll":GetHistograms("DYll"),"Mjj":GetHistograms("Mjj"),"Mll":GetHistograms("Mll"),"jet0_pt":GetHistograms("jet0_pt"),"jet1_pt":GetHistograms("jet1_pt")}#,"MTCut":GetHistograms("MTCut")}

