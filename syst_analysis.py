import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from binnings import get_bin_dict


binning=get_bin_dict()

def diff(arr):
    return arr.max()-arr.min()

def name(syst):
    st_name=syst.split(":")
    return st_name[0]

def up_or_down(syst):
    st_name=syst.split(":")
    if(len(st_name) > 1):
        return st_name[1]
    else:
        return "nominal"

def try1():
    nominal={"vh":5.96764,"Zjets":676.238,"ggf":51.6506,"top":11904.0,"diboson":1223.46,"Vgamma":44.1212}
    nominal_sum=np.sum([value for key, value in nominal.items()])

    data=pd.read_csv("sample_comparisons.txt")
    data["systematic_name"]=data["systematic"].apply(name)
    data["variation"]=data["systematic"].apply(up_or_down)
    data.drop(["systematic"],inplace=True,axis=1)
    data["sum"]=data.loc[:,"vh":"Vgamma"].apply(np.sum,axis=1)
    data["total_diff"]=data["sum"].apply(lambda x: x-nominal_sum)
    for sample,events in nominal.items():
        data[f"{sample}_diff"]=data[sample].apply(lambda x: x - events)
        data.sort_values(by="total_diff",inplace=True,ascending=False)
    print(data.head(5))
    data.to_excel("systematic_variations.xlsx")

def plot_feature_syst(feature):
    bins=np.array(binning[feature])
    data=pd.read_csv(f"{feature}_systematic_comparisons.txt")
    print(data.head(5))
    systematic_plots={}
    for row_index,row in data.iterrows():
        systematic=row["systematic"]
        if systematic not in systematic_plots:
            systematic_plots[systematic]={row['variation']:row["bin1":].values}
        else:
            systematic_plots[systematic][row['variation']]=row["bin1":].values
        
    nominal_data=systematic_plots["nominal"]['nominal']

    del systematic_plots["nominal"]
    x=(bins[1:]+bins[:-1])/2
    xerr=(bins[1:]-bins[:-1])/2
    print(bins)
    print(x)
    print(nominal_data)
    sum_error_down=np.zeros(len(x))
    sum_error_up=np.zeros(len(x))
    for systematic, variations in systematic_plots.items():
        plt.errorbar(x,nominal_data,xerr=xerr,fmt="b+")
        plt.errorbar(x,variations['up'],xerr=xerr,fmt="r+",label="up variation")
        plt.errorbar(x,variations['down'],xerr=xerr,fmt="g+",label="down variation")
        plt.title(f"{feature} {systematic}")
        plt.xlabel("GeV")
        plt.legend()
        plt.ylabel("Weighted MC Events")
        #plt.show()
        #plt.savefig(f"{systematic}_mjj.pdf")
        plt.cla()
        plt.close()
        sum_error_down=sum_error_down+(variations['down']-nominal_data)**2
        sum_error_up=sum_error_up+(variations['up']-nominal_data)**2
    
    sum_error_down=np.float64(sum_error_down**0.5)
    sum_error_up=np.float64(sum_error_up**0.5)
    print(type(sum_error_down))
    print(type(sum_error_up))
    nominal_data=np.array(nominal_data,dtype=np.float64)
    if(x[-1]> 1000):
        x=x/1000
    x=np.array(x,dtype=np.float64)
    stat_uncert=np.sqrt(nominal_data)
    plt.fill_between(x, nominal_data-sum_error_down,nominal_data+sum_error_up,facecolor='c',alpha=0.5,linewidth=2,edgecolor="k",linestyle='--' ,label="systematic variation sum ")
    plt.plot(x,nominal_data,"g+",label="Nominal_data")
    plt.title(f"{feature} Systematic variation")
    plt.legend()
    plt.savefig(f"{feature}_total_systematics.pdf")
    plt.cla()
    plt.close()

def process_variations(feature,process='top'):
    bins=np.array(binning[feature])
    names=["systematic","variation"]
    for n in range(len(bins)):
        names.append(f"bin{n}")
    data=pd.read_csv(f"{feature}_{process}_unweighted_comparisons.txt",names=names)
    data.drop(data.index[0],inplace=True)
    data.drop(["bin0"],axis=1,inplace=True)
    print(data.head(5))
    x=(bins[1:]+bins[:-1])/2
    xerr=(bins[1:]-bins[:-1])/2
    if(x[1]>1000):
        x=x/1000
        xerr=xerr/1000
    for row_index,row in data.iterrows():
        if row["variation"]=="nominal":
            plt.errorbar(x,row["bin1":],xerr=xerr,fmt="k+",label="Nominal",zorder=10)
        else:
            plt.errorbar(x,row["bin1":],xerr=xerr,fmt="+",label=row["systematic"])
    plt.legend()
    plt.ylabel("Expected c16a events")
    plt.title(f"{feature} {process}")
    #plt.show()
    figure = plt.gcf()  # get current figure
    figure.set_size_inches(16, 9)
    #mng = plt.get_current_fig_manager()
    #mng.full_screen_toggle()
    plt.savefig(f"{feature}_{process}_all_variations.pdf", bbox_inches='tight')
    plt.cla()
    plt.close()
if __name__=="__main__":
    processes=["vbf","top","diboson","htt","Zjets","ggf","vh"]
    obs=["Mjj","Mll","DYll","DYjj"]
    
    for ob in obs:
        for proc in processes:
            process_variations(ob,proc)
