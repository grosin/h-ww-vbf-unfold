import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
from numpy import linalg as LA
from numpy.random import multivariate_normal
from matplotlib import rc

def load_matrices(matrix_file):
    matrices={}
    with open(matrix_file) as fl:
        distribution=""
        for line in fl:
            if line.split(":")[0]=="variable":
                distribution=line.split(":")[1].rstrip()
            elif line.split(":")[0]=="size":
                if distribution in matrices:
                    matrices[distribution].append([])
                else:
                    matrices[distribution]=[[]]
                continue
            else:
                try:
                    line_data=[float(i) for i in line.split()]
                    if line_data:
                        matrices[distribution][-1].append(line_data)
                except ValueError:
                    matrices[distribution][-1]=np.array(matrices[distribution][-1])
                    continue
    return matrices

def asymetry_matrix(matrix):
    matrix=np.array(matrix)
    matrix=matrix[:-1,:]
    length=len(matrix)
    asym_matrix=np.zeros((length,length))
    for i in range(length):
        for j in range(length):
            if(matrix[j,i] == 0):
                continue
            asym_matrix[i,j]=matrix[i,j]/matrix[j,i]
    return asym_matrix

def average_matrices(matrix_array):
    numpy_array=np.array(matrix_array)
    average=np.mean(numpy_array,axis=0)
    deviation=np.std(numpy_array,axis=0)
    return (average,deviation)

def label_matrix(matrix):
    for i in range(matrix.shape[1]):
        for j in range(matrix.shape[0]):
            c = round(matrix[j,i],0)
            plt.text(i, j, str(c), va='center', ha='center')

def label_matrix_axis(matrix,axis,dec=0):
    for i in range(matrix.shape[1]):
        for j in range(matrix.shape[0]):
            c = round(matrix[j,i],dec)
            axis.text(i, j, str(c), va='center', ha='center')
def normalize_matrix(matrix):
    for i in range(matrix.shape[1]):
        truth_sum=np.sum(matrix[i,:])
        for j in range(matrix.shape[0]):
            c = round(matrix[j,i]/truth_sum,2)
            plt.text(j, i, str(c), va='center', ha='center')
def cond_number(matrix):
    condition_number=0
    if matrix[0,0] < 0.01:
        condition_number=LA.cond(matrix[1:,1:])
    else:
        condition_number=LA.cond(matrix)
    return condition_number
if __name__=="__main__":
    matrices=load_matrices("response_matrix.txt")
    name_map={"lep0_pt":"$Leading \; Lepton_{Pt}$",
              "lep1_pt":"$Subleading \; Lepton_{Pt}$",
              "jet0_pt":"$Leading \; Jet_{Pt}$",
              "jet1_pt":"$Subleading \; Jet_{Pt}$",
              "pt_H":"$H_{Pt}$",
              "Mjj":"$M_{jj}$",
              "Mll":"$M_{ll}$",
              "DYll":"$\Delta Y_{ll}$",
              "DYjj":"$\Delta Y_{jj}$",
              "costhetastar":r"Cos($\theta$*)",
              "DPhill":"$\Delta \phi_{ll}$",
              "SignedDPhijj":"$\Delta \phi_{jj}$"}
    for name,matrix in matrices.items():
        if("jet1" not in name):
            continue
        plt.title(f"{name_map[name]}",usetex=True)
        mat=np.array(matrix[0])
        plt.imshow(mat[1:,1:])
        normalize_matrix(mat[1:,1:])
        plt.xlabel("Reco")
        plt.ylabel("Truth")
        #plt.show()
        plt.savefig(f"{name}_response_matrix.pdf")
        plt.cla()
        plt.close()
