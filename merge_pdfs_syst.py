import os

path="systematics_comparison"
all_files=os.listdir(path)

all_plots={}
for fil in all_files:
    fil_elements=fil[:-4].split("_")
    process=""
    obs=""
    syst=""
    if "pt_" in fil:
        process=fil_elements[-1]
        obs="_".join(fil_elements[:2])
        syst="_".join(fil_elements[2:-2])
    else:
        process=fil_elements[-1]
        obs=fil_elements[0]
        syst="_".join(fil_elements[1:-2])
    if syst in all_plots:
        if obs in all_plots[syst]:
            all_plots[syst][obs][process]=fil
        else:
            all_plots[syst][obs]={process:fil}
    else:
        all_plots[syst]={obs:{process:fil}}

for syst,values in all_plots.items():
    to_merge=[]
    for name,obs in values.items():
        for process,fil in obs.items():
            to_merge.append(f"{path}/{fil}")
    output_name=f"{syst}_all_samples.pdf"
    os.system(f"pdftk {' '.join(to_merge)} output {output_name}")
