import numpy as np
import matplotlib.pyplot as plt
import re

vbf_bins=[]
vbf_data=[]
top_bins=[]
top_data=[]
with open("bdt_binnings.txt") as bnfl:
    bdt="vbf"
    for line in bnfl:
        if line.split(" ")[0]=="bdt_vbf":
            nums=re.findall(r"-?\d+\.?\d+",line)
            vbf_bins.append(float(nums[0]))
        elif line.split(" ")[0]=="bdt_topww":
            nums=re.findall(r"-?\d+\.?\d+",line)
            top_bins.append(float(nums[0]))
            bdt="top"
        else:
            if bdt=="vbf":
                vbf_data.append(float(line.rstrip()))
            else:
                top_data.append(float(line.rstrip()))
vbf_bins.append(1.0)
top_bins.append(1.0)

vbf_bins=np.array(vbf_bins)
vbf_data=np.array(vbf_data)
top_bins=np.array(top_bins)
top_data=np.array(top_data)
plt.errorbar((vbf_bins[:-1]+vbf_bins[1:])/2,vbf_data/np.sum(vbf_data),xerr=(vbf_bins[1:]-vbf_bins[:-1])/2,fmt=",")
plt.title("vbf_bdt bin events")
plt.xlabel("bdt bin")
plt.ylabel("Percent Of Events")
plt.show()

plt.errorbar((top_bins[:-1]+top_bins[1:])/2,top_data/np.sum(top_data),xerr=(top_bins[1:]-top_bins[:-1])/2,fmt=",")
plt.title("TopWWAll_bdt bin events")
plt.xlabel("bdt bin")
plt.ylabel("Percent Of Events")
plt.show()
