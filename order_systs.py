import uproot
import numpy as np
import pandas as pd

def get_files(process):
    return [f"{process}.root",f"{process}_JERfixnew1.root",f"{process}_JERfixnew2.root"]
def get_all_systematics(process
