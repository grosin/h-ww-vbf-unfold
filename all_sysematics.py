import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from binnings import get_bin_dict

DO_BDT=False
unfold_obs=["lep0_pt","jet0_pt","Mll","DYjj","DYll"]
my_naming={"Mjj":"Mjj","Mll":"Mll","DYjj":"DYjj","DYll":"DYll","pt_H":"higgs_pt","lep0_pt":"lep_pt_0","jet0_pt":"jet_pt_0"}
systematics=["FT_EFF_Eigen_B_0","JET_Flavor_Composition","JET_EtaIntercalibration_Modelling","EL_EFF_ID_CorrUncertaintyNP14","JET_JER_EffectiveNP_1_MCsmear","FT_EFF_Eigen_B_1","MUON_SAGITTA_RESBIAS","EL_EFF_ID_CorrUncertaintyNP3"]
bin_dict=get_bin_dict()
backgrounds=["top","vbf","diboson","Zjets","htt","Vgamma","vh"]


def stacked_plot(binning,signal,backgrounds=None,labels=None,title=""):
    x=(binning[:-1]+binning[1:]) /2
    xerr=(binning[:-1]-binning[1:])
    plt.bar(x, signal, width=xerr, color='k',linewidth=1,label="vbf")
    for i in range(len(backgrounds)):
        plt.bar(x, backgrounds[i], width=xerr, bottom=signal+np.sum(backgrounds[:i],axis=0),label=labels[i])
    plt.title(title)
    plt.legend()
    #plt.show()
    name,cut=title.split(" ")
    if(not DO_BDT):
        plt.savefig(f"{name}_{cut}_stacked_plot.pdf")
    else:
        plt.savefig(f"{name}_{cut}_stacked_plot_bdt.pdf")        
    plt.cla()
    plt.close()
    
def effeciency(data,cut):
    return len(data[data["Mjj"]>cut])/len(data)

def get_tree(process,systematic,campaign="c16a"):    
    merged_tree = uproot.open(f"/eos/user/s/sagar/ATLAS/DumpednTuples_Systematics/{campaign}/{process}.root")[f"{process}_{systematic}"]
    print(f"getting tree {process}_{systematic}")
    cols=["Mjj","inSR","weight","bdt_vbf"].extend(unfold_obs)
    panda_frame=merged_tree.pandas.df(cols)
    panda_frame=panda_frame[panda_frame["inSR"]==1]
    panda_frame["bdt_vbf"]=(panda_frame["bdt_vbf"]+1)/2
    return panda_frame

def get_concat_trees(process,systematic):
    c16a=get_tree(process,systematic,campaign="c16a")
    c16d=get_tree(process,systematic,campaign="c16d")
    c16e=get_tree(process,systematic,campaign="c16d")
    total=pd.concat((c16a,c16d,c16e),copy=False)
    return total

def get_background_signal_data(systematic="nominal",full_run2=False):
    data_dict={}
    if full_run2:
        data_dict["top"]= get_concat_trees("top",systematic)
        vbf_data=get_concat_trees("vbf",systematic)
        data_dict["diboson"]=get_concat_trees("diboson",systematic)
        data_dict["zjets"]=get_concat_trees("Zjets",systematic)
        data_dict["htt"]=get_concat_trees("htt",systematic)
        data_dict["Vgamma"]=get_concat_trees("Vgamma",systematic)
        data_dict["vh"]=get_concat_trees("vh",systematic)
        return vbf_data,data_dict

    else:
        data_dict["top"]=get_tree("top",systematic)
        vbf_data=get_tree("vbf",systematic)
        data_dict["diboson"]=get_tree("diboson",systematic)
        data_dict["zjets"]=get_tree("Zjets",systematic)
        data_dict["htt"]=get_tree("htt",systematic)
        data_dict["Vgamma"]=get_tree("Vgamma",systematic)
        data_dict["vh"]=get_tree("vh",systematic)
        return vbf_data,data_dict
def plot_vbf(observable,binning,nominal_signal,up_estimate,down_estimate,mjj_cut,systematic):
    gev=1
    if observable in ("Mjj","Mll"):
        gev=1000
    binning=binning/gev
    x=(binning[:-1]+binning[1:]) /2
    xerr=(binning[:-1]-binning[1:]) /2
    plt.errorbar(x,nominal_signal,xerr=xerr,fmt="b+",label="nominal")
    plt.errorbar(x,up_estimate,xerr=xerr,fmt="g+",label="up")
    plt.errorbar(x,down_estimate,xerr=xerr,fmt="r+",label="down")
    plt.legend()
    if(not DO_BDT):
        plt.title(f"{observable} {systematic} mjj={mjj_cut}")
        plt.savefig(f"{observable}_{systematic}_{mjj_cut}.pdf")
    else:
        plt.title(f"{observable} {systematic} mjj={mjj_cut} BDT")
        plt.savefig(f"{observable}_{systematic}_{mjj_cut}_bdt.pdf")        
    plt.cla()
    plt.close()


def df_to_histogram(observable,data_dict,binning,mjj_cut):
    background=np.zeros(len(binning)-1)
    background_list=[]
    background_labels=[]
    for name,data in data_dict.items():
        data=data[data["Mjj"]/1000 > mjj_cut]
        weights=data["weight"]
        if(DO_BDT):
            weights=weights*data["bdt_vbf"]
        back_hist=np.histogram(data[observable],bins=binning,weights=weights)[0]
        background+=back_hist
        background_list.append(back_hist)
        background_labels.append(name)
    return background,background_list,background_labels

def create_histogram(observables,systematic,mjj_cuts=[0,450,700]):
    vbf_data,data_dict=get_background_signal_data(systematic,True)
    dict_of_variables={}
    for cut in mjj_cuts:
        dict_of_variables[cut]={}
        for observable in observables:
            binning=np.array(bin_dict[my_naming[observable]])
            background_data,background_list,background_labels=df_to_histogram(observable,data_dict,binning,cut)
            vbf_data=vbf_data[vbf_data["Mjj"]/1000 > cut]
            weights=vbf_data["weight"]
            if(DO_BDT):
                weights=weights*vbf_data["bdt_vbf"]
            signal=np.histogram(vbf_data[observable],bins=binning,weights=weights)[0]
            if(systematic=="nominal"):
                stacked_plot(binning,signal,background_list,background_labels,title=f"{observable} Mjj_cut={cut}")
            dict_of_variables[cut][observable]={"signal":signal,"background":background_data}
    return dict_of_variables

def loop_systematics(observables):
    variation_dict={}
    nominal_data=create_histogram(observables,"nominal")
    for cut_value,mjj_cut in nominal_data.items():
        for ob_name,ob in mjj_cut.items():
            if ob_name in variation_dict:
                variation_dict[f"{ob_name}___{cut_value}"]["nominal"]={"signal":ob["signal"],"background":ob["background"]}
            else:
                variation_dict[f"{ob_name}___{cut_value}"]={"nominal":{"signal":ob["signal"],"background":ob["background"]}}

    for systematic in systematics:
        up_variation=create_histogram(observables,systematic+"__1up")
        down_variation=create_histogram(observables,systematic+"__1down")
        for cut_value,mjj_cut in up_variation.items():
            for ob_name,ob in mjj_cut.items():
                variation_dict[f"{ob_name}___{cut_value}"][systematic]={"up":{"signal":ob["signal"],"background":ob["background"]}}
                
        for cut_value,mjj_cut in down_variation.items():
            for ob_name,ob in mjj_cut.items():
                variation_dict[f"{ob_name}___{cut_value}"][systematic]["down"]={"signal":ob["signal"],"background":ob["background"]}

    total_syst={}
    for name, hist in variation_dict.items():
        print(hist.keys())
        nominal_signal=hist["nominal"]["signal"]
        nominal_background=hist["nominal"]["background"]
        pseudo_data=nominal_signal+nominal_background
        total_syst[name]={"nominal":nominal_signal,"up":np.zeros(len(nominal_signal)),"down":np.zeros(len(nominal_signal))}
        for systematic in systematics:
            print(hist[systematic].keys())
            up_variation=hist[systematic]["up"]["background"]
            down_variation=hist[systematic]["down"]["background"]
            up_estimate=pseudo_data-down_variation
            down_estimate=pseudo_data-up_variation
            obs_name,cut=name.split("___")
            if(np.sum(up_estimate) < np.sum(down_estimate)):
                up_estimate,down_estimate=down_estimate,up_estimate
            total_syst[name]["up"]+=(up_estimate-nominal_signal)**2
            total_syst[name]["down"]+=(down_estimate-nominal_signal)**2
            #plot_vbf(obs_name,np.array(bin_dict[my_naming[obs_name]]),nominal_signal,up_estimate,down_estimate,cut,systematic)
        total_syst[name]["up"]=np.sqrt(total_syst[name]["up"])
        total_syst[name]["down"]=np.sqrt(total_syst[name]["down"])
    return total_syst

def plot_signal_background(variables,mjj_cuts):
    vbf_data,data_dict=get_background_signal_data("nominal",True)
    for variable in variables:
        for cut in mjj_cuts:
            binning=np.array(bin_dict[my_naming[variable]])
            print(binning)
            x=(binning[:-1]+binning[1:]) /2
            xerr=(binning[:-1]-binning[1:]) /2
            b_s_ratio=np.zeros(len(x))
            for name,data in data_dict.items():
                data=data[data["Mjj"] > cut]
                weights=data["weight"]
                if(DO_BDT):
                    weights=weights*data["bdt_vbf"]
                b_s_ratio+=np.histogram(data[variable],bins=binning,weights=weights)[0]
            weights=vbf_data["weight"]
            if(DO_BDT):
                weights=weights*vbf_data["bdt_vbf"]
            signal=np.histogram(vbf_data[variable],weights=vbf_data["weight"],bins=binning)[0]+0.000001
            print(f"background {b_s_ratio}")
            print(f"signal {signal}")
            b_s_ratio=b_s_ratio/signal
            plt.errorbar(x,b_s_ratio,xerr=xerr,fmt="r+")
            plt.xlabel(f"{variable}")
            if not DO_BDT:
                plt.title(f"{variable} Background / signal {variable} Cut={cut} GeV")
                plt.savefig(f"mjj_cut_{cut}_b_s_{variable}.pdf")

            else:
                plt.title(f"{variable} Background / signal {variable} Cut={cut} GeV")
                plt.savefig(f"mjj_cut_{cut}_b_s_{variable}_bdt.pdf")
            plt.cla()
            plt.close()

            #plt.show()
    #plt.savefig(f"mjj_cut_{mjj_cut}_b_s_{variable}.pdf")
    #plt.cla()
    #plt.close()
    
def plot_signal(variable,vbf_data,mjj_cut):
    gev=1
    if variable in ("Mjj","Mll"):
        gev=1000
    binning=np.array(bin_dict[variable])/gev
    print(binning)
    x=(binning[:-1]+binning[1:]) /2
    xerr=(binning[:-1]-binning[1:]) /2
    signal=np.histogram(vbf_data[variable],weights=vbf_data["weight"],bins=binning)[0]+0.000001
    plt.errorbar(x,signal,xerr=xerr,fmt="b+")
    plt.title(f"{variable} VBF Cut={mjj_cut} GeV")
    plt.xlabel(f"{variable}")
    if(not DO_BDT):
        plt.savefig(f"mjj_cut_{mjj_cut}_{variable}.pdf")
    else:
        plt.savefig(f"mjj_cut_{mjj_cut}_{variable}_BDT.pdf")        
    plt.cla()
    plt.close()

    
def main():   
    syst_uncert_data=loop_systematics(unfold_obs)
    for name,histogram in syst_uncert_data.items():
        obs,mjj_cut=name.split("___")
        binning=np.array(bin_dict[my_naming[obs]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[:-1]-binning[1:])/2
        nominal_signal=histogram["nominal"]
        down=histogram["down"]
        up=histogram["up"]
        stat=np.sqrt(nominal_signal)
        plt.errorbar(x,nominal_signal,xerr=xerr,yerr=stat,fmt="g+",label="stat. uncertainty")
        plt.fill_between(x,nominal_signal+up,nominal_signal-down,alpha=0.5,linewidth=2,edgecolor="k",linestyle='--',label="total systematic")
        plt.title(f"{obs} Mjj Cut={mjj_cut}")
        plt.savefig(f"{obs}_error_mjjcut_{mjj_cut}_nobdt.pdf")
        plt.cla()
        plt.close()

        #percentage error
        plt.errorbar(x,((up+down)/2)/nominal_signal,xerr=xerr,fmt="g+",label="stat. uncertainty")
        plt.title(f"{obs} Systematic/Bin Content Mjj Cut={mjj_cut}")
        if DO_BDT:
            plt.savefig(f"{obs}_error_percent_mjjcut_{mjj_cut}_bdt.pdf")
        else:
            plt.savefig(f"{obs}_error_percent_mjjcut_{mjj_cut}_nobdt.pdf")
        plt.cla()
        plt.close()

def plot_effeciencies():
    vbf_data,data_dict=get_background_signal_data("nominal",True)
    mjj_cuts=np.linspace(200000,1000000,200)
    signal_effeciency=[effeciency(vbf_data,cut) for cut in  mjj_cuts]
    plt.plot(mjj_cuts/1000,signal_effeciency,label="vbf")
    back_eff={}
    for name,data in data_dict.items():
        back_eff[name]=[effeciency(data,cut) for cut in  mjj_cuts]
        plt.plot(mjj_cuts/1000,back_eff[name],label=name)
    plt.title("Mjj Cut Effeciencies")
    plt.legend()
    plt.savefig("mjj_cut_effeciency.pdf")
if __name__=="__main__":
    main()
    #plot_signal_background(unfold_obs,[0,450000,700000])
    #plot_effeciencies()
    DO_BDT=True
    main()
    #plot_signal_background(unfold_obs,[0,450000,700000])
    #plot_effeciencies()

