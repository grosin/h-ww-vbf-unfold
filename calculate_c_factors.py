import uproot
import numpy as np
reco_pandas=uproot.open("ntupls/reco/2jet.root")['vbf_nominal'].pandas.df(["inSR","weight","Mjj","DPhill"])
truth_pandas=uproot.open("ntupls/truth/mtt_PowPy8-noMET.root")["Vbf_Total"].pandas.df(["weight","Mjj_truth","DPhill_truth"])

reco_pandas=reco_pandas[reco_pandas["inSR"]==1]

print(np.sum(reco_pandas["weight"])/np.sum(truth_pandas["weight"]))
print(np.sum(truth_pandas["weight"])/139.1)

#reco_pandas=reco_pandas[reco_pandas["Mjj"]>450000]
#reco_pandas=reco_pandas[reco_pandas["DPhill"]<1.4]

#truth_pandas=truth_pandas[truth_pandas["Mjj_truth"]>450000]
#truth_pandas=truth_pandas[truth_pandas["DPhill_truth"]<1.4]

print(np.sum(reco_pandas["weight"])/np.sum(truth_pandas["weight"]))
print(np.sum(truth_pandas["weight"])/139.1)
