import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
from binnings import *
data_folder="fluctuation_data_files/"

def profile(matrix):
    profile_hist=[]
    profile_errors=[]
    for i in range(matrix.shape[1]):
        profile_hist.append(np.mean(matrix[i,:]))
        profile_errors.append(np.std(matrix[i,:]))
    return (profile_hist,profile_errors)
        
def gaussian(x, a, mean, sigma):
    return a * np.exp(-((x - mean)**2 / (2 * sigma**2)))

def load_unfolding_uncertainty():
    ufile=data_folder+"unfolding_uncertinty.txt"
    uncert_dict={}
    with open(ufile,"r") as fl:
        for line in fl:
            line=line[:-1]
            line_data=line.split(",")
            dist=line_data[0]
            iteration=line_data[1]
            bins_data=np.array([float(f) for f in line_data[2:] if f],dtype=np.float64)
            if dist in uncert_dict:
                if iteration in uncert_dict[dist]:
                    uncert_dict[dist][iteration]=np.mean((uncert_dict[dist][iteration],bins_data),axis=0)
                else:
                    uncert_dict[dist][iteration]=bins_data
            else:
                uncert_dict[dist]={iteration:bins_data}
    return uncert_dict
def load_matrices(matrix_file):
    matrices={}
    with open(matrix_file) as fl:
        distribution=""
        for line in fl:
            if line.split(":")[0]=="variable":
                distribution=line.split(":")[1].rstrip()
            elif line.split(":")[0]=="size":
                matrices[distribution]=[]
                continue
            else:
                try:
                    line_data=[float(i) for i in line.split()]
                    if line_data:
                        matrices[distribution].append(line_data)
                except ValueError:
                    matrices[distribution]=np.array(matrices[distribution])
                    continue
    return matrices
                
def data_by_bins(data,iterations,columns):
    bin_arrays=[]
    for it in range(1,iterations+1):
        bin_data=[]
        for name in columns:
            iter_data=data.loc[data["iters"]==it,name]
            bin_means=(abs(np.mean(iter_data[1:])),np.std(iter_data))
            bin_data.append(bin_means)
        bin_arrays.append(bin_data)
    return bin_arrays

def load(data_file,columns):
    names=["Dist","iters"]+columns
    return pd.read_csv(data_file,names=names)

class BinData:
    def __init__(self,data_file,distribution,num_of_columns,binning,response_matrix,sigma=5,iterations=4):
        self.columns=[f"Column_{i}" for i in range(1,num_of_columns)]
        self.num_of_columns=num_of_columns
        self.raw_data=load(data_file,self.columns).fillna(0).replace([np.inf, -np.inf],0)
        self.processed_data=data_by_bins(self.raw_data,iterations,[f"Column_{i}" for i in range(1,num_of_columns)])
        self.distribution=distribution
        self.sigma=sigma
        self.iterations=iterations
        self.binning=binning
        self.response_matrix=np.array(response_matrix)
        
    def bin_by_bin_histogram(self,iteration):
        i=0
        matrix_data=[]
        profile_mean=[]
        profile_std=[]
        init=0
        for column in self.columns:
            try:
                data=self.raw_data.loc[self.raw_data["iters"]==iteration,column]/self.binning[i]
                yvalues,bins=np.histogram(data,bins=30)
                hist_data,edges=np.histogram(data,bins=60,range=(-0.05,0.05))
                matrix_data.append(hist_data)
                xvalues = (bins[:-1] + bins[1:]) / 2
                guesses=(80,round(np.mean(data),5),round(np.std(data),5))
                results,cc=curve_fit(gaussian,xvalues[yvalues>0],yvalues[yvalues>0],p0=guesses,maxfev=5000)
                #plt.plot(xvalues,gaussian(xvalues,*results),"r",label=f"Mean={round(results[1],4)}\nstd={round(results[2],4)}")
                profile_mean.append(results[1])
                profile_std.append(results[2])
                #print(results)
                #plt.title(f"Bin {i} For {self.distribution} with {iteration} Iterations")
                #plt.legend()
                #plt.show()
                #if(i > 1 and i <6):
                #    plt.savefig(f"{self.distribution}_Bin_{i}_hist.pdf")
                #plt.close()
                i+=1
            except Exception as e:
                profile_mean.append(1000)
                profile_std.append(1000)
                print(e)
                init+=1
                #plt.show()
                continue
        #print(matrix_data)

        matrix_data=[x for _,x in sorted(zip(self.binning,matrix_data))]
        #fig, ax = plt.subplots(1,1)
        #hist_matrix=np.array(matrix_data).transpose()
        #img = ax.imshow(hist_matrix,extent=[0,12,0.0005,-0.0005],aspect=10000)
        #ax.set_xticklabels(self.binning)
        #ax.set_ylabel("Truth-Unfolded")
        #ax.set_xlabel("Bin Content")
        #ax.set_title(f"{self.distribution} for Iteration {iteration}")
        
        #fig.colorbar(img, ax=ax)
        #plt.savefig(f'{self.distribution}_2d_hist_Iteration_{iteration}.pdf')
        #plt.show()
        #plt.clf()
        #plt.close()

        profile_mean=np.array(profile_mean[1:])
        profile_std=np.array(profile_std[1:])
        my_binning=np.array(self.binning)
        mask=np.logical_and(profile_mean < 1,profile_std < 1)
        #mask=np.logical_and(mask,my_binning >= 0)
        #plt.errorbar(my_binning[mask],profile_mean[mask],yerr=profile_std[mask],fmt=".",label=f"{iteration} iterations")
        masked_binning=my_binning[mask]
        masked_profile=profile_mean[mask]
        sort_both=zip(masked_binning,masked_profile)
        sort_both=sorted(sort_both)
        sorted_binnin=[x for x,y in sort_both]
        sorted_profile=[y for x,y in sort_both]
        plt.plot(sorted_binnin,sorted_profile,".--",label=f"{iteration} iterations")
        print(sorted_binnin)
        print(sorted_profile)
        #plt.show()
        #plt.savefig(f'{self.distribution}_profile_Iteration_{iteration}.pdf')
            
            

    def total_unfolding_error_vs_iteration(self,label=""):
        integral=[0]*self.iterations
        for j in range(self.num_of_columns-2):
            for i in range(self.iterations):
                print(j,i)
                integral[i]+=self.processed_data[i][j+1][0]
        x=[x+1 for x in range(len(integral))][:10]
        print(integral)
        plt.plot(x,integral[:10]/np.sum(self.binning),label=f"Average Unfolding Error {label}",)
        plt.title(f"Integrated Unfolding Difference vs bayesian iterations")
        plt.xlabel("Bayesian Iterations")
        plt.ylabel("Average Difference Unfolded and Weighted Truth")

    def get_total_error(self,iteration):
        total_error=0
        for j in range(self.num_of_columns):
            total_error+=self.processed_data[j][iteration-1][0]
        return total_error
    
    def unfolding_error_vs_events(self):
        per_bin_error=np.empty((self.num_of_columns,self.iterations))
        for j in range(self.num_of_columns-2):
            for i in range(self.iterations):
                per_bin_error[j,i]=self.processed_data[i][j][0]
                
        for i in range(self.iterations-3):
            plt.plot(self.binning[1:],per_bin_error[:,i][1:19],"*",label=f"{i+1} iterations")

        plt.legend()
        plt.title(f"average unfolding error vs. number of bin events for {self.distribution}")
        plt.show()

    def error_per_bin_vs_iteration(self):
        x=[x+1 for x in range(self.iterations)]
                
        for j in range(self.num_of_columns):
            bin_error=[0]*self.iterations
            print(len(self.processed_data))
            print(len(self.processed_data[1]))
            print(len(self.processed_data[1][1]))
            
            for i in range(self.iterations):
                try:
                    bin_error[i]=self.processed_data[i][j][0]/self.binning[j]
                except:
                    break
            plt.title(f"Error vs. Iterations for bin {j} for {self.distribution}")
            plt.plot(x,bin_error,label=f"Bin {j}")
            plt.xlabel("iteration")
            plt.ylabel("bin error")
            plt.legend()
            if(j%5==0):
                plt.savefig(f"{self.distribution}_error_v_iteration_{j}.pdf")
        plt.show()
            
    def get_error_per_bin(self,iteration):
        error_data=np.empty(self.num_of_columns)
        print(self.binning)
        for j in range(self.num_of_columns-2):
            print(self.binning[j])
            error_data[j]=self.processed_data[iteration-1][j][0]/self.binning[j]
        return error_data

    def plot_error_vs_bin(self,iteration):
        bin_error_list=self.get_error_per_bin(iteration)
        plt.plot([x for x in range(len(bin_error_list[1:-2]))],bin_error_list[1:-2],"g*-",label=f"Fluctuation Error")
        #plt.plot([x for x in range(len(DETALL_UNCERT))],[sig/events for sig,events in zip(DETALL_UNCERT,DETALL_BINNING)],"b*-",label="Stat. Uncertanty")
        plt.legend()

    def error_vs_off_diagonal(self,iteration):
        bin_error_list=self.get_error_per_bin(iteration)
        off_diagonal=np.sum(self.response_matrix,axis=1)-self.response_matrix.diagonal()
        plt.plot(np.absolute(bin_error_list[1:-1])[np.absolute(bin_error_list[1:-1])<1],off_diagonal[np.absolute(bin_error_list[1:-1])<1],'g.')
        plt.title(f"off diagonal_elements vs. bin error {self.distribution} with {iteration} iterations")
        plt.xlabel("bin error")
        plt.ylabel("off diagonal  elements")
        plt.show()

    def error_std_normalized(self):
        bin_data=[]
        for j in range(self.num_of_columns-2):
            iteration_data=[]
            for i in range(self.iterations-1):
                try:
                    iteration_data.append(self.processed_data[i+1][j+1][0]/self.processed_data[i+1][j+1][1])
                except:
                    pass
            bin_data.append(iteration_data)

        i=1
        j=1
        for bin_n in bin_data:
            plt.subplot(2,3,j%7)
            x=[i+1 for i in range(len(bin_n))]
            plt.plot(x,bin_n,"b.-")
            plt.title(f"{self.distribution} bin {i+1}  mean/std vs. Iteration")
            #plt.ylabel("Abs(mean)/standard deviation")
            if(j % 6 ==0):
               plt.show()
               plt.cla()
               j=1
            else:
                j+=1
            i+=1

def mjj_analysis():
       #variations
    ['mjj_sig_1.0variations_data.txt','mjj_sig_2.0variations_data.txt','mjj_sig_3.0variations_data.txt','mjj_sig_4.0variations_data.txt','mjj_sig_5.0variations_data.txt']

    
    one_sigma=BinData("fluctuation_data_files/mjj_sig_1.0variations_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=10)
    two_sigma=BinData("fluctuation_data_files/mjj_sig_2.0variations_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=10)
    three_sigma=BinData("fluctuation_data_files/mjj_sig_3.0variations_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=10)

    #for i in range(1,5):
    #    one_sigma_mjj.bin_by_bin_histogram(i)
        
    #one_sigma_mjj.total_unfolding_error_vs_iteration("1 sigma")
    #two_sigma_mjj.total_unfolding_error_vs_iteration("2 sigma")
   ## three_sigma_mjj.total_unfolding_error_vs_iteration("3 sigma")
    #plt.legend()
    one_sigma.bin_by_bin_histogram(1)
    one_sigma.bin_by_bin_histogram(2)
    one_sigma.bin_by_bin_histogram(3)
    one_sigma.bin_by_bin_histogram(4)
    one_sigma.bin_by_bin_histogram(5)
    plt.show()

def analyze_dist(dist,dist_bin,matrices):
    one_dist_data_file="fluctuation_data_files/"+dist+"_sig_1.0statistics_data.txt"
    one_sigma=BinData(one_dist_data_file,dist,len(dist_bin)+2,dist_bin,matrices[dist],sigma=1,iterations=10)
    
    zero_five_dist_data_file="fluctuation_data_files/"+dist+"_sig_0.5statistics_data.txt"
    zero_five_sigma=BinData(zero_five_dist_data_file,dist,len(dist_bin)+2,dist_bin,matrices[dist],sigma=1,iterations=10)
    
    two_dist_data_file="fluctuation_data_files/"+dist+"_sig_2.0statistics_data.txt"
    two_sigma=BinData(two_dist_data_file,dist,len(dist_bin)+2,dist_bin,matrices[dist],sigma=1,iterations=10)
    
    three_dist_data_file="fluctuation_data_files/"+dist+"_sig_3.0statistics_data.txt"
    three_sigma=BinData(three_dist_data_file,dist,len(dist_bin)+2,dist_bin,matrices[dist],sigma=1,iterations=10)
    #one_sigma.error_per_bin_vs_iteration()
    plt.clf()
    plt.close()
    one_sigma.bin_by_bin_histogram(1)
    one_sigma.bin_by_bin_histogram(2)
    one_sigma.bin_by_bin_histogram(3)
    one_sigma.bin_by_bin_histogram(4)
    one_sigma.bin_by_bin_histogram(5)

    plt.title(f"Average Bias vs. Bin Content {dist}")
    plt.xlabel("bin content")
    plt.ylabel("average error")
   # plt.ylim(-0.006,.006)
    plt.legend()
    plt.show()
    #zero_five_sigma.total_unfolding_error_vs_iteration("0.5 sigma")
    #one_sigma.total_unfolding_error_vs_iteration("2.0 sigma")
    #two_sigma.total_unfolding_error_vs_iteration("3.0 sigma")
    #three_sigma.total_unfolding_error_vs_iteration("1.0 sigma")
    #plt.legend()
    #plt.savefig(f"{dist}_total_unfolding_error.pdf")
    #plt.close()
if __name__=="__main__":
    matrices=load_matrices("response_matrix.txt")
    for name,matrix in matrices.items():
        plt.title(name)
        plt.imshow(matrix)
        plt.xlabel("Reco")
        plt.ylabel("Truth")
        plt.colorbar()
        plt.show()
    #unfolding_error=load_unfolding_uncertainty()
    #analyze_dist("dphijj",DPHIJJ_BINNING,matrices)
    #analyze_dist("detajj",DETAJJ_BINNING,matrices)
    #analyze_dist("mll",MLL_BINNING,matrices)
    #mjj_analysis()
    #analyze_dist("drjj",DRJJ_BINNING,matrices)
    #analyze_dist("drll",DRLL_BINNING,matrices)
    #analyze_dist("lead_jet_pt",JET_PT_BINNING,matrices)
    #analyze_dist("lead_lep_pt",LEP_PT_BINNING,matrices)
    #mjj_analysis()
    '''
    two_sigma_mjj=BinData("mjj_sig_2.0variations_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=10)
    #one_sigma_mjj.total_unfolding_error_vs_iteration("1 sigma")
    two_sigma_mjj.bin_by_bin_histogram(1)

    three_sigma_mjj=BinData("mjj_sig_3.0variations_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=10)
    #one_sigma_mjj.total_unfolding_error_vs_iteration("1 sigma")
    three_sigma_mjj.bin_by_bin_histogram(1)
    
    four_sigma_mjj=BinData("mjj_sig_4.0variations_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=10)
    #one_sigma_mjj.total_unfolding_error_vs_iteration("1 sigma")
    four_sigma_mjj.bin_by_bin_histogram(1)
    
    five_sigma_mjj=BinData("mjj_sig_5.0variations_data.txt","mjj",15,MJJ_BINNING,matrices['mjj'],sigma=1,iterations=10)
    #one_sigma_mjj.total_unfolding_error_vs_iteration("1 sigma")
    five_sigma_mjj.bin_by_bin_histogram(1)
    '''
