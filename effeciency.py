import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from binnings import get_bin_dict
from operator import gt,lt
from analyze_matrices import label_matrix
from syst_studies import *
from scipy.optimize import curve_fit
from sklearn.neighbors import LocalOutlierFactor
DO_BDT=False
unfold_obs=["Mjj","lep0_pt","jet0_pt","Mll","DYjj","DYll"]
jet_obs=["Mjj","DYjj","jet0_pt","jet1_pt","jet0_eta","jet1_eta"]
my_naming={"Mjj":"Mjj","Mll":"Mll","DYjj":"DYjj","DYll":"DYll","pt_H":"higgs_pt","lep0_pt":"lep0_pt","jet0_pt":"jet0_pt","jet1_pt":"jet1_pt","MT":"MT","jet0_eta":"jet0_eta","jet1_eta":"jet1_eta"}
systematics=['JET_Flavor_Composition','JET_JER_EffectiveNP_1','JET_JER_EffectiveNP_2','JET_JER_EffectiveNP_3','JET_EtaIntercalibration_Modelling','JET_JER_DataVsMC_MC16']
bin_dict=get_bin_dict()
backgrounds=["top","diboson","Zjets","htt","Vgamma","vh","ggf"]

Full_run2=False
ntuple_directory="ntupls/systematics"
if(Full_run2):
    ntuple_directory="/eos/user/s/sagar/ATLAS/DumpednTuples_Systematics"
def get_files(process):
    return [f"{process}.root",f"{process}_JERfixnew1.root",f"{process}_JERfixnew2.root"]
def get_all_systematics():
    all_systematics=[]
    files=[f"vbf_JERfixnew1.root",f"vbf_JERfixnew2.root"]
    for fil in files:
        tree_file=uproot.open("{ntuple_directory}/c16a"+fil)
        for key,values in tree_file.items():
            key=str(key.decode())
            if 'nomina' in key:
                continue
            if "JER" not in fil:
                if "MCsmear" in key or "PDsmear" in key:
                    continue
            all_systematics.append("_".join(key.split("__")[0].split("_")[1:]))
        tree_file.close()
    return all_systematics

def find_root_file(process,tree_name,campaign="ca16a"):
    possible_files=[f"{ntuple_directory}/{campaign}/{process}.root",f"{ntuple_directory}/{campaign}/{process}_JERfixnew1.root",f"{ntuple_directory}/{campaign}/{process}_JERfixnew2.root"]
    if("JET_JER" in tree_name):
        possible_files=possible_files[1:]
    for fil in possible_files:
        tree_file=uproot.open(fil)
        if tree_name in tree_file:
            tree_file.close()
            return fil
        tree_file.close()
        
def get_tree(process,systematic,campaign="c16a"):
    systematic_name=f"{process}_{systematic}"
    filename=find_root_file(process,systematic_name,campaign)
    with uproot.open(filename) as rootfile:
        merged_tree = rootfile[systematic_name]
        print(f"getting tree {process}_{systematic}")
        cols=["inSR","weight","bdt_vbf"].extend(unfold_obs)
        panda_frame=merged_tree.pandas.df(cols)
        panda_frame=panda_frame[panda_frame["inSR"]==1]
        panda_frame["bdt_vbf"]=(panda_frame["bdt_vbf"]+1)/2
        print(f"Total number of events {len(panda_frame)}")
        panda_frame=cut_data(panda_frame,1)
        print(f"Total number of events after cut {len(panda_frame)}")
    return panda_frame


def stacked_plot(binning,signal,backgrounds=None,labels=None,title=""):
    x=(binning[:-1]+binning[1:]) /2
    xerr=(binning[:-1]-binning[1:])
    plt.bar(x, signal, width=xerr, color='k',linewidth=1,label="vbf")
    for i in range(len(backgrounds)):
        plt.bar(x, backgrounds[i], width=xerr, bottom=signal+np.sum(backgrounds[:i],axis=0),label=labels[i])
    plt.title(title)
    plt.legend()
    #plt.show()
    name,cut=title.split(" ")
    if(not DO_BDT):
        plt.savefig(f"{name}_{cut}_stacked_plot.pdf")
    else:
        plt.savefig(f"{name}_{cut}_stacked_plot_bdt.pdf")        
    plt.cla()
    plt.close()
    
def effeciency(var,data,cut,comp):
    return np.sum(data[comp(data[var],cut)]["weight"])/np.sum(data["weight"])

def mt_effeciency():
    vbf_data,data_dict=get_background_signal_data()
    comp=lt
    cuts=np.linspace(0,200000,100)
    signal_effeciency=[effeciency("MT",vbf_data,cut,comp) for cut in cuts]
    plt.plot(cuts,signal_effeciency,label="vbf")
    back_eff={}
    total_events=0
    cut_events_150=0
    cut_events_200=0

    for name,data in data_dict.items():
        back_eff[name]=[effeciency("MT",data,cut,comp) for cut in cuts]
        plt.plot(cuts,back_eff[name],label=name)
        total_events+=np.sum(data["weight"])
        cut_events_200+=np.sum(data[comp(data["MT"],200000)]["weight"])
        cut_events_150+=np.sum(data[comp(data["MT"],150000)]["weight"])
        
    plt.title(f"MT Cut Effeciencies with Mjj and Mll cuts")
    plt.legend()
    plt.savefig(f"MT_cut_effeciency_with_cuts.pdf")
    plt.cla()
    plt.close()
    print(f"200 MT cut effeciency {cut_events_200/total_events}")
    print(f"150 MT cut effeciency {cut_events_150/total_events}")

def mtt_effeciency():
    vbf_data,data_dict=get_background_signal_data()
    comp=lt
    cuts=np.linspace(0,66000,100)
    signal_effeciency=[effeciency("mtt",vbf_data,cut,comp) for cut in cuts]
    plt.plot(cuts,signal_effeciency,label="vbf")
    back_eff={}
    total_events=0

    for name,data in data_dict.items():
        back_eff[name]=np.array([effeciency("mtt",data,cut,comp) for cut in cuts])
        if name in ("top","Zjets","diboson"):
            plt.plot(cuts,back_eff[name],label=name)
        total_events+=np.sum(data["weight"])
    total_effeciency=np.zeros(len(cuts))
    for name,eff in back_eff.items():
        total_effeciency+=eff*np.sum(data_dict[name]["weight"])
    total_effeciency=total_effeciency/total_events
    plt.plot(cuts,total_effeciency,label="total background")
    plt.title(f"mtt cut Effeciencies")
    plt.xlabel("mtt [MeV]")
    plt.legend()
    plt.show()
    #plt.savefig(f"MT_cut_effeciency_with_cuts.pdf")
    #plt.cla()
    #plt.close()

def mtt_effeciency():
    vbf_data,data_dict=get_background_signal_data()
    comp=lt
    cuts=np.linspace(0,66000,100)
    signal_effeciency=[effeciency("mtt",vbf_data,cut,comp) for cut in cuts]
    plt.plot(cuts,signal_effeciency,label="vbf")
    back_eff={}
    total_events=0

    for name,data in data_dict.items():
        back_eff[name]=np.array([effeciency("mtt",data,cut,comp) for cut in cuts])
        if name in ("top","Zjets","diboson"):
            plt.plot(cuts,back_eff[name],label=name)
        total_events+=np.sum(data["weight"])
    total_effeciency=np.zeros(len(cuts))
    for name,eff in back_eff.items():
        total_effeciency+=eff*np.sum(data_dict[name]["weight"])
    total_effeciency=total_effeciency/total_events
    plt.plot(cuts,total_effeciency,label="total background")
    plt.title(f"mtt cut Effeciencies")
    plt.xlabel("mtt [MeV]")
    plt.legend()
    
def jet0_effeciency():
    vbf_data,data_dict=get_background_signal_data()
    comp=gt
    jet_pt_cuts=np.linspace(30000,60000,100)
    dyjj_cuts=np.linspace(2.1,3.1,10)
    signal_effeciency=[effeciency("jet1_pt",vbf_data,cut,comp) for cut in jet_pt_cuts]
    plt.plot(jet_pt_cuts/1000,signal_effeciency,label="vbf")
    back_eff={}
    total_events=0

    for name,data in data_dict.items():
        back_eff[name]=np.array([effeciency("jet1_pt",data,cut,comp) for cut in jet_pt_cuts])
        if name in ("top","Zjets","diboson"):
            plt.plot(jet_pt_cuts/1000,back_eff[name],label=name)
        total_events+=np.sum(data["weight"])
    total_effeciency=np.zeros(len(jet_pt_cuts))
    for name,eff in back_eff.items():
        total_effeciency+=eff*np.sum(data_dict[name]["weight"])
    total_effeciency=total_effeciency/total_events
    plt.plot(jet_pt_cuts/1000,total_effeciency,label="total background")
    plt.title(f"jet1 pt cut Effeciencies")
    plt.xlabel("jet1 Pt [GeV]")
    plt.legend()
    plt.show()


def cut_data(data,cut):
    return data[(data["Mjj"]/1000 > 700) & (data["DYjj"] > 3.0)]


def get_concat_trees(process,systematic):
    c16a=get_tree(process,systematic,campaign="c16a")
    c16d=get_tree(process,systematic,campaign="c16d")
    c16e=get_tree(process,systematic,campaign="c16d")
    total=pd.concat((c16a,c16d,c16e),copy=False)
    del c16a
    del c16d
    del c16e
    return total

def get_background_signal_data(systematic="nominal",processes=["top","diboson","Zjets","Vgamma","ggf","vh","htt"],full_run2=Full_run2):
    data_dict={}
    print(processes)
    if full_run2:
        vbf_data=get_tree("vbf",systematic)
        for proc in processes:
            data_dict[proc]=get_concat_trees(proc,systematic)
        return vbf_data,data_dict
    else:
        vbf_data=get_tree("vbf",systematic)
        for proc in processes:
            data_dict[proc]=get_tree(proc,systematic)
        return vbf_data,data_dict
def plot_vbf(observable,binning,nominal_signal,up_estimate,down_estimate,mjj_cut,systematic):
    gev=1
    if observable in ("Mjj","Mll"):
        gev=1000
    binning=binning/gev
    x=(binning[:-1]+binning[1:]) /2
    xerr=(binning[:-1]-binning[1:]) /2
    plt.errorbar(x,nominal_signal,xerr=xerr,fmt="b+",label="nominal")
    plt.errorbar(x,up_estimate,xerr=xerr,fmt="g+",label="up")
    plt.errorbar(x,down_estimate,xerr=xerr,fmt="r+",label="down")
    plt.legend()
    if(not DO_BDT):
        plt.title(f"{observable} {systematic} mjj={mjj_cut}")
        plt.savefig(f"{observable}_{systematic}_{mjj_cut}.pdf")
    else:
        plt.title(f"{observable} {systematic} mjj={mjj_cut} BDT")
        plt.savefig(f"{observable}_{systematic}_{mjj_cut}_bdt.pdf")        
    plt.cla()
    plt.close()


def df_to_histogram(observable,data_dict,binning,cut):
    background=np.zeros(len(binning)-1)
    background_list=[]
    background_labels=[]
    for name,data in data_dict.items():
        data=cut_data(data,cut)
        weights=data["weight"]
        if(DO_BDT):
            weights=weights*data["bdt_vbf"] *len(data) / np.sum(data["bdt_vbf"])
        back_hist=np.histogram(data[observable],bins=binning)[0]
        background+=back_hist
        background_list.append(back_hist)
        background_labels.append(name)
    return background,background_list,background_labels

def nominal_uncertainty(observables,process="vbf"):
    obs_dict={}
    vbf_data=get_tree(process,"nominal")
    for observable in observables:
        binning=np.array(bin_dict[my_naming[observable]])
        signal=np.histogram(vbf_data[observable],bins=binning)[0]
        obs_dict[observable]=np.sqrt(signal) / signal
    return obs_dict

def create_histogram(observables,systematic,processes=["top","diboson","Zjets"],mll_cuts=[70],do_stacked=False):
    vbf_data,data_dict=get_background_signal_data(systematic,processes)
    dict_of_variables={}
    for cut in mll_cuts:
        dict_of_variables[cut]={}
        for observable in observables:
            binning=np.array(bin_dict[my_naming[observable]])
            background_data,background_list,background_labels=df_to_histogram(observable,data_dict,binning,cut)
            vbf_data=cut_data(vbf_data,cut)
            weights=vbf_data["weight"]
            if(DO_BDT):
                weights=weights*vbf_data["bdt_vbf"]*len(vbf_data) / np.sum(vbf_data["bdt_vbf"])
            signal=np.histogram(vbf_data[observable],bins=binning)[0]
            if (do_stacked):
                stacked_plot(binning,signal,background_list,background_labels,title=f"{observable} {systematic}")
                pass
            dict_of_variables[cut][observable]={"signal":signal,"background":background_data,"background_dict":{label:hist for label,hist in zip(background_labels,background_list)}}
    del vbf_data
    data_dict.clear()
    del data_dict
    return dict_of_variables

def loop_systematics(observables):
    variation_dict={}
    nominal_data=create_histogram(observables,"nominal")
    for cut_value,mjj_cut in nominal_data.items():
        for ob_name,ob in mjj_cut.items():
            if ob_name in variation_dict:
                variation_dict[f"{ob_name}___{cut_value}"]["nominal"]={"signal":ob["signal"],"background":ob["background"]}
            else:
                variation_dict[f"{ob_name}___{cut_value}"]={"nominal":{"signal":ob["signal"],"background":ob["background"]}}

    for systematic in systematics:
        up_variation=create_histogram(observables,systematic+"__1up")
        down_variation=create_histogram(observables,systematic+"__1down")
        for cut_value,mjj_cut in up_variation.items():
            for ob_name,ob in mjj_cut.items():
                variation_dict[f"{ob_name}___{cut_value}"][systematic]={"up":{"signal":ob["signal"],"background":ob["background"]}}
                
        for cut_value,mjj_cut in down_variation.items():
            for ob_name,ob in mjj_cut.items():
                variation_dict[f"{ob_name}___{cut_value}"][systematic]["down"]={"signal":ob["signal"],"background":ob["background"]}

    total_syst={}
    for name, hist in variation_dict.items():
        print(hist.keys())
        nominal_signal=hist["nominal"]["signal"]
        nominal_background=hist["nominal"]["background"]
        pseudo_data=nominal_signal+nominal_background
        total_syst[name]={"nominal":nominal_signal,"up":np.zeros(len(nominal_signal)),"down":np.zeros(len(nominal_signal))}
        for systematic in systematics:
            print(hist[systematic].keys())
            up_variation=hist[systematic]["up"]["background"]
            down_variation=hist[systematic]["down"]["background"]
            up_estimate=pseudo_data-down_variation
            down_estimate=pseudo_data-up_variation
            obs_name,cut=name.split("___")
            if(np.sum(up_estimate) < np.sum(down_estimate)):
                up_estimate,down_estimate=down_estimate,up_estimate
            total_syst[name]["up"]+=(up_estimate-nominal_signal)**2
            total_syst[name]["down"]+=(down_estimate-nominal_signal)**2
            #plot_vbf(obs_name,np.array(bin_dict[my_naming[obs_name]]),nominal_signal,up_estimate,down_estimate,cut,systematic)
        total_syst[name]["up"]=np.sqrt(total_syst[name]["up"])
        total_syst[name]["down"]=np.sqrt(total_syst[name]["down"])
    return total_syst

def plot_signal_background(variables,cuts):
    vbf_data,data_dict=get_background_signal_data()
    for variable in variables:
        for cut in cuts:
            binning=np.array(bin_dict[my_naming[variable]])
            print(binning)
            x=(binning[:-1]+binning[1:]) /2
            xerr=(binning[:-1]-binning[1:]) /2
            b_s_ratio=np.zeros(len(x))
            vbf_data=cut_data(vbf_data,cut)
            for name,data in data_dict.items():
                data=cut_data(data,cut)
                weights=data["weight"]
                if(DO_BDT):
                    weights=weights*data["bdt_vbf"]*len(data) / np.sum(data["bdt_vbf"])
                b_s_ratio+=np.histogram(data[variable],bins=binning,weights=weights)[0]
            weights=vbf_data["weight"]
            if(DO_BDT):
                weights=weights*vbf_data["bdt_vbf"]* len(data) / np.sum(data["bdt_vbf"])
            signal=np.histogram(vbf_data[variable],weights=vbf_data["weight"],bins=binning)[0]+0.000001
            print(f"background {b_s_ratio}")
            print(f"signal {signal}")
            b_s_ratio=b_s_ratio/signal
            plt.errorbar(x/1000,b_s_ratio,xerr=xerr/1000,fmt="+",label=f"Mjj cut {cut}")
        plt.xlabel(f"{variable}")
        plt.legend()
        if not DO_BDT:
            plt.title(f"{variable} Background / signal ")
            plt.savefig(f"b_s_{variable}.pdf")
        else:
            plt.title(f"{variable} Background / signal ")
            plt.savefig(f"mjj_cut_{cut}_b_s_{variable}_bdt.pdf")
        plt.cla()
        plt.close()

            #plt.show()
    #plt.savefig(f"mjj_cut_{mjj_cut}_b_s_{variable}.pdf")
    #plt.cla()
    #plt.close()


def main():
    global DO_BDT
    DO_BDT=False
    no_bdt_syst_uncert_data=loop_systematics(unfold_obs)
    bdt_syst_uncert_data=loop_systematics(unfold_obs)    
    for (bdt_name,bdt_histogram),(no_bdt_name,no_bdt_histogram) in zip(bdt_syst_uncert_data.items(),no_bdt_syst_uncert_data.items()):
        obs,mjj_cut=bdt_name.split("___")
        binning=np.array(bin_dict[my_naming[obs]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[:-1]-binning[1:])/2
        bdt_nominal=bdt_histogram["nominal"]
        bdt_down=bdt_nominal-bdt_histogram["down"]
        bdt_up=bdt_nominal+bdt_histogram["up"]
        no_bdt_nominal=no_bdt_histogram["nominal"]
        no_bdt_down=no_bdt_nominal-no_bdt_histogram["down"]
        no_bdt_up=no_bdt_nominal+no_bdt_histogram["up"]
        nominal_ratio=bdt_nominal/no_bdt_nominal
        up_ratio=bdt_up/no_bdt_up
        down_ratio=bdt_down/no_bdt_down
        plt.errorbar(x,bdt_nominal,xerr=xerr,fmt="g+",label="bdt nominal")
        plt.errorbar(x,no_bdt_nominal,xerr=xerr,fmt="g*",label="no bdt nominal")
        plt.errorbar(x,bdt_up,xerr=xerr,fmt="b+",label="bdt up")
        plt.errorbar(x,no_bdt_up,xerr=xerr,fmt="b*",label="no bdt up")
        plt.errorbar(x,bdt_down,xerr=xerr,fmt="r+",label="bdt down")
        plt.errorbar(x,no_bdt_down,xerr=xerr,fmt="r*",label="no bdt down")
        plt.title(f"{obs}")
        plt.legend()
        plt.savefig(f"{obs}_all_variations.pdf")
        plt.close()
        plt.errorbar(x,nominal_ratio,xerr=xerr,fmt="g+",label="nominal bdt /no bdt")
        plt.errorbar(x,up_ratio,xerr=xerr,fmt="b+",label="up var bdt / no bdt")
        plt.errorbar(x,down_ratio,xerr=xerr,fmt="r+",label="down var  bdt / no bdt")
        plt.title(f"{obs} BDT/no BDT weighted Ratio")
        plt.legend()
        plt.savefig(f"{obs}_bdt_no_bdt_ratio.pdf")
        plt.close()
        '''
        ratio of bdt / no bdt
        stat=np.sqrt(nominal_signal)
        plt.errorbar(x,nominal_signal,xerr=xerr,yerr=stat,fmt="g+",label="stat. uncertainty")
        plt.fill_between(x,nominal_signal+up,nominal_signal-down,alpha=0.5,linewidth=2,edgecolor="k",linestyle='--',label="total systematic")
        plt.title(f"{obs} systematic ratio")
        is_bdt="no_bdt"
        if DO_BDT:
           is_bdt="bdt"
        plt.legend()
        plt.show()
        plt.savefig(f"{obs}_systematics_ratio_{is_bdt}.pdf")
        plt.cla()
        plt.close()
        '''
        '''
        #ratio of up / nominal for bdt and no bdr
        plt.errorbar(x,no_bdt_up/no_bdt_nominal,xerr=xerr,fmt="b+",label="up /nominal")
        plt.errorbar(x,no_bdt_down/no_bdt_nominal,xerr=xerr,fmt="r+",label="down /nominal")
        plt.title(f"{obs} Systematic ratio No BDT")
        plt.legend()
        plt.savefig(f"{obs}_systematics_ratio_no_bdt.pdf")
        plt.cla()
        plt.close()
        
        plt.errorbar(x,bdt_up/bdt_nominal,xerr=xerr,fmt="b+",label="up /nominal")
        plt.errorbar(x,bdt_down/bdt_nominal,xerr=xerr,fmt="r+",label="down /nominal")
        plt.title(f"{obs} Systematic ratio With BDT Weights")
        plt.legend()
        plt.savefig(f"{obs}_systematics_ratio_bdt.pdf")
        plt.cla()
        plt.close()
        '''
        """
        #percentage error
        plt.errorbar(x,((up+down)/2)/nominal_signal,xerr=xerr,fmt="g+",label="stat. uncertainty")
        plt.title(f"{obs} Systematic/Bin Content Mjj Cut={mjj_cut}")
        if DO_BDT:
            plt.savefig(f"{obs}_error_percent_mjjcut_{mjj_cut}_bdt.pdf")
        else:
            plt.savefig(f"{obs}_error_percent_mjjcut_{mjj_cut}_nobdt.pdf")
        plt.cla()
        plt.close()
        """
def plot_effeciencies(observable,comp):
    vbf_data,data_dict=get_background_signal_data()
    cuts={"Mjj":np.linspace(200000,1000000,200),"Mll":np.linspace(0,100000,200),"DYjj":np.linspace(2.1,3.0,200)}

    signal_effeciency=[effeciency(observable,vbf_data,cut,comp) for cut in cuts[observable]]
    plt.plot(cuts[observable],signal_effeciency,label="vbf")
    back_eff={}
    for name,data in data_dict.items():
        back_eff[name]=[effeciency(observable,data,cut,comp) for cut in cuts[observable]]
        plt.plot(cuts[observable],back_eff[name],label=name)
    plt.title(f"{observable} Cut Effeciencies")
    plt.legend()
    plt.savefig(f"{observable}_cut_effeciency.pdf")
    plt.cla()
    plt.close()

def plot_2d_eff():
    vbf_data,data_dict=get_background_signal_data()
    cuts={"DYjj":np.linspace(2.1,3.1,11),"jet0_pt":np.linspace(30000,70000,11)}
 

    plt.xticks(np.linspace(0,10,11),[round(i,1) for i in cuts["DYjj"]])
    plt.yticks(np.linspace(0,10,11),[int(i/1000) for i in cuts["jet0_pt"]])
    plt.ylabel("jet0_pt Cut")
    plt.xlabel("DYjj Cut")
    signal=np.empty((len(cuts["jet0_pt"]),len(cuts["DYjj"])))
    background=np.zeros((len(cuts["jet0_pt"]),len(cuts["DYjj"])))
    for i in range(len(cuts["jet0_pt"])):
        for j in range(len(cuts["DYjj"])):
            signal_num=np.sum(vbf_data[(vbf_data["DYjj"] > cuts["DYjj"][j]) & (vbf_data["jet0_pt"] > cuts["jet0_pt"][i])]["weight"])
            signal[i,j]=signal_num

    total_background=0
    for name,data in data_dict.items():
        total_background+=np.sum(data["weight"])
        for i in range(len(cuts["jet0_pt"])):
            for j in range(len(cuts["DYjj"])):
                background[i,j]+=np.sum(data[(data["DYjj"] > cuts["DYjj"][j]) & (data["jet0_pt"] > cuts["jet0_pt"][i])]["weight"])

    signal_eff=signal/np.sum(vbf_data["weight"])
    back_eff=background/total_background
    back_sig_ratio=signal / background

    plt.imshow(signal_eff)
    label_matrix((signal_eff*100).astype(int))
    plt.title(f"Signal Effeciency ")
    plt.show()
    #plt.savefig(f"background_effeciency_mjj_cut_{mjj_cut}.pdf")
    plt.cla()
    plt.close()
        
def bdt_observable_bias(observables):
    vbf_data,data_dict=get_background_signal_data()
    for observable in observables:
        obs_binning=np.array(bin_dict[my_naming[observable]])
        bdt_binning=np.linspace(0,1.0,10)
        plt.hist2d(vbf_data[observable],vbf_data["bdt_vbf"],bins=[obs_binning,bdt_binning])
        plt.title(f"VBF  BDT vs. {observable}")
        plt.xlabel(f"{observable}")
        plt.ylabel("bdt_vbf")
        plt.savefig(f"vbf_bdt_vs_{observable}.pdf")
        plt.cla()
        plt.close()
        for name,data in data_dict.items():
            plt.hist2d(data[observable],data["bdt_vbf"],bins=[obs_binning,bdt_binning])
            plt.title(f"{name}  BDT vs. {observable}")
            plt.xlabel(f"{observable}")
            plt.ylabel("bdt_vbf")
            plt.savefig(f"{name}_bdt_vs_{observable}.pdf")
            plt.cla()
            plt.close()

def compare_bdt_skew(observables):
    vbf_data,data_dict=get_background_signal_data()
    vbf_data=cut_data(vbf_data,0)
    for obs in observables:
        binning=np.array(bin_dict[my_naming[obs]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[1:]-binning[:-1])
        no_bdt_data=np.histogram(vbf_data[obs],bins=binning,weights=vbf_data["weight"])[0]
        bdt_data=np.histogram(vbf_data[obs],bins=binning,weights=vbf_data["weight"]*vbf_data["bdt_vbf"]*len(vbf_data) / np.sum(vbf_data["bdt_vbf"]))[0]
        plt.bar(x,no_bdt_data,width=xerr,yerr=np.sqrt(no_bdt_data),label="no bdt",capsize=5)
        plt.bar(x,bdt_data,width=xerr,label="with bdt",alpha=0.5)

        plt.legend()
        plt.title(f"{obs}")
        plt.savefig(f"{obs}_vbf_bdt_skew_comparison.pdf")
        plt.cla()
        plt.close()
        '''
        plt.cla()
        plt.close()
        plt.errorbar(x,no_bdt_data/bdt_data,xerr=xerr,fmt="+")
        plt.title(f"vbf {obs} no bdt / bdt ratio")
        plt.savefig(f"bdt_skew_vbf_{obs}.pdf")
        plt.cla()
        plt.close()
        '''
        '''
        for name,data in data_dict.items():
            data=cut_data(data,0)
            no_bdt_data=np.histogram(data[obs],bins=binning,weights=data["weight"])[0]
            bdt_data=np.histogram(data[obs],bins=binning,weights=data["weight"]*data["bdt_vbf"])[0]
            plt.errorbar(x,no_bdt_data/bdt_data,xerr=xerr,fmt="+")
            plt.title(f"{name} {obs} no bdt /BDT ratio")
            plt.savefig(f"bdt_ratio_{name}_{obs}.pdf")
            plt.cla()
            plt.close()
        '''

def variation_by_systematic(observables,systematics=systematics,processes=["top","diboson","Zjets"]):
    variation_dict={}
    print(processes)
    nominal_data=create_histogram(observables,"nominal",processes)
    for cut_value,mjj_cut in nominal_data.items():
        for ob_name,ob in mjj_cut.items():
            if ob_name in variation_dict:
                variation_dict[f"{ob_name}___{cut_value}"]["nominal"]={"signal":ob["signal"],"background":ob["background"],"background_dict":ob["background_dict"]}
            else:
                variation_dict[f"{ob_name}___{cut_value}"]={"nominal":{"signal":ob["signal"],"background":ob["background"],"background_dict":ob["background_dict"]}}

    for systematic in systematics:
        try:
            up_variation=create_histogram(observables,systematic+"__1up",processes)
            down_variation=create_histogram(observables,systematic+"__1down",processes)
            for cut_value,mjj_cut in up_variation.items():
                for ob_name,ob in mjj_cut.items():
                    variation_dict[f"{ob_name}___{cut_value}"][systematic]={"up":{"signal":ob["signal"],"background":ob["background"],"background_dict":ob["background_dict"]}}
                
            for cut_value,mjj_cut in down_variation.items():
                for ob_name,ob in mjj_cut.items():
                    variation_dict[f"{ob_name}___{cut_value}"][systematic]["down"]={"signal":ob["signal"],"background":ob["background"],"background_dict":ob["background_dict"]}
        except:
            print(f"could not fine {systematic}")
        all_variations={}
    for name, hist in variation_dict.items():
        print(hist.keys())
        nominal_signal=hist["nominal"]["signal"]
        nominal_background=hist["nominal"]["background"]            
        pseudo_data=nominal_signal+nominal_background
        obs_name,cut=name.split("___")
        all_variations[obs_name]={"nominal":{"signal":nominal_signal}}
        for sample,sample_data in hist["nominal"]["background_dict"].items():
            all_variations[obs_name]["nominal"][sample]=sample_data
        for systematic in systematics:
            print(hist[systematic].keys())
            up_variation=hist[systematic]["up"]["background"]
            down_variation=hist[systematic]["down"]["background"]
            up_estimate=pseudo_data-down_variation
            down_estimate=pseudo_data-up_variation
            all_variations[obs_name][systematic]={"up":{"up_signal":up_estimate},"down":{"down_signal":down_estimate}}
            for sample,sample_data in hist[systematic]["up"]["background_dict"].items():
                all_variations[obs_name][systematic]["up"][sample]=sample_data
            for sample,sample_data in hist[systematic]["down"]["background_dict"].items():
                all_variations[obs_name][systematic]["down"][sample]=sample_data

    return all_variations

def plot_variations_by_syst(observables,systematics,processes=backgrounds):
    variation_data=variation_by_systematic(observables,systematics,processes)
    #averages_file=open("sytematics_ratio_means.txt","a")
    #plot up variations
    data_by_process={obs:{} for obs in observables}
    uncertainties={}
    for proc in processes:
        uncertainties[proc]=nominal_uncertainty(observables,proc)
    for name,obs in variation_data.items():
        binning=np.array(bin_dict[my_naming[name]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[:-1]-binning[1:])/2
        stat_y=np.zeros(len(x)+1)
        #stat_y[:-1]=uncertainty[name]*obs["nominal"]["signal"]
        stat_y[-1]=stat_y[-2]
        for syst_name,syst in obs.items():
            if syst_name=="nominal":
                continue

            '''
            up_ratio=syst["up"]["up_signal"]/obs["nominal"]
            down_ratio=syst["down"]["down_signal"]/obs["nominal"]
            plt.errorbar(x,up_ratio,xerr=xerr,fmt="+",label=syst_name+" up / nominal")
            plt.errorbar(x,down_ratio,xerr=xerr,fmt=".",label=syst_name+" down / nominal")
            plt.errorbar(x,np.ones(len(x),xerr=xerr,yerr=uncertainty[name],fmt="k,",label="MC uncertinaty")
            plt.title(f"{name}_{processes[0]}")
            plt.legend()
            plt.savefig(f"{name}_{syst_name}_{processes[0]}_ratios.pdf")
            plt.cla()
            plt.close()
            '''
            '''
            data_by_process[name][syst_name]={"nominal":obs["nominal"]["signal"],"up":syst["up"]["up_signal"],"down":syst["down"]["down_signal"]}
            plt.errorbar(x,syst["up"]["up_signal"],xerr=xerr,fmt=",",label=syst_name+" up")
            plt.errorbar(x,syst["down"]["down_signal"],xerr=xerr,fmt=".",label=syst_name+" down")
            plt.errorbar(x,obs["nominal"]["signal"],xerr=xerr,capsize=5,fmt="k*",label="nominal,")
            #plt.fill_between(binning,np.zeros(len(binning))+stat_y,np.zeros(len(binning))-stat_y,alpha=0.5,linewidth=2,edgecolor="k",linestyle='--',label="statistical uncertainty (MC)")
            plt.title(f"{name} {syst_name} {processes[0]} signal estimate")
            plt.legend()
            plt.savefig(f"{name}_{syst_name}_{processes[0]}_syst_signal_ratio.pdf")
            plt.cla()
            plt.close()
            '''
            
            for back_name in processes:
                if back_name not in ("Zjets","diboson","top"):
                    continue
                nominal=obs["nominal"][back_name]
                up_variation=syst["up"][back_name]
                down_variation=syst["down"][back_name]
                up_ratio=up_variation /nominal
                down_ratio=down_variation /nominal
                up_vbf=syst["up"]["up_signal"]
                down_vbf=syst["down"]["down_signal"]
                
                plt.errorbar(x,up_ratio,xerr=xerr,fmt="+",label="up")
                plt.errorbar(x,down_ratio,xerr=xerr,fmt="+",label="down")
                plt.errorbar(x,np.ones(len(x)),xerr=xerr,yerr=uncertainties[back_name][name],fmt="k,",label="MC uncertinaty")
                plt.ylabel("variation  / nominal")
                plt.title(f"{name} {syst_name} {back_name} ratios")
                plt.legend()
                plt.savefig(f"{name}_{syst_name}_{back_name}_mc_ratios_no_weights.pdf")
                plt.cla()
                plt.close()

                '''
                plt.errorbar(x,nominal,xerr=xerr,yerr=nominal*uncertainties[back_name][name],fmt=",",label="nominal",capsize=3)
                plt.errorbar(x,up_variation,xerr=xerr,fmt=".",label="up ratio")
                plt.errorbar(x,down_variation,xerr=xerr,fmt=".",label="down ratio")
              
                plt.ylabel("Events")
                plt.title(f"{name} {syst_name} {back_name} ")
                plt.legend()
                plt.savefig(f"{name}_{syst_name}_{back_name}_with_weights_total_events.pdf")
                plt.cla()
                plt.close()
                '''
    #averages_file.close()
    return data_by_process
def syst_contributions():
    #nominal
    vbf_nominal,back_nominal=get_background_signal_data()
    nominal_data=np.sum(vbf_nominal["weight"])
    nominal_events={"vbf":np.sum(vbf_nominal["weight"])}
    for name,data in back_nominal.items():
        nominal_events[name]=np.sum(data["weight"])
    del vbf_nominal
    back_nominal.clear()
    del back_nominal
    #all systematics
    syst_list=get_all_systematics()
    up_systematics={"systematic":[],"top":[],"diboson":[],"vh":[],"Vgamma":[],"Zjets":[],"htt":[],"ggf":[]}
    down_systematics={"systematic":[],"top":[],"diboson":[],"vh":[],"Vgamma":[],"Zjets":[],"htt":[],"ggf":[]}
    for syst in syst_list:
        try:
            vbf_up,back_up=get_background_signal_data(syst+"__1up")
            vbf_down,back_down=get_background_signal_data(syst+"__1down")
            up_systematics["systematic"].append(syst)
            down_systematics["systematic"].append(syst)
            for name_up,data_up in back_up.items():
                down_systematics[name_up].append(round(nominal_events[name_up]-np.sum(data_up["weight"]),3))
            for name_down,data_down in back_down.items():
                up_systematics[name_down].append(round(nominal_events[name_down]-np.sum(data_down["weight"]),3))
            
            del vbf_up
            del vbf_down
            back_up.clear()
            back_down.clear()
            del back_up
            del back_down
        except Exception as e:
            print(f"{syst} failed")
            print(e)
    up_data=pd.DataFrame(up_systematics)
    down_data=pd.DataFrame(down_systematics)
    up_data.to_csv("up_systematics_contribution.csv")
    down_data.to_csv("down_systematics_contribution.csv")

def find_negative_bins(obs):
    systematics=get_all_large_systs()
    variations=variation_by_systematic(obs,systematics=systematics,processes=backgrounds)
    negative_systs=[]
    for observable,data in variations.items():
        for name,syst in data.items():
            if 'nominal' in name:
                continue
            if np.any(syst["up"] < 0):
                negative_systs.append((observable,"up",name))
            elif np.any(syst["down"] < 0):
                negative_systs.append((observable,"down",name))
            print(observable,name)
            print(syst["up"])
            print(syst["down"])
    print(negative_systs)
    print("-------------------------------------------")
    for i in negative_systs:
        print(i)
        
def weight_investigation(observables,processes,systematic):
    weight_data={obs:{} for obs in observables}
    for proc in processes:
        vbf,back=get_background_signal_data(f"{systematic}",processes=[proc])
        for obs in observables:
            binning=np.array(bin_dict[my_naming[obs]])
            x=(binning[:-1] + binning[1:]) /2
            xerr=(binning[1:] - binning[:-1]) /2
            weighted_hist_data=np.histogram(back[proc][obs],bins=binning,weights=back[proc]["weight"])[0]
            unweighted_hist_data=np.histogram(back[proc][obs],bins=binning)[0]
            weights_ratio=weighted_hist_data / unweighted_hist_data
            weight_data[obs][proc]=weights_ratio
            
    for obs,obs_data in weight_data.items():
        binning=np.array(bin_dict[my_naming[obs]])
        x=(binning[:-1] + binning[1:]) /2
        xerr=(binning[1:] - binning[:-1]) /2
        for proc,proc_data in obs_data.items():
            plt.errorbar(x,proc_data,xerr=xerr,fmt=",",label=f"{proc}")
        plt.legend()
        plt.title(f"{obs} Average Weight Per Bin")
        plt.savefig(f"{obs}_{systematic}_weight_per_bin.pdf")
        plt.cla()
        plt.close()

def weights_per_bin(observables,systematic=systematics[0]):
     vbf_nom,Zjets_nom=get_background_signal_data("nominal",processes=["Zjets"])     
     zjets=Zjets["Zjets"]
     for obs in observables:
         binning=np.array(bin_dict[my_naming[obs]])
         x=(binning[:-1] + binning[1:]) /2
         xerr=(binning[1:] - binning[:-1]) /2
         for i,b in enumerate(x):
             zjets_bin=zjets_nom[(zjets[obs] > (x[i]-xerr[i])) & (zjets[obs] < (x[i]+xerr[i]))]
             plt.plot(zjets_bin[obs],zjets_bin["weight"],".")
             plt.xlabel(obs)
             plt.ylabel("weights")
             plt.title(f"Zjets weight vs {obs}, Bin {i}")
             plt.show()
def var_per_bin(observables,systematic=systematics[0]):
     vbf_nom,Zjets_nom=get_background_signal_data("nominal",processes=["Zjets"])
     vbf_up,Zjets_up=get_background_signal_data(systematic+"__1up",processes=["Zjets"])
     vbf_down,Zjets_down=get_background_signal_data(systematic+"__1down",processes=["Zjets"])    
     zjets_nom=Zjets_nom["Zjets"]
     zjets_up=Zjets_up["Zjets"]
     zjets_down=Zjets_down["Zjets"]
     for obs in observables:
         binning=np.array(bin_dict[my_naming[obs]])
         x=(binning[:-1] + binning[1:]) /2
         xerr=(binning[1:] - binning[:-1]) /2
         for i,b in enumerate(x):
             zjets_nom_bin=zjets_nom[(zjets_nom[obs] > (x[i]-xerr[i])) & (zjets_nom[obs] < (x[i]+xerr[i]))]
             zjets_up_bin=zjets_up[(zjets_up[obs] > (x[i]-xerr[i])) & (zjets_up[obs] < (x[i]+xerr[i]))]
             if(len(zjets_nom_bin) < 10):
                 continue
             plt.plot(zjets_nom_bin[obs],zjets_nom_bin["weight"],".",label="nominal")
             plt.plot(zjets_up_bin[obs],zjets_up_bin["weight"],".",label="up variation",alpha=0.5)
             plt.legend()
             plt.xlabel(obs)
             plt.ylabel("weights")
             plt.title(f"Zjets weight vs {obs}, Bin {i} {systematic}")
             plt.savefig(f"var_weights_outliers_{obs}_Bins{i}_up.pdf")
             plt.cla()
             plt.close()
def remove_outliers(data):
    #lof= LocalOutlierFactor()
    #outlier_weights=lof.fit_predict(data["weight"].values.reshape(-1, 1))
    outlier_weights=data["weight"] < 10
    return data[outlier_weights]
    
def show_remove_outliers():
    obs="Mjj"
    data=get_tree("Zjets","nominal")
    no_out=remove_outliers(data)
    binning=np.array(bin_dict[my_naming[obs]])
    x=(binning[:-1] + binning[1:]) /2
    xerr=(binning[1:] - binning[:-1]) /2
    plt.hist(data[obs],bins=binning,weights=data["weight"],label=f"w/ outlier weights")
    plt.hist(no_out[obs],bins=binning,weights=no_out["weight"],label=f"w/o outlier weights",alpha=0.5)
    plt.show()

def compare_entries():
    processes=["Zjets","top","diboson"]
    enries_dict={"systematic":[],"top":[],"Zjets":[],"diboson":[]}
    root_file=f"{ntuple_directory}/c16a/{process}.root"
    
    rootfile=uproot.open(root_file)
    nominal_n=uproot.tree.numentries(root_file,f"{process}_nominal")
    for tree_name in rootfile.keys():
        nentries=uproot.tree.numentries(root_file,tree_name)
        print(f"Syst {tree_name}  : entries: {nentries/nominal_n}")
if __name__=="__main__":
    #find_negative_bins(unfold_obs)
    #nominal=create_histogram(unfold_obs,"nominal",processes=backgrounds,do_stacked=True)
    #for i in range(1,12):
    #    systematic=f"JET_JER_EffectiveNP_{i}"
    #    up_variation=create_histogram(unfold_obs,systematic+"__1up",processes=backgrounds,do_stacked=True)
    #    down_variation=create_histogram(unfold_obs,systematic+"__1down",processes=backgrounds,do_stacked=True)
    #print(syst_contributions())
    #create_histogram(["MT"],"nominal",backgrounds)
    #mt_effeciency()
    #plot_variations_by_syst(unfold_obs,["top"])
    '''
    data_by_process={}
    uncert={}
    test_systs=["FT_EFF_Eigen_B_0","EL_EFF_ID_SIMPLIFIED_UncorrUncertaintyNP0","MUON_EFF_ISO_STAT","MUON_SAGITTA_RESBIAS","EL_EFF_ID_CorrUncertaintyNP0"]
    for proc in ("Zjets","top","diboson"):
        data_by_process[proc]=plot_variations_by_syst(["Mjj"],test_systs,[proc])
        uncert[proc]=nominal_uncertainty(["Mjj"],proc)
    '''
    '''
    reorg_data={obs:{syst:{proc:{} for proc in ("Zjets","top","diboson") } for syst in systematics[:1]} for obs in jet_obs}
    for proc,data in data_by_process.items():
        for obs_name,obs_data in data.items():
            for syst_name,syst_data in obs_data.items():
                reorg_data[obs_name][syst_name][proc]={"nominal":syst_data["nominal"],"up":syst_data["up"],"down":syst_data["down"]}

            
    for obs_name,obs_data in reorg_data.items():
        binning=np.array(bin_dict[my_naming[obs_name]])
        x=(binning[:-1]+binning[1:]) /2
        xerr=(binning[:-1]-binning[1:])/2
        for syst_name,syst_data in obs_data.items():
            for proc_name,proc_data in syst_data.items():
                plt.errorbar(x,proc_data["up"]/proc_data["nominal"],xerr=xerr,fmt="+",label=f"{proc_name} ratio")
                #plt.errorbar(x,np.ones(len(x)),yerr=uncert[proc_name][obs_name],fmt="k,",capsize=3,label="stat. uncert")
            plt.title(f"{obs_name} {syst_name} up syst ratios")
            plt.legend()
            plt.savefig(f"process_comparsons_{syst_name}_{obs_name}_up.pdf")
            plt.cla()
            plt.close()
    '''
    #bdt_observable_bias(["DYll","DYjj"])
    #main()
    #DO_BDT=True
    #main()
    #plot_signal_background(unfold_obs+["Mjj"],[350,450,700])
    #plot_effeciencies("DYjj",gt)
    #plot_2d_eff()
    #mtt_effeciency()
    #jet0_effeciency()
    #plot_2d_eff()
    #weight_investigation(jet_obs,["Zjets","top","diboson"],"nominal")
    #weight_investigation(jet_obs,["Zjets","top","diboson"],systematics[0]+"__1up")
    #weight_investigation(jet_obs,["Zjets","top","diboson"],systematics[0]+"__1down")
    #weights_per_bin(jet_obs)
    #var_per_bin(jet_obs)
    #show_remove_outliers()
    #jet0_effeciency()
    compare_entries("top")
