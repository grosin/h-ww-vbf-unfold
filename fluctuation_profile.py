import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import analyze_matrices as am
import os
import sys
from binnings import get_bin_dict
from matplotlib import gridspec
from scipy.interpolate import interp1d


def get_name(syst):
    st_name=syst.split(":")
    return st_name[0]

def up_or_down(syst):
    st_name=syst.split(":")
    if(len(st_name) > 1):
        return st_name[1]
    else:
        return "nominal"
    
class color_cycle:
    def __init__(self):
        self.color_wheel=['r', 'c', 'm', 'y', 'k','b','g']
        self.current_color=0

    def get_color(self):
        color=self.color_wheel[self.current_color]
        self.current_color+=1
        self.current_color=self.current_color % len(self.color_wheel)
        return color

    def next(self):
        self.current_color+=1
        self.current_color=self.current_color % len(self.color_wheel)

    def get_color_no_rotation(self):
        return self.color_wheel[self.current_color]
    
    def reset(self):
        self.current_color=0

        

def read_binning(binning_file):
    binning={}
    with open(binning_file) as bfile:
        for line in bfile:
            name,bins=line.split(":")
            name=name.replace("_binning","")
            bin_contents=[float(i) for i in bins.split(",")]
            binning[name]=bin_contents
    return binning

def fit_and_draw_gaus(x,y):
    gaus=lambda x,mu,sig,N : N*np.exp(-(x-mu)**2 / sig**2)
    results,cov=curve_fit(gaus,x,y,[np.mean(x),np.std(x),1],maxfev = 5000)
    x_data=np.linspace(x[0],x[-1],100)
    plt.plot(x_data,gaus(x_data,*results),"b--",label="gaussian fit")
    
def twoD_to_oneD(hist,y_edges,x_edges,name):
    x_bins=(x_edges[:-1]+x_edges[1:]) /2
    y_bins=(y_edges[:-1]+y_edges[1:]) /2
    for i in range(hist.shape[0]):
       plt.plot(y_bins,hist[i,:],"r+",label=f"{round(x_bins[i],2)} variation")
       try:
           fit_and_draw_gaus(y_bins,hist[i,:])
       except:
           pass
       plt.title(name)
       plt.legend()
       plt.show()
       
def profile_histogram(hist,y_edges,x_edges,it,draw=True):
    y_bins=(y_edges[:-1]+y_edges[1:])/2
    x_bins=(x_edges[:-1]+x_edges[1:])/2
    profile_means=np.empty(hist.shape[0])
    profile_std=np.empty(hist.shape[0])
    for i in range(hist.shape[0]):
        averages=[]
        bin_sum=0
        for j in range(hist.shape[1]):
            averages.append(hist[i,j]*y_bins[j])
            bin_sum+=hist[i,j]
        if bin_sum ==0:
            profile_means[i]=0
            profile_std[i]=0
        else:
            profile_means[i]=np.sum(averages)/bin_sum
            profile_std[i]=np.std([q/bin_sum for q in averages])
    #results,cov=curve_fit(lambda x,a,b: a*x+b,x_bins,profile_means,p0=[1,0],sigma=profile_std,maxfev=2000)
    color=cycler.get_color()
    #plt.plot(x_bins,results[0]*np.array(x_bins)+results[1],":",color=color)
    if(draw):
        plt.errorbar(x_bins,profile_means,yerr=profile_std,fmt=".",color=color,capsize=3,label=f"{it} iterations")
    return (x_bins,profile_means,profile_std)

cycler=color_cycle()
bin_contents_dict=read_binning("textfiles/binning_nominal.txt")
matrices=am.load_matrices("textfiles/response_matrix.txt")
binnings=get_bin_dict()
name_dict={"higgs_pt":"$H\ P_{t}$",
           "MTll":"$MT^{ll}$",
           "ptTot":"$Total\ P_{t}$",
           "Ptll":"$Pt_{ll}$",
           "lep_pt_0":"$Leading\  Lep\ P_{t}$",
           "lep_pt_1":"$Subleading\ Lep\ P_{t}$",
           "DYll":r"$\Delta Y_{ll}$",
           "DPhill":"$\Delta \phi _{ll}$",
           "cosTheta":"$Cos(\Theta^{*})$",
           "Mjj":"$M_{jj}$",
           "jet_pt_0":"$Leading\ Jet\ P_{t}$",
           "jet_pt_1":"$Subleading\ Jet\ P_{t}$",
           "DYjj":"$\Delta Y_{jj}$",
           "Mll":"$M_{ll}$"
           }
class observable:
    def __init__(self,name):
        print(f"initializing observable {name}")
        self.iterations=9
        self.name=name
        self.title=name_dict[name]
        self.bin_contents=bin_contents_dict[name]
        self.bin_contents[0]+=0.0000001
        self.stat_file=f"t_{name}_statistical1.0.txt"
        self.bias_file=f"t_{name}_bias1.0.txt"
        self.coverage_file=f"t_{name}_coverage.txt"
        self.bins=np.array(binnings[f"{name}"])
        self.units=""
        self.divide=1
        if(self.bins[-1] > 1000):
            self.units="[GeV]"
            self.divide=1000
        names=["dist","iteration"]
        for i in range(len(self.bin_contents)):
            names.append(f"BinError{i}")
            names.append(f"TruthBin{i}")
        names.append("chi2")
        try:
            self.bias_data=pd.read_csv(self.bias_file,names=names)
        except:
            print("no bias data")
        try:
            self.stat_data=pd.read_csv(self.stat_file,names=names)
        except:
            print("no stats data")
        try:
            self.coverage_data=pd.read_csv(self.coverage_file)
        except:
            print("no coverage data")
        syst_data=pd.read_csv("Mjj_systematics_comparison.csv")
        syst_data["systematic_name"]=syst_data["systematic"].apply(get_name)
        syst_data["variation"]=syst_data["systematic"].apply(up_or_down)
        syst_data.drop(["systematic"],inplace=True,axis=1)
        self.syst_data=syst_data

            
    def draw_histograms(self):
        for i in range(1,len(self.bin_contents)):
            if self.bin_contents[i] == 0:
                continue
            for it in range(self.iterations):
                try:
                    iter_data=self.bias_data.loc[self.bias_data["iteration"]==it]
                    h,x_edges,y_edges=np.histogram2d((iter_data[f"TruthBin{i}"]-self.bin_contents[i])/np.sqrt(self.bin_contents[i]+0.0001),(iter_data[f"BinError{i}"])/iter_data[f"TruthBin{i}"],bins=10)
                    profile_histogram(h,y_edges,x_edges,it)
                except Exception as e:
                    print(e)
                    break
            cycler.reset()
            plt.legend()
            plt.title(f"Truth fluctuation vs. Unfolding Error {self.title} Bin {i}")
            plt.xlabel("Truth fluctuation / sqrt(n)")
            plt.ylabel("Unfolding error / truth events")
            #plt.savefig(f"{self.title}_profiles_bin{i}.pdf")
            plt.show()
            plt.cla()
            plt.close()

    def draw_2dhist(self):
        for i in range(1,len(self.bin_contents)):
            if self.bin_contents[i] == 0:
                continue
            for it in range(1,4):
                iter_data=self.bias_data.loc[self.bias_data["iteration"]==it]
                h,xedges,yedges,img=plt.hist2d(iter_data[f"BinError{i}"]+iter_data[f"TruthBin{i}"],iter_data[f"TruthBin{i}"],bins=10,cmap=plt.cm.BuPu)
                xmeans,ymeans,stds=profile_histogram(h,yedges,xedges,1,False )
                y=(yedges[:-1]+yedges[1:])/2
                error=stds
                plt.fill_between((xedges[1:]+xedges[:-1])/2, ymeans-error, ymeans+error,facecolor='c',alpha=0.5,linewidth=2,edgecolor="k",linestyle='--' ,label="0.68%")
                plt.legend()
                plt.title(f"True Cross Section vs. Fitted {self.title} Bin {i}")
                plt.xlabel("Fitted Cross Section")
                plt.ylabel("True cross section")
                plt.savefig(f"{self.title}_coverage_bin{i}_iteration_{it}.pdf")
                plt.cla()
                plt.close()

    def bin_level_histogram(self):
        total_means=np.zeros(7)
        iterations=np.arange(7)
        for i in range(1,len(self.bin_content)):
            if self.bin_content == 0:
                continue
            means=[]
            stds=[]
            for it in range(0,7):
                iter_data=self.bias_data.loc[self.bias_data["iteration"]==it]
                means.append(abs(np.mean(iter_data[f"BinError{i}"]))/self.bin_content[i])
                stds.append(np.std(iter_data[f"BinError{i}"]))
                total_means[it]+=abs(np.mean(iter_data[f"BinError{i}"]))
            plt.plot(iterations,means,"b.--")
            plt.show()
        plt.plot(iterations,total_means)
        plt.show()

    def gaussians(self):
        for i in range(len(self.bin_contents)):
            if(self.bin_contents[i]==0):
                continue
            for it in range(0,5):
                data=self.bias_data.loc[self.bias_data["iteration"]==it]
                std=np.std(data)
                plt.hist(data[f"TruthBin{i}"],bins=20,label=f"{it} Iterations",histtype=u'step')
            
            plt.legend()
            plt.xlabel("Unfolded Distribution")
            plt.title(f"{self.title} Bin {i}")
            #plt.show()
            plt.savefig(f"{self.title}_bin_{i}.pdf")
            plt.cla()
            plt.close()

    def coverage_hist1d(self):
        for i in range(len(self.bin_contents)):
            if(self.bin_contents[i]==0):
                continue
            for it in range(0,1):
                color=cycler.get_color()
                data=self.coverage_data.loc[self.coverage_data["iteration"]==it]
                std=np.std(data)
                n, bins, patches=plt.hist(data[f"BinError{i}"]/data[f"TruthBin{i}"],bins=20,histtype=u'step',color=color)
                gaus=lambda x,mu,sig,N : N*np.exp(-(x-mu)**2 /(2* sig**2))
                x=(bins[:-1]+bins[1:]) /2
                results,cov=curve_fit(gaus,x,n,[np.mean(x),np.std(x),1],maxfev = 5000)
                plt.plot(x,gaus(x,*results),label=f"Iterations {it},mean={round(results[0],1)},std={round(results[1],2)}",color=color)
            plt.legend()
            plt.xlabel("Unfolded - Truth / Truth")
            plt.title(f"Coverage Test{self.title} Bin {i}")
            #plt.show()
            plt.savefig(f"{self.title}_bin_{i}.pdf")
            plt.cla()
            plt.close()
            cycler.reset()

    def coverage_hist2d(self):
        for i in range(len(self.bin_contents)):
            if(self.bin_contents[i]==0):
                continue
            for it in range(0,5):
                data=self.coverage_data.loc[self.coverage_data["iteration"]==it]
                std=np.std(data)
                #h,xedges,yedges=np.histogram2d(data[f"TruthBin{i}"],data[f"BinError{i}"]+data[f"TruthBin{i}"],bins=10)
                #profile_histogram(h,yedges,xedges,it)
                plt.hist2d(data[f"TruthBin{i}"],data[f"BinError{i}"]+data[f"TruthBin{i}"],bins=10)
                plt.legend()
                plt.xlabel("Truth Cross Section")
                plt.ylabel("Unfolded Cross Section")
                plt.title(f"Coverage Test{self.title} Bin {i} Iteration {it}")
                plt.show()
                #plt.savefig(f"{self.title}_bin_{i}.pdf")
                plt.cla()
                plt.close()
    def stats_per_iteration(self,bin_num):
        stats_error=np.zeros(self.iterations)
        for it in range(1,self.iterations+1):
            data=self.stat_data.loc[self.stat_data["iteration"]==it]
            std=np.std(data[f"BinError{bin_num}"]/(self.bin_contents[bin_num]+0.01))
            stats_error[it-1]=std
        return stats_error

    def plot_stats(self):
        x_iter=[i+1 for i in range(self.iterations)]
        for i in range(len(self.bin_contents)):
            y_stats=self.stats_per_iteration(i)
            plt.plot(x_iter,y_stats,".--b")
            plt.title(f"{self.title} Statistical Uncertainty Bin {i}")
            plt.xlabel("Bayesian Iteration")
            plt.ylabel("Statistical Uncertainty")
            plt.savefig(f"stat_uncert_{self.title}_bin{i}.pdf")
            plt.cla()
            plt.close()
    def bias_per_iteration(self,bin_num):
        iteration_data=[]
        bin_num=3
        for it in range(1,self.iterations+1):
            iter_data=self.bias_data.loc[self.bias_data["iteration"]==it]
            h,x_edges,y_edges=np.histogram2d(np.absolute(iter_data[f"TruthBin{bin_num}"]-self.bin_contents[bin_num])/np.absolute(self.bin_contents[bin_num]+0.0001),np.absolute(iter_data[f"BinError{bin_num}"])/(iter_data[f"TruthBin{bin_num}"]+0.00001),bins=[[0.1,0.2,0.3,0.4,0.5],[0.02,0.04,0.06,0.08,0.1]])
            x_bins,prof,stds=profile_histogram(h,y_edges,x_edges,it,draw=True)
            plt.show()
            iteration_data.append({x:y for x,y in zip(x_bins,prof)})
        bias_data={}
        for biases in iteration_data:
            for x,y in biases.items():
                if x in bias_data:
                    bias_data[x].append(y)
                else:
                    bias_data[x]=[y]
        return bias_data

    def stat_vs_bias(self):
        x_iters=[i+1 for i in range(self.iterations)]
        
        for i in range(len(self.bin_contents)):
            stats_data=self.stats_per_iteration(i)
            bias_data=self.bias_per_iteration(i)
            
            fig, ax1 = plt.subplots()
            color = 'tab:red'
            ax1.set_xlabel('Iterations')
            ax1.set_ylabel('Statistical Uncertainty', color=color)
            ax1.plot(x_iters, stats_data,".-", color=color)
            ax1.tick_params(axis='y', labelcolor=color)
            ax1.set_ylim(0)
            
            ax2 = ax1.twinx() 
            ax2.set_ylim(0)
            color = 'tab:blue'
            ax2.set_ylabel("bias", color=color)
            ax2.tick_params(axis='y', labelcolor=color)
            for key,value in bias_data.items():
                print(f"var{key} {value}")
                ax2.plot(x_iters,value,".--",label=f"{round(key,2)} variation") 
            fig.tight_layout()
            #plt.savefig(f"{self.title}_stats_vs_bias_bin{i}.pdf")
            plt.legend()
            plt.show()
    def print_matrix(self):
        matrix=np.array(matrices[self.title][-1])
        plt.matshow(matrix)
        am.label_matrix(matrix)
        plt.title(f"{self.title} Migration")
        #plt.show()
        plt.savefig(f"{self.title}_migrations.pdf")
        plt.cla()
        plt.close()

    def pull(self):
        for i in range(1,len(self.bin_contents)):
            if self.bin_contents[i] == 0:
                continue
            for it in range(1,6):
                iter_data=self.bias_data.loc[self.bias_data["iteration"]==it]
                h,xedges,yedges=np.histogram2d(iter_data[f"BinError{i}"]+iter_data[f"TruthBin{i}"],iter_data[f"TruthBin{i}"],bins=10)
                xbins,ymeans,stds=profile_histogram(h,yedges,xedges,1,draw=False)
                stat_iter_data=self.stat_data.loc[self.stat_data["iteration"]==it]
                stat_std=np.std(stat_iter_data[f"BinError{i}"])
                pull_data=[bias / stat_std for bias in stds ]
                plt.plot(xbins,pull_data,"--.",label=f"{it} Iteration")
            plt.legend()
            plt.xlabel("Truth Events")
            plt.ylabel("bias RMS / stat uncertanty")
            plt.title(f"{self.title} Bin {i} ")
            plt.show()

    def coverage_plot(self):
        for i in range(len(self.bin_contents)):
            if self.bin_contents[i] == 0:
                continue
            for it in range(0,10):
                color=cycler.get_color()
                try:
                    data=self.coverage_data.loc[self.coverage_data["Iteration"]==it]
                    print(data[f"BinBias{i}"][:10])
                    print(data[f"UnfoldingError{i}"][:10])
                    
                    n,bins,p=plt.hist(data[f"BinBias{i}"]/data[f"UnfoldingError{i}"],bins=50,histtype=u'step')
                    gaus=lambda x,mu,sig,N : N*np.exp(-(x-mu)**2 /(2* sig**2))
                    x=(bins[:-1]+bins[1:]) /2
                    results,cov=curve_fit(gaus,x,n,[np.mean(x),np.std(x),1],maxfev = 5000)
                    d_mean=np.mean(data[f"BinBias{i}"]/data[f"UnfoldingError{i}"])
                    d_std=np.std(data[f"BinBias{i}"]/data[f"UnfoldingError{i}"])
                    plt.plot(x,gaus(x,*results),label=f"Iter {it} mean={round(d_mean,2)},std={round(d_std,2)}")
                except Exception as e:
                    print(f"failed at Bin {i} , Iteration {it} for Variable {self.title}")
                    print(e)
            plt.title(f"{self.title} Bin {i}")
            plt.xlabel("Truth - Unfolded  / unfolding uncertainty")
            plt.legend()
            #plt.savefig(f"{self.name}_coverage_bin{i}.pdf")
            plt.show()
            plt.close()

    def plot_stat_fill(self):
        print(f"stat plot {self.title}")
        x=(self.bins[1:]+self.bins[:-1])/2
        xerr=(self.bins[1:]-self.bins[:-1])/2
        stats_data=self.stat_data.loc[self.stat_data["iteration"]==3]
        y_stat_error=[]
        for i in range(1,len(self.bin_contents)):
            stat=np.std(stats_data[f"BinError{i}"])
            y_stat_error.append(stat)
            y_stat_error.append(stat)
        y_stat_error=np.array(y_stat_error)
        y=self.bin_contents[1:]
        y_stat=np.zeros(2*len(y))
        y_stat[::2]=y
        y_stat[1::2]=y
        print(y)
        print(y_stat)
        print(y_stat_error)
        plt.errorbar(x/1000,y,xerr=xerr/1000,fmt="b+")
        plt.ylabel("Estimated Full Run 2 Events")
        x_stat=np.zeros(len(x)*2)
        x_stat[::2]=self.bins[:-1]
        x_stat[1::2]=self.bins[1:]
        plt.fill_between(x_stat/1000, y_stat-y_stat_error,y_stat+y_stat_error,facecolor='c',alpha=0.5,linewidth=0,edgecolor="k",label="Statistical Uncertainty")
        plt.title(f"{self.title} Statistical Uncertainty")
        plt.xlabel(f"{self.title} {self.units}")
        plt.savefig(f"{self.name}_stat_uncertainty.pdf")
        plt.show()
        plt.cla()
        
    def systematics_error(self):
        up_error_y=[]
        low_error_y=[]
        y_stat=[]
        x=(self.bins[1:]+self.bins[:-1])/2
        xerr=(self.bins[1:]-self.bins[:-1])/2
        stats_data=self.stat_data.loc[self.stat_data["iteration"]==3]        
        for i in range(1,len(self.bin_contents)):
            up_variation=np.sqrt(np.sum((self.syst_data[self.syst_data["variation"]=="up"][f"up_var{i}"]-self.bin_contents[i])**2))
            down_variation=np.sqrt(np.sum((self.syst_data[self.syst_data["variation"]=="down"][f"up_var{i}"]-self.bin_contents[i])**2))
            up_error_y.append(up_variation)
            low_error_y.append(down_variation)
            stat=np.std(stats_data[f"BinError{i}"])
            y_stat.append(stat)
        y_stat_errr=np.zeros(len(y_stat)+1)
        y_stat_errr[1:]=np.array(y_stat)/self.bin_contents[1:]
        y_stat_errr[0]=y_stat_errr[1]
        #y_stat[1:]=self.bin_contents[1:]
        y_stat=np.zeros(len(y_stat)+1)
        up_error_y=np.array(up_error_y)/self.bin_contents[1:]
        low_error_y=np.array(low_error_y)/self.bin_contents[1:]
        #y_stat[0]=y_stat[1]
        #plt.errorbar(x/1000,self.bin_contents[1:],xerr=xerr/1000,yerr=[low_error_y,up_error_y],fmt=".g",capsize=3,label="Systematics")
        plt.errorbar(x/self.divide,np.zeros(len(x)),xerr=xerr/self.divide,yerr=[low_error_y,up_error_y],fmt=".g",capsize=3,label="Systematics")
        print(len(self.bins))
        print(len(y_stat))
        print(len(y_stat_errr))
        plt.fill_between(self.bins/self.divide, y_stat-y_stat_errr,y_stat+y_stat_errr,facecolor='c',alpha=0.5,linewidth=2,edgecolor="k",linestyle='--' ,label="Statistical Uncertainty")
        plt.title(f"{self.title} Uncertainty Comparison")
        plt.legend()
        plt.xlabel("GeV")
        plt.ylabel("Uncertainty / Nominal Value")
        plt.savefig(f"{self.title}_systematics_error_comparison.pdf")
        plt.show()
        plt.cla()
        plt.close()
    def move(self):
        os.system(f"mv {self.title}_*.pdf observables/{self.title}/")


if __name__=="__main__":
    plt.rc('text', usetex=True)
    args=sys.argv
    observables={}
    #observables["Mjj_c16a"]=observable("Mjj")
    #observables["DPhill_c16a"]=observable("DPhill")
    #observables["Mll_c16a"]=observable("Mll")
    #observables["DYll_c16a"]=observable("DYll")
    #observables["DYjj_c16a"]=observable("DYjj")


    observables["higgs_pt"]=observable("higgs_pt")
    observables["Mjj"]=observable("Mjj")
    
    observables["Mll"]=observable("Mll")

    observables["DYjj"]=observable("DYjj")
    observables["DYll"]=observable("DYll")
    observables["MTll"]=observable("MTll")
    observables["lep_pt_0"]=observable("lep_pt_0")
    observables["lep_pt_1"]=observable("lep_pt_1")
    #observables["jet_pt_0"]=observable("jet_pt_0")
    #observables["jet_pt_1"]=observable("jet_pt_1")
    observables["cosTheta"]=observable("cosTheta")
    observables["DPhill"]=observable("DPhill")
    observables["Ptll"]=observable("Ptll")
    observables["ptTot"]=observable("ptTot")
    
   # observables["higgs_pt"].stat_vs_bias()
   # observables["ptTot"].print_matrix()
   # observables["ptTot"].draw_histograms()
   # observables["Mjj_c16a"].systematics_error()
    for dist,obs in observables.items():
        if(len(args) > 1):
            if args[1]=="matrix":
                obs.print_matrix()
            elif args[1]=="move":
                obs.move()
     
        #obs.stat_vs_bias()
        #obs.draw_histograms()
        obs.coverage_plot()
        #obs.plot_stat_fill()
        #obs.print_matrix()
        #obs.gaussians()
        #obs.draw_2dhist()
        #obs.systematics_error()
        #obs.plot_stat_fill()


        
