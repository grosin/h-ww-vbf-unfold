import uproot
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from binnings import get_bin_dict
from operator import gt,lt
from analyze_matrices import label_matrix

bin_dict=get_bin_dict()

def get_tree(cols,filename="ntupls/reco/01jet.root"):
    with uproot.open(filename) as rootfile:
        merged_tree = rootfile["top_nominal"]
        panda_frame=merged_tree.pandas.df(cols+["inSR","weight","inggFCR3"])
        panda_frame=panda_frame[panda_frame["inggFCR3"]==1]
        print(f"df size {len(panda_frame)}")
    return panda_frame

def get_new_tree(cols):
    filename="ntupls/reco/reco_ntuple_v21_signedjj.root"
    with uproot.open(filename) as rootfile:
        merged_tree = rootfile["Vbf_Total"]
        panda_frame = merged_tree.pandas.df(cols+["mtt","bjets","jet1_pt","weight","lep0_type","lep1_type"])
        panda_frame=panda_frame[panda_frame["mtt"]/1000 < (91-25)]
        panda_frame=panda_frame[panda_frame["Mjj"]/1000 >200]
        panda_frame=panda_frame[panda_frame["jet0_pt"]/1000 >30]
        panda_frame=panda_frame[panda_frame["jet1_pt"]/1000 >30]
        panda_frame=panda_frame[panda_frame["bjets"]<1]
        panda_frame=panda_frame[panda_frame["lep0_type"] !=panda_frame["lep1_type"]]
    return panda_frame
def plot_variable(obs,data):
    binning=np.array(bin_dict[obs])
    x=(binning[1:]+binning[:-1])/2
    xerr=(binning[1:]-binning[:-1])/2
    hist=np.histogram(data[obs],binning,weights=data["weight"])[0]
    f, (a0, a1) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [3, 1],'hspace':0})
    a0.set_title(f"{obs}")
    a0.set_ylabel("Weighted Bin Content")
    a0.errorbar(x,hist,xerr=xerr,yerr=np.sqrt(hist),fmt="+")
    a1.set_ylabel("Stat Uncert. Ratio")
    a1.errorbar(x,np.sqrt(hist)/hist,xerr=xerr,fmt=".")
    a1.grid(axis="y")
    a1.set_yticks([0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5])
    f.tight_layout()
    plt.show()

def overflow(obs,data):
    binning=np.array(bin_dict[obs])
    upper_edge=binning[-1]
    lower_edge=binning[0]
    overflow=np.sum(data[data[obs] > upper_edge]["weight"])
    underflow=np.sum(data[data[obs] < lower_edge]["weight"])
    print(f"{obs} : Overflow {overflow} Underflow {underflow}")


def loop_variables():
    #variables=["Mjj","DYll","DYjj","Mll","DPhill","lep0_pt","jet0_pt","costhetastar","SignedDPhijj","pt_H","lep1_pt","jet1_pt"]
    variables=["jet1_pt","Mll","jet0_pt"]
    data1=get_tree(variables)

    for var in variables:
        overflow(var,data1)
        plot_variable(var,data1)


def compare_trees():
    reco_file="ntupls/reco/reco_v21_ntuples.root"
    my_file="ntupls/reco/reco_ntuple_v21_signedjj.root"
    variables=["Mjj","DYll","DYjj","Mll","jet0_pt"]
    data1=get_new_tree(variables)
    data2=get_tree(variables,reco_file)
    for obs in variables:
        binning=np.array(bin_dict[obs])
        x=(binning[1:]+binning[:-1])/2
        xerr=(binning[1:]-binning[:-1])/2
        hist1=np.histogram(data1[obs],binning,weights=data1["weight"])[0]
        hist2=np.histogram(data2[obs],binning,weights=data2["weight"])[0]
        plt.errorbar(x,hist1,xerr=xerr,fmt=",",label="guy file")
        plt.errorbar(x,hist2,xerr=xerr,fmt=",",label="sagar file")
        plt.legend()
        plt.title(f"{obs}")
        plt.show()
if __name__=="__main__":
    #compare_trees()
    loop_variables()

