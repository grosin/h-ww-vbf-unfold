import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from binnings import get_bin_dict



def load_syst_data():
    path="systematics_contributions/"
    data_up1=pd.read_csv(path+"up_systematics_contribution_1.csv",index_col=0)
    data_up2=pd.read_csv(path+"up_systematics_contribution.csv",index_col=0)
    data_up=pd.concat([data_up1,data_up2])
    
    data_down1=pd.read_csv(path+"down_systematics_contribution_1.csv",index_col=0)
    data_down2=pd.read_csv(path+"down_systematics_contribution.csv",index_col=0)
    data_down=pd.concat([data_down1,data_down2])

    data_up.drop_duplicates(inplace=True)
    data_down.drop_duplicates(inplace=True)
    
    data_down["total"]=np.sum(np.abs(data_down.loc[:,"top":]),axis=1)
    data_up["total"]=np.sum(np.abs(data_up.loc[:,"top":]),axis=1)

    data_up.drop_duplicates(inplace=True)
    data_down.drop_duplicates(inplace=True)

    data_up_zeros=data_up[data_up["total"] <= 0.001].reset_index(drop=True)
    data_up_small=data_up[np.logical_and(data_up["total"]<=.1,data_up["total"]>.001)].reset_index(drop=True)
    data_up_large=data_up[data_up["total"] >.1].sort_values(by="total",ascending=False).reset_index(drop=True)
    
    data_down_zeros=data_down[data_down["total"] <= 0.001].reset_index(drop=True)
    data_down_small=data_down[np.logical_and(data_down["total"]<=.01,data_down["total"]>.001)].reset_index(drop=True)
    data_down_large=data_down[data_down["total"] >.01].sort_values(by="total",ascending=False).reset_index(drop=True)
    
   
    return data_up_large,data_down_large

def get_large_systs_up():
    large_up,large_down=load_syst_data()
    return large_up["systematic"].values

def get_large_systs_down():
    large_up,large_down=load_syst_data()
    return large_down["systematic"].values

def get_all_large_systs():
    large_up,large_down=load_syst_data()
    all_data=pd.concat((large_up,large_down))
    all_data.sort_values(by="total",ascending=False).reset_index(drop=True)
    return all_data["systematic"].values

def load_obs_syst():
    bin_dict=get_bin_dict()
    my_naming={"Mjj":"Mjj","Mll":"Mll","DYjj":"DYjj","DYll":"DYll","pt_H":"higgs_pt","lep0_pt":"lep_pt_0","jet0_pt":"jet_pt_0","MT":"MT"}
    #binning=np.array(bin_dict[my_naming[observable]])
    data_file=open("sytematics_ratio_means.txt")
    data_frame_dict={obs:{f"Bin{i}":[] for i in range(len(bin_dict[value])-1)} for obs,value in my_naming.items()}
    for key,di in data_frame_dict.items():
        data_frame_dict[key].update({"systematic":[]})
        
    for line in data_file:
        line_array=line.split(",")
        data_frame_dict[line_array[0]]["systematic"].append(line_array[1])
        line_data=line_array[2:]
        for i in range(len(line_data)):
           data_frame_dict[line_array[0]][f"Bin{i}"].append(float(line_data[i]))

    obs_dataframes={}
    for key,data in data_frame_dict.items():
        obs_dataframes[key]=pd.DataFrame(data).drop_duplicates()

    up_systs={}
    down_systs={}
    for key,data in obs_dataframes.items():
        if data.empty:
            continue
        print("-------------------------------------------")
        print(f"Distribution {key}")
        for i in range(len(data.columns)-1):
            print(f"top 5  up systematics for bin {i}")
            sorted_up=data.sort_values(by=f"Bin{i}",ascending=False).reset_index(drop=True)
            for indx,syst in enumerate(sorted_up["systematic"].head(5)):
                if syst in up_systs:
                    up_systs[syst][indx]+=1
                else:
                    up_systs[syst]=[0]*5
                    up_systs[syst][indx]+=1

            print(sorted_up[["systematic",f"Bin{i}"]].head(3))
            print("******************")
            print(f"top 5 down systematics for bin{i}")
            sorted_down=data.sort_values(by=f"Bin{i}",ascending=True).reset_index(drop=True)

            for indx,syst in enumerate(sorted_down["systematic"].head(5)):
                if syst in down_systs:
                    down_systs[syst][indx]+=1
                else:
                    down_systs[syst]=[0]*5
                    down_systs[syst][indx]+=1
            print(sorted_down[["systematic",f"Bin{i}"]].head(5))
            print("*********************")
        print("-------------------------------------------")

    sorted_up_systematics=[(k, up_systs[k]) for k in sorted(up_systs, key=lambda key:up_systs[key][4]*1+up_systs[key][3]*10+up_systs[key][2]*100+up_systs[key][1]*1000+up_systs[key][0]*10000, reverse=True)]

    sorted_down_systematics=[(k, down_systs[k]) for k in sorted(down_systs, key=lambda key:down_systs[key][4]*1+down_systs[key][3]*10+down_systs[key][2]*100+down_systs[key][1]*1000+down_systs[key][0]*10000, reverse=True)]
    for i in sorted_up_systematics:
        print(i)

    print("-----------------------------------------")
    for i in sorted_down_systematics:
        print(i)
    print("-----------------------")
    top_systs=set(["_".join(s.split("_")[:-1]) for s in up_systs.keys()]) | set(["_".join(s.split("_")[:-1]) for s in down_systs.keys()])
if __name__=="__main__":
    load_obs_syst()

    
    
