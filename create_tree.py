import uproot
import pandas as pd
import numpy as np

def write_tree(tree_names,name):
    cols=["weight","Mjj","Mll","costhetastar","pt_H","DPhill","SignedDPhijj","DYll","DYjj","lep0_pt","lep1_pt","jet0_pt","jet1_pt"]
    with uproot.recreate(name+"_nominal_tree.root") as f:
        for tree_name in tree_names:
            f[tree_name] = uproot.newtree({col: np.float32 for col in cols})
       
def get_names(samples):
    all_systs=open("AllSystematics.txt")
    fakes=open("FakesSystematics.txt")
    syst_names=[]
    for line in all_systs:

        for sample in samples:
            if("akes" in sample):
                continue
            if line.rstrip().lower()=="nominal":
                syst_names.append(sample+"_"+line.rstrip())
                print("appended nominal")
                print(sample+"_"+line.rstrip())
            else:
                continue
                syst_names.append(sample+"_"+line.rstrip()+"__1down")
                syst_names.append(sample+"_"+line.rstrip()+"__1up")

    for line in fakes:
        continue
        name=line.rstrip()
        if name not in syst_names:
            syst_names.append("Fakes_"+name+"__1up")
            syst_names.append("Fakes_"+name+"__1down")
    all_systs.close()
    fakes.close()
    return syst_names

samples="top vgamma diboson zjets vbf Zjets0 vbf0 ggf data vh htt fakes".split(" ")
for sample in samples:
    tree_names=get_names([sample])
    write_tree(tree_names,sample)
